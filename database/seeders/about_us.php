<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class about_us extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content')->insert([
            'slug' => "about-us",
            'type' => "sejarah",
            "json" => '{"image":"upload/about-us/default/default-1.jpg","text":"<p>Jejak Imani Travel Umrah &amp; Islamic Tours adalah perusahaan yang bergerak di bidang jasa travel <strong>Biro Umrah, Islamic Halal Tour dan Haji</strong><em>.</em> Jejak Imani berdiri sejak tahun 2014 dengan berbentuk Perseroan Terbatas (PT.) dengan nama PT. Jejak Imani Berkah Bersama. Jejak Imani sudah berizin resmi, dengan nomor&nbsp;<strong>PPIU No. U.553 Tahun 2020 </strong>dan <strong>akreditasi PPIU bernilai A</strong> dengan nomor <strong>2020-PPIU 1-0011</strong>. Lokasi Jejak Imani berada di Jl. Siliwangi No. 4, Kecamatan Pondok Benda, Kota Tangerang Selatan, Provinsi Banten.</p>"}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "about-us",
            'type' => "visi",
            "json" => 'Menjadi Biro Umrah, Islamic Halal Tour dan Haji Terbesar di Indonesia dengan Pelayanan Prima',
            "created_by" => 1
        ]);
        
        DB::table('content')->insert([
            'slug' => "about-us",
            'type' => "misi",
            "json" => '<p>1. Membangun hubungan interpersonal yang bersifat kekeluargaan antara Jejak Imani dengan jamaah maupun calon jamaah&nbsp;Umrah, Haji Plus, <i>Islamic </i>dan <i>Halal Tours</i><span class="Apple-converted-space">&nbsp;</span>Jejak Imani.</p><p>2. Terus menerus meningkatkan pelayanan&nbsp;Umrah, Haji Plus, <i>Islamic</i> dan <i>Halal Tours </i>dengan melayani&nbsp;jama’ah sepenuh hati.</p><p>3. Memudahkan jamaah dalam program pembayaran biaya Umrah dan Haji Plus.</p><p>4. Menjaga kualitas ke syar’ian dalam setiap ibadah Umrah dan Haji Plus.</p><p>5. Memberi pelayanan terbaik kepada jamaah <em>Islamic Tours</em> dengan menyediakan makanan halal dan memfasilitasi tempat shalat selama program tersebut.</p>',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "about-us",
            'type' => "eksekutif",
            "json" => '{"jabatan":"Komisaris Jejak Imani","name": "H. Sjafardamsah S.ST. S.E M.M Ak. BKP. C. A.","text":"H. Sjafardamsah S.ST. S.E M.M Ak. BKP. C. A. adalah komisaris Jejak Imani. Selain menjadi komisaris Jejak Imani, beliau adalah pengajar di PKN STAN dan menjadi konsultan Pajak."}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "about-us",
            'type' => "eksekutif",
            "json" => '{"jabatan":"Presiden Direktur Jejak Imani","name": "M. Rizaldy Latief","text":"Beliau adalah Presiden Direktur Jejak Imani. Beliau adalah salah satu inisiator dari komunitas Jallan Barreng."}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "about-us",
            'type' => "eksekutif",
            "json" => '{"jabatan":"Komisaris Utama Jejak Imani","name": "H. Salim A. Fillah","text":"Beliau seorang Da`i Nasional dan penulis buku. Salim A. Fillah sudah menelurkan banyak buku best seller tentang dakwah dan pernikahan. Beliau adalah Pembina dari komunitas Majelis Jejak Nabi dan Jallan Barreng."}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "about-us",
            'type' => "eksekutif",
            "json" => '{"jabatan":"Direktur Pengembangan Bisnis","name": "H. Rony Helmi S.E","text":"H. Rony Helmi S.E adalah direktur pengembangan bisnis Jejak Imani. Beliau adalah seorang pengusaha. Selain Jejak Imani, Beliau mempunyai belasan outlet fashion di Jabodetabek, Jawa Barat dan Jawa Tengah."}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "about-us",
            'type' => "sambutan",
            "json" => '{"image":"upload/about-us/default/default-2.jpg","text":"<p>﷽<br><br>السلامعليكمورحمةاللهوبركاته<br><br>Sejak awal Jejak Imani didirikan, kami berkomitmen untuk menjadi sebaik-baiknya pelayan para tamu Allah menuju Tanah Suci. Tahun demi tahun kami lewati dengan semangat untuk senantiasa memperbaiki kualitas dan sistem pelayanan agar jamaah dapat beribadah dengan maksimal dan nyaman.<br><br>Melayani para tamu Allah ke Tanah Suci dan memfasilitasi ummat melihat dan mempelajari sejarah keIslaman di negara-negara muslim lainnya adalah tujuan dan kebahagiaan kami.<br><br>Alhamdulillah saat ini PT. Jejak Imani Berkah Bersama sudah memiliki izin resmi bernomor U.553 tahun 2020 dari Kementerian Agama Republik Indonesia. Semoga dengan izin resmi ini, kami dapat dipercaya umat muslim Indonesia sebagai pilihan utama untuk beribadah ke Tanah Suci juga menyusuri jejak-jejak keislaman di berbagai negara yang Allah karuniakan peradaban dan kejayaan Islam.<br><br>Kami berharap dapat menjalin kerjasama yang baik dan saling menguntungkan dengan Bapak/Ibu sebagai langkah awal melayani para tamu Allah dengan sebaik-baiknya pelayanan.<br><br>Terima kasih.<br><br>جزاكماللهخيرا<br>والسلامعليكمورحمةاللهوبركاته<br><br>Muhammad Rizaldy Latief<br>Presiden Direktur<br>PT.Jejak Imani Berkah Bersama<br>Pelayan Tamu Allah<br></p>"}',
            "created_by" => 1
        ]);
    }
}
