<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\user as SeedersUser;
use Database\Seeders\umroh as SeedersUmroh;
use Database\Seeders\umroh_category as SeedersUmrohCategory;
use Database\Seeders\haji as SeedersHaji;
use Database\Seeders\sorter as SeedersSorter;
use Database\Seeders\article as SeedersArticle;
use Database\Seeders\about_us as SeedersAboutUs;
use Database\Seeders\contact_us as SeedersContactUs;
use Database\Seeders\sosial_media as SeedersSosialMedia;
use Database\Seeders\faq as SeedersFAQ;
use Database\Seeders\packet as SeedersPacket;
use Database\Seeders\jejak_islami as SeedersJejakIslami;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            SeedersUser::class,
            SeedersUmrohCategory::class,
            SeedersUmroh::class,
            SeedersHaji::class,
            SeedersSorter::class,
            SeedersArticle::class,
            SeedersAboutUs::class,
            SeedersContactUs::class,
            SeedersSosialMedia::class,
            SeedersFAQ::class,
            SeedersPacket::class,
            SeedersJejakIslami::class,
        ]);
    }
}
