<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class umroh_category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('umroh_category')->insert([
            'name' => "Tanpa kategori",
            "created_by" => 1
        ]);

        DB::table('umroh_category')->insert([
            'name' => "Sapphire",
            "text" => "Umrah Lebih Nyaman (Sapphire ⭐5 & Umrah Plus)",
            "created_by" => 1
        ]);

        DB::table('umroh_category')->insert([
            'name' => "Ruby",
            "text" => "Umrah Lebih Hemat (Emerald ⭐4 & Ruby ⭐3 )",
            "created_by" => 1
        ]);
    }
}
