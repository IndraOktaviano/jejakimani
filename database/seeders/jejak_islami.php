<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class jejak_islami extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "section-1",
            "json" => '{"image":"upload/jejak-islami/section-1/default/default-1.jpg","price":"Mulai dari Rp 10 jutaan * per pax","name":"Desember, Januari, Februari dan Maret 2021 – 2022","phone":"628119513223", "text_wa":"Assalamu’alaikum Mbak Yuta saya melihat paket Jejak Islami Turkey di website dan ingin bertanya tentang paket tur Jejak Islami Turki", "label_wa":"Daftar Sekarang"}',
            "created_by" => 1
        ]);

        foreach (range(1, 7) as $i) {
            DB::table('content')->insert([
                'slug' => "jejak-islami",
                'type' => "provit",
                "json" => '{"image":"upload/jejak-islami/profit/default/default-' . $i . '.jpg","name":"Keterangan Keunggulan ' . $i . '"}',
                "created_by" => 1
            ]);
        }

        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "trip-plan",
            "json" => '{"name":"JAKARTA – ISTANBUL (Dinner on Board)","text":"Berkumpul di Bandara Internasional Soekarno Hatta untuk penerbangan menuju Istanbul."}',
            "created_by" => 1
        ]);
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "trip-plan",
            "json" => '{"name":"ISTANBUL – BURSA (Lunch, Dinner)","text":"Tiba di Bandara Istanbul dilanjutkan perjalanan ke Bursa, mengunjungi Grand Mosque, Green Mosque, Uludag Mountain dan Cable Car. Bermalam di Bursa. Hotel : Holiday Inn/setaraf *5"}',
            "created_by" => 1
        ]);
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "trip-plan",
            "json" => '{"name":"BURSA – KUSADASI (3x Meals)","text":"Setelah sarapan mengunjungi Turkish Delight Showroom dilanjutkan perjalanan menuju Kusadasi. Mengunjungi Masjid Isabey. Setelah itu perjalanan ke hotel untuk makan malam dan menginap. Hotel : Korumar Hotel/setaraf *5"}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "trip-facility",
            "json" => '{"name":"Hotel Bintang 4/5 setaraf (Room Double/Twin/Triple)","icon":"fas fa-bed","detail": [{"location": "Bursa", "name": "Tiara Thermal Hotel" }, {"location": "Kusadasi", "name": "Marti Beach" },{"location": "Pamukkale", "name": "Pam Thermal Hotel" },{"location": "Cappadocia", "name": "Uchisar Kaya Hotel" },{"location": "Ankara", "name": "Sergah Hotel" },{"location": "Istanbul", "name": "Golden Way" }]}',
            "created_by" => 1
        ]);
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "trip-facility",
            "json" => '{"name":"Tiket PP Ekonomi","icon":"fas fa-plane"}',
            "created_by" => 1
        ]);
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "trip-facility",
            "json" => '{"name":"Makan & Minum Sesuai itinerary","icon":"fas fa-utensils"}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "no-include",
            "json" => '{"name":"Asuransi perjalanan","icon":"fas fa-ambulance"}',
            "created_by" => 1
        ]);
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "no-include",
            "json" => '{"name":"PCR Test di Turki","icon":"fas fa-user-md"}',
            "created_by" => 1
        ]);
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "no-include",
            "json" => '{"name":"Tips Guide & Driver","icon":"fas fa-car"}',
            "created_by" => 1
        ]);
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "no-include",
            "json" => '{"name":"PCR test di Indonesia","icon":"fas fa-first-aid"}',
            "created_by" => 1
        ]);
        DB::table('content')->insert([
            'slug' => "jejak-islami",
            'type' => "optional",
            "json" => '<ul> <li>Hot Air Balloon 250 USD/pax;</li> <li>Jeep Safari 80 USD/pax;</li> <li>Panorama 1453 Museum 10 USD/pax;</li> <li>Uludag Mountain 30 USD/hour per pax;</li> <li>Tarian Sufi 30 USD/pax;</li> </ul>',
            "created_by" => 1
        ]);        
    }
}
