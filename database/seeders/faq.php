<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class faq extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content')->insert([
            'slug' => "faq",
            'type' => "list",
            "json" => '{"name":"Paket perjalanan apa saja yang tersedia di Jejak Imani?","text":"<p>Jejak Imani Travel Umrah &amp; Islamic Tours mempunyai Paket:</p><ol><li>Umrah Emerald &amp; Shapphire (Reguler)</li><li>Umrah Ruby (Super Hemat)</li><li>Jejak Islami (Halal Tour)</li></ol>"}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "faq",
            'type' => "list",
            "json" => '{"name":"Apa perbedaan paket Emerald, Sapphire & Ruby","text":"<p><p>Perbedaan paket Emerald, Shapphire &amp; Ruby antara lain:</p><ol><li>Paket Ruby memakai hotel bintang 3, paket Emerald memakai hotel bintang 4 dan&nbsp;paket Shapphire memakai hotel bintang 5</li><li>Koper paket Ruby &amp; Emerald dari bahan kain, sementara paket Shapphire dari bahan fiber</li></ol>"}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "faq",
            'type' => "phone",
            "json" => '021 2274 1322 | 0857 2002 8100 | 0811 8246 988',
            "created_by" => 1
        ]);
    }
}
