<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class sorter extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sorter')->truncate();

        DB::table('sorter')->insert([
            'slug' => "haji-khusus",
            'type' => "section-list-1",
            "json" => '[9,10,11,12,13,14,15,16]',
            "created_by" => 1
        ]);
        DB::table('sorter')->insert([
            'slug' => "haji-khusus",
            'type' => "section-list-2",
            "json" => '[17,18,19]',
            "created_by" => 1
        ]);
        DB::table('sorter')->insert([
            'slug' => "haji-khusus",
            'type' => "section-list-3",
            "json" => '[20,21,22,23,24,25]',
            "created_by" => 1
        ]);
        DB::table('sorter')->insert([
            'slug' => "haji-khusus",
            'type' => "section-list-4",
            "json" => '[26]',
            "created_by" => 1
        ]);
        // separtaor...............
        DB::table('sorter')->insert([
            'slug' => "haji-furoda",
            'type' => "section-list-1",
            "json" => '[35,36,37,38,39,40,41,42]',
            "created_by" => 1
        ]);
        DB::table('sorter')->insert([
            'slug' => "haji-furoda",
            'type' => "section-list-2",
            "json" => '[43,44,45]',
            "created_by" => 1
        ]);
        DB::table('sorter')->insert([
            'slug' => "haji-furoda",
            'type' => "section-list-3",
            "json" => '[46,47,48,49,50,51]',
            "created_by" => 1
        ]);
        DB::table('sorter')->insert([
            'slug' => "haji-furoda",
            'type' => "section-list-4",
            "json" => '[52]',
            "created_by" => 1
        ]);

        // separtor ...................
        DB::table('sorter')->insert([
            'slug' => "about-us",
            'type' => "eksekutif",
            "json" => '[4,5,6,7]',
            "created_by" => 1
        ]);


        // separator ..............
        DB::table('sorter')->insert([
            'slug' => "jejak-islami",
            'type' => "provit",
            "json" => '[23,24,25,26,27,28,29]',
            "created_by" => 1
        ]);

        DB::table('sorter')->insert([
            'slug' => "jejak-islami",
            'type' => "trip-plan",
            "json" => '[30,31,32]',
            "created_by" => 1
        ]);

        DB::table('sorter')->insert([
            'slug' => "jejak-islami",
            'type' => "trip-facility",
            "json" => '[33,34,35]',
            "created_by" => 1
        ]);

        DB::table('sorter')->insert([
            'slug' => "jejak-islami",
            'type' => "no-include",
            "json" => '[36,37,38,39]',
            "created_by" => 1
        ]);

    }
}
