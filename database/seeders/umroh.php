<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class umroh extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 3) as $value) {
            $date = mt_rand(1, 28);
            DB::table('umroh')->insert([
                'umroh_category_id' => 2,
                "slug" => 2 . "-" . $value,
                'name' => date($date . " F Y"),
                'price' => mt_rand(10000000, 30000000),
                'schedule' => date("Y-m-" . $date),
                'image' => "upload/umroh/default/default-1.jpg",
                'maskapai' => "upload/umroh/maskapai/default/maskapai.png",
                "created_by" => 1
            ]);
        }
        foreach (range(1, 5) as $value) {
            $date = mt_rand(1, 28);
            DB::table('umroh')->insert([
                'umroh_category_id' => 3,
                "slug" => 3 . "-" . $value,
                'name' => date($date . " F Y"),
                'price' => mt_rand(10000000, 30000000),
                'schedule' => date("Y-m-" . $date),
                'image' => "upload/umroh/default/default-2.jpg",
                'maskapai' => "upload/umroh/maskapai/default/maskapai.png",
                "created_by" => 1
            ]);
        }
    }
}
