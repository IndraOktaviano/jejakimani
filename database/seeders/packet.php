<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class packet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 6) as $i) {
            DB::table('other')->insert([
                'slug' => "packet",
                'type' => "lainnya",
                "json" => '{"image":"upload/other/packet/default/default-' . $i . '.png","url":"#"}',
                "created_by" => 1
            ]);
        }
    }
}
