<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class haji extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "banner",
            "json" => "upload/haji/banner/default/banner-1.jpg",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "banner",
            "json" => "upload/haji/banner/default/banner-2.jpg",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "banner",
            "json" => "upload/haji/banner/default/banner-3.png",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "label_banner",
            "json" => '{"name":"<h1>HAJI PLUS/HAJI KHUSUS</h1>", "text":"“Dan (di antara) kewajiban manusia terhadap Allah adalah melaksanakan ibadah haji ke Baitullah, yaitu bagi orang-orang yang mampu mengadakan perjalanan ke sana. Barangsiapa mengingkari (kewajiban) haji, maka ketahuilah bahwa Allah Mahakaya (tidak memerlukan sesuatu) dari seluruh alam.” (QS Al Imran : 97)", "phone":"628119513223", "text_wa":"Assalamu’alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.", "label_wa":"Klik Disini Untuk Konsultasi Gratis", "event": ""}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "section-1",
            "json" => '{"image":"upload/haji/default-section-1.jpg", "text":"<h2>لَبَّيْكَ اللَّهُمَّ لَبَّيْكَ</h2><p>Aku datang memenuhi panggilan-Mu ya Allah, aku datang.</p>"}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "section-2",
            "json" => '{"image":"upload/haji/default-section-2.jpg","name":"<h2>HAJI KHUSUS</h2><p>ARBAIN</p>","price":"700000","label_price": "<p>DP $5.000 / +- Rp 72.500.000,- (Kurs Rp 14.500) <small>All in</small></p>","phone":"628119513223","text_wa":"Assalamu’alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.","text":"<p>Bersama Jejak Imani yang selalu mengutamakan dan memfasilitasi kenyamanan lebih bagi para tamu-tamu Allah &lrm;ﷻ sejak sebelum keberangkatan hingga kembali ke tanah air.</p><p>Jejak Imani terdaftar secara resmi sebagai penyelenggara perjalanan haji khusus <strong>No. 394 Tahun 2021</strong> dan penyelenggara perjalanan ibadah umrah <strong>No. U.533 Tahun 2020</strong> di Kementerian Agama RI. Alhamdulillah, Jejak Imani&nbsp;<strong>terakreditasi A</strong> oleh Kementerian Agama RI.</p>", "label_wa":"Klik Disini Untuk Konsultasi Gratis", "event": ""}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "section-3",
            "json" => '{"name": "Keterangan:","text":"<ol><li>Harga di atas bersifat sementara, harga tetap akan diinformasikan pada tahun keberangkatan</li><li>Harga menggunakan USD akan disesuaikan dengan kurs IDR pada tahun pendaftaran dan tahun keberangkatan</li><li>DP untuk mendapatkan nomor porsi haji plus sebesar USD $5.000 / +- Rp 72.500.000,- (Kurs Rp 14.500) (All in)</li><li>Estimasi masa tunggu quota Haji Plus/Haji Khusus adalah 5-8 tahun</li><li>Apabila jamaah melakukan pembatalan haji, maka akan dibebankan biaya administrasi sesuai dengan ketentuan yang berlaku di Jejak Imani</li><li>Apabila saat berangkat jumlah jamaah tidak memenuhi paket standar quad (sekamar ber-4), maka calon jamaah akan dikenakan biaya paket sesuai jumlah jamaah yang ada, yaitu harga paket sekamar ber-3 (triple) atau ber-2 (double)</li></ol>"}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "section-4",
            "json" => '{"name": "Perhatian!","text":"<p>Jejak Imani memfasilitasi keberangkatan Haji Plus/Haji Khusus dengan masa tunggu yang lebih singkat (dibanding haji reguler) dengan fasilitas bintang lima (⭐️⭐️⭐️⭐️⭐️). Jadwal keberangkatan Haji Plus/Haji Khusus&nbsp;ditentukan oleh BPIH/Kementerian Agama RI.</p><p>Jika ingin berangkat haji lebih cepat (di tahun ini), Anda dapat memilih program haji furoda’.</p>", "phone": "628119513223", "text_wa": "Assalamu’alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.", "label_wa": "Klik Disini Untuk Berangkat Haji Lebih Cepat", "event": ""}',
            "created_by" => 1
        ]);

        foreach (range(1, 8) as $i) {
            DB::table('haji')->insert([
                'slug' => "khusus",
                "type" => "section-list-1",
                "json" => '{"image": "upload/haji/section-list-1/default/default-' . $i . '.jpg", "name": "Menu-' . $i . '","text":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry' . 's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}',
                "created_by" => 1
            ]);
        }

        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "section-list-2",
            "json" => '{"name": "HARI PERTAMA - 4 DZULHIJJAH : JAKARTA - JEDDAH - AZIZIYAH","text":"<ul><li>Jamaah berkumpul di Hotel <strong>Bandara Soekarno Hatta</strong> – Jakarta 8 (delapan) jam sebelum keberangkatan.</li><li>Pada waktu yang ditentukan, jamaah akan bertolak menuju <strong>Jeddah</strong>.</li><li>Setelah pemeriksaan imigrasi jamaah melanjutkan perjalanan ke apartemen transit di <strong>Aziziyah</strong> dengan menggunakan kendaraan yang telah kami siapkan.</li><li><em>Check in</em> apartemen dan persiapan umrah.</li></ul>"}',
            "created_by" => 1
        ]);

        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "section-list-2",
            "json" => '{"name": "HARI KEDUA - 5 DZULHIJJAH : AZIZIYAH - APARTEMEN TRANSIT - UMRAH","text":"<ul><li>Thawaf, sa’i dan tahallul.</li><li>Kegiatan selama di <strong>Makkah</strong> setelah umrah adalah : jamaah memperbanyak ibadah dan istirahat <strong>persiapan haji</strong> di <strong>Apartemen Transit.</strong></li><li>Mengikuti seluruh program yang sudah dijadwalkan oleh Jejak Imani.</li><li>Shalat berjamaah, baca dzikir pagi &amp; petang, mengaji bersama.</li><li>Kajian pemaknaan haji.</li></ul>"}',
            "created_by" => 1
        ]);

        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "section-list-2",
            "json" => '{"name": "HARI KETIGA - 6 DZULHIJJAH : AZIZIYAH - APARTEMEN TRANSIT - CITY TOUR MAKKAH","text":"<ul><li>Kegiatan selama di <strong>Makkah</strong> setelah umrah adalah : jamaah memperbanyak ibadah dan istirahat <strong>persiapan haji</strong> di <strong>Apartemen Transit</strong>.</li><li>Mengikuti seluruh program yang sudah dijadwalkan oleh Jejak Imani.</li><li>Ziarah Kota <strong>Makkah</strong> (City tour napak tilas haji). <b>Jabal Tsur, ARMINA, Miqat di Masjid Ji’ranah, Jabal Nur.</b></li><li>Shalat berjamaah, baca dzikir pagi &amp; petang, mengaji bersama.</li><li>Pemantapan fiqh thaharah dan shalat.</li></ul>"}',
            "created_by" => 1
        ]);

        foreach (range(1, 6) as $i) {
            DB::table('haji')->insert([
                'slug' => "khusus",
                "type" => "section-list-3",
                "json" => '{"image": "upload/haji/section-list-3/default/default-' . $i . '.jpg", "name": "Judul-' . $i . '","text":"Ini deskripsi ' . $i . '"}',
                "created_by" => 1
            ]);
        }

        DB::table('haji')->insert([
            'slug' => "khusus",
            "type" => "section-list-4",
            "json" => '{"name": "BIAYA TIDAK TERMASUK","text":"<ol><li>Tiket pesawat PP kelas ekonomi</li><li>Visa haji/biaya ONH Plus kuota Kemenag</li><li>Asuransi syariah</li><li>Bimbingan manasik haji di Indonesia dan Arab Saudi</li><li>Perlengkapan ibadah haji</li><li>Hotel ⭐️ 5 dan apartemen Aziziyah sesuai program</li><li>Makan 3 kali sehari <em>f</em><em>ullboard</em></li><li>Transportasi bus AC selama perjalanan ibadah haji di Tanah Suci</li><li>Pembimbing dan muthawwif berpengalaman dengan <em>background</em> syari’ah</li><li>Penggunaan radiophone selama program</li><li>Air zam-zam 5 liter per jamaah</li></ol>"}',
            "created_by" => 1
        ]);

        // Separator................................

        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "banner",
            "json" => "upload/haji/banner/default copy/banner-1.jpg",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "banner",
            "json" => "upload/haji/banner/default copy/banner-2.jpg",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "banner",
            "json" => "upload/haji/banner/default copy/banner-3.png",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "label_banner",
            "json" => '{"name":"<h1>HAJI FURODA/HAJI MANDIRI</h1><h2>HAJI TANPA ANTRI</h2>", "text":"“Dan (di antara) kewajiban manusia terhadap Allah adalah melaksanakan ibadah haji ke Baitullah, yaitu bagi orang-orang yang mampu mengadakan perjalanan ke sana. Barangsiapa mengingkari (kewajiban) haji, maka ketahuilah bahwa Allah Mahakaya (tidak memerlukan sesuatu) dari seluruh alam.” (QS Al Imran : 97)", "phone":"628119513223", "text_wa":"Assalamu’alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.", "label_wa":"Klik Disini Untuk Konsultasi Gratis", "event": ""}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "section-1",
            "json" => '{"image":"upload/haji/default-section-1 copy.jpg", "text":"<h2>لَبَّيْكَ اللَّهُمَّ لَبَّيْكَ</h2><p>Aku datang memenuhi panggilan-Mu ya Allah, aku datang.</p>"}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "section-2",
            "json" => '{"image":"upload/haji/default-section-2 copy.jpg","name":"<h2>HAJI MANDIRI TANPA ANTRI</h2>","price":"700000","label_price": "<p>DP $5.000 / +- Rp 72.500.000,- (Kurs Rp 14.500) <small>All in</small></p>","phone":"628119513223","text_wa":"Assalamu’alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.","text":"<p>Bersama Jejak Imani yang selalu mengutamakan dan memfasilitasi kenyamanan lebih bagi para tamu-tamu Allah &lrm;ﷻ sejak sebelum keberangkatan hingga kembali ke tanah air.</p><p>Jejak Imani terdaftar secara resmi sebagai penyelenggara perjalanan haji khusus <strong>No. 394 Tahun 2021</strong> dan penyelenggara perjalanan ibadah umrah <strong>No. U.533 Tahun 2020</strong> di Kementerian Agama RI. Alhamdulillah, Jejak Imani&nbsp;<strong>terakreditasi A</strong> oleh Kementerian Agama RI.</p>", "label_wa":"Klik Disini Untuk Konsultasi Gratis", "event": ""}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "section-3",
            "json" => '{"name": "Keterangan:","text":"<ol><li>Harga di atas bersifat sementara, harga tetap akan diinformasikan pada tahun keberangkatan</li><li>Harga menggunakan USD akan disesuaikan dengan kurs IDR pada tahun pendaftaran dan tahun keberangkatan</li><li>DP untuk mendapatkan nomor porsi haji plus sebesar USD $5.000 / +- Rp 72.500.000,- (Kurs Rp 14.500) (All in)</li><li>Estimasi masa tunggu quota Haji Plus/Haji Khusus adalah 5-8 tahun</li><li>Apabila jamaah melakukan pembatalan haji, maka akan dibebankan biaya administrasi sesuai dengan ketentuan yang berlaku di Jejak Imani</li><li>Apabila saat berangkat jumlah jamaah tidak memenuhi paket standar quad (sekamar ber-4), maka calon jamaah akan dikenakan biaya paket sesuai jumlah jamaah yang ada, yaitu harga paket sekamar ber-3 (triple) atau ber-2 (double)</li></ol>"}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "section-4",
            "json" => '{"name": "Perhatian!","text":"<p>Jejak Imani memfasilitasi keberangkatan Haji Plus/Haji Khusus dengan masa tunggu yang lebih singkat (dibanding haji reguler) dengan fasilitas bintang lima (⭐️⭐️⭐️⭐️⭐️). Jadwal keberangkatan Haji Plus/Haji Khusus&nbsp;ditentukan oleh BPIH/Kementerian Agama RI.</p><p>Jika ingin berangkat haji lebih cepat (di tahun ini), Anda dapat memilih program haji furoda’.</p>", "phone": "628119513223", "text_wa": "Assalamu’alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.", "label_wa": "Klik Disini Untuk Berangkat Haji Lebih Cepat", "event": ""}',
            "created_by" => 1
        ]);

        foreach (range(1, 8) as $i) {
            DB::table('haji')->insert([
                'slug' => "furoda",
                "type" => "section-list-1",
                "json" => '{"image": "upload/haji/section-list-1/default copy/default-' . $i . '.jpg", "name": "Menu-' . $i . '","text":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry' . 's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}',
                "created_by" => 1

            ]);
        }

        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "section-list-2",
            "json" => '{"name": "HARI PERTAMA - 4 DZULHIJJAH : JAKARTA - JEDDAH - AZIZIYAH","text":"<ul><li>Jamaah berkumpul di Hotel <strong>Bandara Soekarno Hatta</strong> – Jakarta 8 (delapan) jam sebelum keberangkatan.</li><li>Pada waktu yang ditentukan, jamaah akan bertolak menuju <strong>Jeddah</strong>.</li><li>Setelah pemeriksaan imigrasi jamaah melanjutkan perjalanan ke apartemen transit di <strong>Aziziyah</strong> dengan menggunakan kendaraan yang telah kami siapkan.</li><li><em>Check in</em> apartemen dan persiapan umrah.</li></ul>"}',
            "created_by" => 1
        ]);

        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "section-list-2",
            "json" => '{"name": "HARI KEDUA - 5 DZULHIJJAH : AZIZIYAH - APARTEMEN TRANSIT - UMRAH","text":"<ul><li>Thawaf, sa’i dan tahallul.</li><li>Kegiatan selama di <strong>Makkah</strong> setelah umrah adalah : jamaah memperbanyak ibadah dan istirahat <strong>persiapan haji</strong> di <strong>Apartemen Transit.</strong></li><li>Mengikuti seluruh program yang sudah dijadwalkan oleh Jejak Imani.</li><li>Shalat berjamaah, baca dzikir pagi &amp; petang, mengaji bersama.</li><li>Kajian pemaknaan haji.</li></ul>"}',
            "created_by" => 1
        ]);

        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "section-list-2",
            "json" => '{"name": "HARI KETIGA - 6 DZULHIJJAH : AZIZIYAH - APARTEMEN TRANSIT - CITY TOUR MAKKAH","text":"<ul><li>Kegiatan selama di <strong>Makkah</strong> setelah umrah adalah : jamaah memperbanyak ibadah dan istirahat <strong>persiapan haji</strong> di <strong>Apartemen Transit</strong>.</li><li>Mengikuti seluruh program yang sudah dijadwalkan oleh Jejak Imani.</li><li>Ziarah Kota <strong>Makkah</strong> (City tour napak tilas haji). <b>Jabal Tsur, ARMINA, Miqat di Masjid Ji’ranah, Jabal Nur.</b></li><li>Shalat berjamaah, baca dzikir pagi &amp; petang, mengaji bersama.</li><li>Pemantapan fiqh thaharah dan shalat.</li></ul>"}',
            "created_by" => 1
        ]);

        foreach (range(1, 6) as $i) {
            DB::table('haji')->insert([
                'slug' => "furoda",
                "type" => "section-list-3",
                "json" => '{"image": "upload/haji/section-list-3/default copy/default-' . $i . '.jpg", "name": "Judul-' . $i . '","text":"Ini deskripsi ' . $i . '"}',
                "created_by" => 1
            ]);
        }

        DB::table('haji')->insert([
            'slug' => "furoda",
            "type" => "section-list-4",
            "json" => '{"name": "BIAYA TIDAK TERMASUK","text":"<ol><li>Tiket pesawat PP kelas ekonomi</li><li>Visa haji/biaya ONH Plus kuota Kemenag</li><li>Asuransi syariah</li><li>Bimbingan manasik haji di Indonesia dan Arab Saudi</li><li>Perlengkapan ibadah haji</li><li>Hotel ⭐️ 5 dan apartemen Aziziyah sesuai program</li><li>Makan 3 kali sehari <em>f</em><em>ullboard</em></li><li>Transportasi bus AC selama perjalanan ibadah haji di Tanah Suci</li><li>Pembimbing dan muthawwif berpengalaman dengan <em>background</em> syari’ah</li><li>Penggunaan radiophone selama program</li><li>Air zam-zam 5 liter per jamaah</li></ol>"}',
            "created_by" => 1
        ]);


        // Haji Parent

        DB::table('haji')->insert([
            'slug' => "parent",
            "type" => "banner",
            "json" => "upload/haji/banner/default/banner-1.jpg",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "parent",
            "type" => "banner",
            "json" => "upload/haji/banner/default/banner-2.jpg",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "parent",
            "type" => "banner",
            "json" => "upload/haji/banner/default/banner-3.png",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "parent",
            "type" => "label_banner",
            "json" => '{"name":"<h1>HAJI LEBIH NYAMAN BERSAMA JEJAK IMANI</h1>", "text":"“Barang siapa hendak melaksanakan ibadah Haji, hendaklah segera ia lakukan, karena terkadang seseorang itu sakit, atau binatang (kendaraannya) hilang, dan adanya suatu hajat yang menghalanginya untuk menunaikan ibadah Haji.” (HR. Ibnu Majah)", "phone":"628119513223", "text_wa":"Assalamu’alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.", "label_wa":"Klik Disini Untuk Konsultasi Gratis", "event": ""}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "parent",
            "type" => "haji_plus",
            "json" => "<h2>HAJI PLUS / HAJI KHUSUS</h2><p>Waktu Tunggu : 5-8 Tahun<br> Keberangkatan : 4 Dzulhijjah (Tentatif)<br> Tenda Mina – Jamarat : Maktab 111-115<br> Hotel Makkah : Daar Al Ghufron / Safwa Tower/ Setaraf ⭐️⭐️⭐️⭐️⭐️<br> Hotel Madinah : Shaza Al Madinah / Mövenpick / Setaraf ⭐️⭐️⭐️⭐️⭐️<br> Transit : Apartemen Transit<br> Maskapai : Saudi Arabian Airlines / Garuda Indonesia</p><p><strong>DP : $5.000 / +- Rp 72.500.000,- (Kurs Rp 14.500)</strong></p><p>Perkiraan Harga : Quad : $13.500, Triple : $14.250, Double : $15.000.</p><p>*Kepastian besaran biaya pelunasan keberangkatan menyesuaikan dengan biaya akomodasi haji khusus pada tahun keberangkatan</p><p><em><strong>Jejak Imani</strong> | Layanan Paket Travel Haji Umroh Resmi Terbaik Exclusive Berkualitas di Jakarta Bogor Depok Tangerang Bekasi Bandung Jabodetabek</em></p>",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "parent",
            "type" => "haji_furoda",
            "json" => "<h2>HAJI FURODA / MANDIRI</h2><p>Tidak ada waktu tunggu. Bisa berangkat tahun ini juga<br> Keberangkatan : Bulan Dzulhijjah<br> Tenda Mina – Jamarat : Maktab 79 / 111-115<br> Hotel Makkah : Swiss Al Maqam / Setaraf ⭐️⭐️⭐️⭐️⭐️<br> Hotel Madinah : Frontel ex Mercure / Dallah Thaibah / Annokhbah / Setaraf ⭐️⭐️⭐️⭐️⭐️<br> Transit : Apartemen Transit<br> Maskapai : Saudi Arabian Airlines / Garuda Indonesia</p><p><strong>DP : $8.500 (refundable) / +- Rp 123.250.000,- (Kurs Rp 14.500)</strong></p><p>Perkiraan Harga : Quad : $17.600, Triple : $18.600, Double : $19.600.<br> *Harga tahun 2021</p>",
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "parent",
            "type" => "testimoni",
            "json" => '{"image":"upload/haji/testimoni/default/default-1.png","text":"Insya Allah nanti saya akan rekomendasikan Jejak Imani ke rekan rekan yang lain, Alhamdulillaah kami sekeluarga bisa menjalani ibadah dengan mudah dan merasa seperti bersama keluarga sendiri.","name":"Capt. Megah Putra Prakasa","position":"Senior Captain Pilot Batik Air"}',
            "created_by" => 1
        ]);
        DB::table('haji')->insert([
            'slug' => "parent",
            "type" => "testimoni",
            "json" => '{"image":"upload/haji/testimoni/default/default-1.png","text":"Insya Allah nanti saya akan rekomendasikan Jejak Imani ke rekan rekan yang lain, Alhamdulillaah kami sekeluarga bisa menjalani ibadah dengan mudah dan merasa seperti bersama keluarga sendiri.","name":"Demo 2","position":"Senior Captain Pilot Batik Air"}',
            "created_by" => 1
        ]);
    }
}
