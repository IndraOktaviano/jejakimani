<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class contact_us extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content')->insert([
            'slug' => "contact-us",
            'type' => "description",
            "json" => 'Untuk Konsultasi Umrah silakan kontak kami di',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "contact-us",
            'type' => "head-office",
            "json" => 'Jl. Siliwangi No.4, Pd. Benda, Kec. Pamulang, Kota Tangerang Selatan, Banten 15417',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "contact-us",
            'type' => "cabang",
            "json" => '{"name":"Jogjakarta","text":"Jl. Salakan 2 No.222, Saman, Bangunharjo, Kec. Sewon, Bantul, Daerah Istimewa Yogyakarta 55188"}',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "contact-us",
            'type' => "cabang",
            "json" => '{"name":"Contoh Aja","text":"Jl. Yaya 2 No.222, Saman, Bangunharjo, Kec. Sewon, Bantul, Daerah Istimewa 55188"}',
            "created_by" => 1
        ]);
    }
}
