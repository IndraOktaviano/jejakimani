<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class article extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article')->truncate();
        
        DB::table('article')->insert([
            'name' => "Ingin Wisata Ke Turki Pada 2022? Lihat Dulu Yuk Destinasi Menariknya!",
            "slug" => "ingin-wisata-ke-turki-pada-2022-lihat-dulu-yuk-destinasi-menariknya",
            "image" => "upload/article/default/default-1.jpg",
            "json" => '<p>Wisata Turki 2022 – Wisata Turki terkenal dengan destinasi yang sangat menawan. Turki adalah negara yang bertempat di benua Eropa dan Asia, menyajikan perpaduan keindahan antara budaya klasik dan modern.</p>',
            "created_by" => 1,
            "status" => 1,
            "created_at" => date("Y-m-" . mt_rand(1, 28) . " H:i:s")
        ]);

        DB::table('article')->insert([
            'name' => "Jadwal Paket Umroh 2022 Berangkat Lebih Nyaman Bersama Jejak Imani",
            "slug" => "jadwal-paket-umroh-2022-berangkat-lebih-nyaman-bersama-jejak-imani",
            "image" => "upload/article/default/default-2.jpg",
            "json" => '<p>Jadwal Umroh 2022 Jakarta – Sesuaikan jadwal keberangkatan Umroh 2022 Anda bersama Jejak Imani dan dapatkan yang lebih terjangkau, paket perjalanan umroh yang istimewa akan kami berikan untuk Anda para tamu-tamu Allah.</p>',
            "created_by" => 1,
            "status" => 1,
            "created_at" => date("Y-m-" . mt_rand(1, 28) . " H:i:s")
        ]);
    }
}
