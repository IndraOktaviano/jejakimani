<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class sosial_media extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content')->insert([
            'slug' => "sosial-media",
            'type' => "email",
            "json" => 'jejakimani@gmail.com',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "sosial-media",
            'type' => "twitter",
            "json" => 'jejakimani',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "sosial-media",
            'type' => "facebook",
            "json" => 'jejakimani',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "sosial-media",
            'type' => "youtube",
            "json" => 'UCx095g0HfU_-_czn8n1MLUA',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "sosial-media",
            'type' => "instagram",
            "json" => 'JejakImani',
            "created_by" => 1
        ]);

        DB::table('content')->insert([
            'slug' => "sosial-media",
            'type' => "whatsapp",
            "json" => '{"phone":"628119513223","text":"Assalamualaikum Mas Bustomi Saya lihat dari web jejakimani , mau tanya"}',
            "created_by" => 1
        ]);
    }
}
