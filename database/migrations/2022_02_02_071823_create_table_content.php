<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->id();
            $table->enum("slug", ["about-us", "contact-us", "faq", "sosial-media", "jejak-islami"]);
            $table->string("type", 25);
            $table->longText("json")->nullable();
            $table->enum("active", [1, 0]);
            $table->timestamps();
            $table->integer('created_by', false, true)->nullable();
            $table->integer('updated_by', false, true)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_content');
    }
}
