<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEventTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tracker', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('page');
            $table->string('type')->nullable();
            $table->string('event')->nullable();
            $table->string('event_label')->nullable();
            $table->string('event_category')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_tracker');
    }
}
