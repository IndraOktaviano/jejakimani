<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSorterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sorter', function (Blueprint $table) {
            $table->id();
            $table->enum("slug", ["haji-furoda", "haji-khusus", "about-us", "jejak-islami"]);
            $table->string("type", 25);
            $table->text("json");
            $table->timestamps();
            $table->integer('created_by', false, true)->nullable();
            $table->integer('updated_by', false, true)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sorter');
    }
}
