<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHajiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haji', function (Blueprint $table) {
            $table->id();
            $table->enum("slug", ["furoda", "khusus", "parent"]);
            // $table->enum("type", ["banner", "label_banner", "header", "section-header-1", "section-header-2", "section-header-3", "section-header-4", "section-body-1", "section-body-2", "section-body-3", "section-body-4",]);
            $table->string("type", 25);
            $table->longText("json")->nullable();
            $table->enum("active", [1, 0]);
            $table->timestamps();
            $table->integer('created_by', false, true)->nullable();
            $table->integer('updated_by', false, true)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('haji');
    }
}
