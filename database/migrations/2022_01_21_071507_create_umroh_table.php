<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUmrohTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umroh', function (Blueprint $table) {
            $table->id();
            $table->integer("umroh_category_id", false, true);
            $table->string("name", 50);
            $table->string("slug", 50)->unique();
            $table->float("price", 11, 2);
            $table->date("schedule");
            $table->string("image", "255");
            $table->string("maskapai", "255");
            $table->timestamps();
            $table->integer('created_by', false, true)->nullable();
            $table->integer('updated_by', false, true)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umroh');
    }
}
