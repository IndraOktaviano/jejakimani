<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $send['auth'] = auth()->user();
        $send['title'] = "Dashboard";
        return view('home', $send);
    }

    public function tinymce_upload(Request $request)
    {
        $image = $request->file("file");
        if ($image) {
            $imageName = 'upload/article/content/' . nameFile($image);
            if (!$image->move(public_path('upload/article/content'), $imageName)) {
                Session::flash('error', 'Terjadi kesalahan saat upload gambar');
                http_response_code(400);
            }

            return response()->json(["location" => asset($imageName)]);
        }
    }
}
