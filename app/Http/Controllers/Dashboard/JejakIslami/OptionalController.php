<?php

namespace App\Http\Controllers\Dashboard\JejakIslami;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OptionalController extends Controller
{
    private $slug = "jejak-islami";
    private $type = "optional";
    private $title = "Opsional - Jejak Islami";
    private $route = "cms.content.jejak.islami.optional";

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = $this->title;
        $send['data'] = Content::where("slug", $this->slug)->where('type', $this->type)->first();
        return view('dashboard.content.jejak-islami.optional.index', $send);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Content::where("slug", $this->slug)->where('type', $this->type)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        try {
            DB::beginTransaction();            
            $data->json = $request->text;
            $data->save();
            DB::commit();
            Session::flash("success", "Perbarui " . $this->title . " Berhasil");
            return redirect()->route($this->route . '.index');
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Content::where('slug', $this->slug)->where('type', $this->type)->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            http_response_code(400);
        }

        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            http_response_code(200);
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }
}
