<?php

namespace App\Http\Controllers\Dashboard\JejakIslami;

use App\Http\Controllers\Controller;
use App\Models\Sorter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SorterJejakIslamiController extends Controller
{
    public function update(Request $request)
    {
        $data = Sorter::where('slug', $request->slug)->where('type', $request->type)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back();
        }

        $data->json = json_encode($request->id);

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            return redirect()->route($request->route.'.index')
                ->with("success", "Perbarui Urutan Berhasil");
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back();
        }
    }
}
