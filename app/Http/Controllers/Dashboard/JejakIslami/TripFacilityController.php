<?php

namespace App\Http\Controllers\Dashboard\JejakIslami;

use App\Http\Controllers\Dashboard\JejakIslamiController as BASE;
use App\Models\Content;
use App\Models\Sorter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TripFacilityController extends BASE
{
    private $slug = "jejak-islami";
    private $type = "trip-facility";
    private $title = "Fasilitas Perjalan - Jejak Islami";
    private $route = "cms.content.jejak.islami.trip-facility";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = $this->title;
        $send['route'] = $this->route;
        $send['data'] = Content::where("slug", $this->slug,)->where('type', $this->type)->get();
        $send['order'] = sorter($send['data'], $this->slug, $this->type, true);
        return view('dashboard.content.jejak-islami.trip-facility.index', $send);
    }

    public function create()
    {
        $send['auth'] = Auth::user();
        $send['title'] = $this->title;
        return view('dashboard.content.jejak-islami.trip-facility.create', $send);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Content;

        try {
            DB::beginTransaction();
            $data->slug = $this->slug;
            $data->type = $this->type;
            $json = [
                "name" => $request->name,
                "icon" => $request->icon,
            ];
            $data->json = json_encode($json);
            $data->save();

            $order = Sorter::where('slug', $this->slug)->where('type', $this->type)->first();
            if ($order) {
                $arr = json_decode($order->json);
                array_push($arr, $data->id);
                $order->json = json_encode($arr);
                $order->save();
            }
            DB::commit();
            Session::flash("success", "Tambah " . $this->title . " Berhasil");
            return redirect()->route($this->route . '.index');
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = $this->title;
        $send['data'] = Content::where("slug", $this->slug,)->where('type', $this->type)->where("id", $id)->first();
        return view('dashboard.content.jejak-islami.trip-facility.edit', $send);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Content::where('slug', $this->slug)->where('type', $this->type)->where("id", $id)->first();

        $json = json_decode($data->json);

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        try {
            DB::beginTransaction();
            $detail = [];
            if (isset($json->detail) && count($json->detail)) {
                foreach (range(0, 5) as $i) {
                    array_push($detail, [
                        "location" => $request->location[$i],
                        "name" => $request->name_detail[$i],
                    ]);
                }
            }
            $json = [
                "name" => $request->name,
                "icon" => $request->icon,
            ];
            if ($detail) {
                $json = [
                    "name" => $request->name,
                    "icon" => $request->icon,
                    "detail" => $detail,
                ];
            }
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            Session::flash("success", "Perbarui " . $this->title . " Berhasil");
            return redirect()->route($this->route . '.index');
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Content::where('slug', $this->slug)->where('type', $this->type)->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            http_response_code(400);
        }

        try {
            DB::beginTransaction();
            $data->delete();
            $order = Sorter::where('slug', $this->slug)->where('type', $this->type)->first();
            if ($order) {
                $arr = json_decode($order->json);
                foreach ($arr as $key => $value) {
                    if ($value == $id) {
                        unset($arr[$key]);
                    }
                }
                $order->json = json_encode($arr);
                $order->save();
            }
            DB::commit();
            http_response_code(200);
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }
}
