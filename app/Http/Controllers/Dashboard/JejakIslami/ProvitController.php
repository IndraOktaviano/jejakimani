<?php

namespace App\Http\Controllers\Dashboard\JejakIslami;

use App\Http\Controllers\Dashboard\JejakIslamiController as BASE;
use App\Models\Content;
use App\Models\Sorter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProvitController extends BASE
{
    private $slug = "jejak-islami";
    private $type = "provit";
    private $title = "Keunggulan - Jejak Islami";
    private $route = "cms.content.jejak.islami.provit";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = $this->title;
        $send['route'] = $this->route;
        $send['data'] = Content::where("slug", $this->slug,)->where('type', $this->type)->get();
        $send['order'] = sorter($send['data'], $this->slug, $this->type, true);
        return view('dashboard.content.jejak-islami.provit.index', $send);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $send['auth'] = Auth::user();
        $send['title'] = $this->title;
        return view('dashboard.content.jejak-islami.provit.create', $send);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Content;

        $image = $request->file("image");
        $imageName = 'upload/jejak-islami/' . $this->slug . '/' . nameFile($image);

        $order = Sorter::where('slug', $request->slug)->where('type', $request->type)->first();
        $arr = json_decode($order->json);

        try {
            DB::beginTransaction();
            $image->move(public_path('upload/jejak-islami/' . $this->slug . ''), $imageName);
            $data->slug = $this->slug;
            $data->type = $this->type;
            $json = [
                "image" => $imageName,
                "name" => $request->name,
            ];
            $data->json = json_encode($json);
            $data->save();

            $order = Sorter::where('slug', $this->slug)->where('type', $this->type)->first();
            if ($order) {
                $arr = json_decode($order->json);
                array_push($arr, $data->id);
                $order->json = json_encode($arr);
                $order->save();
            }
            DB::commit();
            return redirect()->route($this->route . '.index')
                ->with("success", "Tambah " . $this->title . " Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = $this->title;
        $send['data'] = Content::where("slug", $this->slug,)->where('type', $this->type)->where("id", $id)->first();
        return view('dashboard.content.jejak-islami.provit.edit', $send);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Content::where('slug', $this->slug)->where('type', $this->type)->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        $image = $request->file("image");
        $imageName = json_decode($data->json)->image;
        if ($image) {
            $imageName = 'upload/jejak-islami/section-1/' . nameFile($image);
        }

        try {
            DB::beginTransaction();
            if ($image) {
                deletedFile(json_decode($data->json)->image);
                $image->move(public_path('upload/jejak-islami/section-1'), $imageName);
            }
            $json = [
                "image" => $imageName,
                "name" => $request->name,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route($this->route . '.index')
                ->with("success", "Perbarui " . $this->title . " Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Content::where('slug', $this->slug)->where('type', $this->type)->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            http_response_code(400);
        }

        try {
            DB::beginTransaction();
            deletedFile(json_decode($data->json)->image);
            $data->delete();
            $order = Sorter::where('slug', $this->slug)->where('type', $this->type)->first();
            if ($order) {
                $arr = json_decode($order->json);
                foreach ($arr as $key => $value) {
                    if ($value == $id) {
                        unset($arr[$key]);
                    }
                }
                $order->json = json_encode($arr);
                $order->save();
            }
            DB::commit();
            http_response_code(200);
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }
}
