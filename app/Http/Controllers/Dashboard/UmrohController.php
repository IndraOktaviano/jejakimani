<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\EventTracker;
use App\Models\Umroh;
use App\Models\UmrohCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UmrohController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application umroh.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Umroh";
        $send['data'] = Umroh::all();
        $send['tracker_home'] = EventTracker::where('slug', 'umroh')->where('page', 'home')->first();
        $send['tracker_detail'] = EventTracker::where('slug', 'umroh')->where('page', 'detail')->first();
        return view('dashboard.umroh.index', $send);
    }

    public function create()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Umroh";
        $send['category'] = UmrohCategory::all();
        return view('dashboard.umroh.create', $send);
    }

    public function store(Request $request)
    {
        $image = $request->file("image");
        $imageName = 'upload/umroh/' . nameFile($image);
        $maskapai = $request->file("maskapai");
        $maskapaiName = 'upload/umroh/maskapai/' . nameFile($maskapai);
        $price = str_replace(".", "", $request->price);
        $price = str_replace(",", ".", $price);
        $slug = $request->slug;
        try {
            DB::beginTransaction();
            $data = new Umroh;
            if (!$maskapai->move(public_path('upload/umroh/maskapai'), $maskapaiName)) {
                Session::flash('error', 'Terjadi kesalahan saat upload gambar');
                return redirect()->back()
                    ->withInput($request->all());
            }
            if (!$image->move(public_path('upload/umroh'), $imageName)) {
                Session::flash('error', 'Terjadi kesalahan saat upload gambar');
                return redirect()->back()
                    ->withInput($request->all());
            }
            $data->name = $request->name;
            $data->umroh_category_id = $request->category;
            $data->price = $price;
            $data->schedule = $request->schedule;
            $data->maskapai = $maskapaiName;
            $data->image = $imageName;
            $data->slug = "unknow";
            $data->save();
            if (!$slug) {
                $slug = str_replace(' ', '_', strtolower(UmrohCategory::where("id", $request->category)->first()->name)) . "-" . $data->id;
            } else {
                $slug = str_replace(' ', '_', strtolower($slug));
            }
            $data->slug = $slug;
            $data->save();
            DB::commit();
            return redirect()->route('cms.umroh.index')
                ->with("success", "Tambah Umroh Berhasil");
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function edit($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Umroh";
        $send['data'] = Umroh::where("id", $id)->first();
        if (!$send['data']) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back();
        }
        $send['category'] = UmrohCategory::all();
        return view('dashboard.umroh.edit', $send);
    }

    public function update(Request $request)
    {

        $image = $request->file("image");
        $maskapai = $request->file("maskapai");
        if ($image) {
            $imageName = 'upload/umroh/' . nameFile($image);
            if (!$image->move(public_path('upload/umroh'), $imageName)) {
                Session::flash('error', 'Terjadi kesalahan saat upload gambar');
                return redirect()->back()
                    ->withInput($request->all());
            }
        }
        if ($maskapai) {
            $maskapaiName = 'upload/umroh/maskapai' . nameFile($maskapai);
            if (!$maskapai->move(public_path('upload/umroh/maskapai'), $maskapaiName)) {
                Session::flash('error', 'Terjadi kesalahan saat upload gambar');
                return redirect()->back()
                    ->withInput($request->all());
            }
        }
        $price = str_replace(".", "", $request->price);
        $price = str_replace(",", ".", $price);
        $slug = $request->slug;
        if (!$slug) {
            $slug = str_replace(' ', '_', strtolower(UmrohCategory::where("id", $request->category)->first()->name) . "-" . $request->id);
        } else {
            $slug = str_replace(' ', '_', strtolower($slug));
        }
        try {
            DB::beginTransaction();
            $data = Umroh::where('id', $request->id)->first();
            $data->name = $request->name;
            $data->umroh_category_id = $request->category;
            $data->price = $price;
            $data->slug = $slug;
            $data->schedule = $request->schedule;
            if ($image) {
                deletedFile($data->image);
                $data->image = $imageName;
            }
            if ($maskapai) {
                deletedFile($data->maskapai);
                $data->maskapai = $maskapaiName;
            }
            $data->save();
            DB::commit();
            return redirect()->route('cms.umroh.index')
                ->with("success", "Perbaharui Umroh Berhasil");
        } catch (\Exception $e) {

            DB::rollback();
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function show($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Umroh";
        $send['data'] = Umroh::where("id", $id)->first();
        if (!$send['data']) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back();
        }
        $send['category'] = UmrohCategory::all();
        // return view('dashboard.umroh.edit', $send);
    }

    public function destroy(Request $request)
    {
        $data = Umroh::where("id", $request->id)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }
        try {
            DB::beginTransaction();
            if (!str_contains($data->maskapai, "/default")) {
                unlink(public_path($data->maskapai));
            }
            if (!str_contains($data->image, "/default")) {
                unlink(public_path($data->image));
            }
            $data->delete();
            DB::commit();
            http_response_code(200);
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function store_tracker(Request $request)
    {
        try {
            DB::beginTransaction();
            foreach ($request->event as $i => $value) {
                EventTracker::updateOrCreate(
                    [
                        'slug' => 'umroh',
                        'page' => $request->page[$i],
                    ],
                    [
                        'slug' => 'umroh',
                        'page' => $request->page[$i],
                        'event' => $request->event[$i],
                        'event_label' => $request->event_label[$i],
                        'event_category' => $request->event_category[$i],
                    ],
                );
            }
            DB::commit();
            return redirect()->route('cms.umroh.index')
                ->with("success", "Perbaharui Tracker Umroh Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return redirect()->route('cms.umroh.index')
                ->with('error', 'Terjadi kesalahan saat menyimpan data');
        }
    }
}
