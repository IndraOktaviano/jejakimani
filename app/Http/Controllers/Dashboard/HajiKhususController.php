<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Haji;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HajiKhususController extends Controller
{
    protected $slug = "khusus";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function banner()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Banner Haji Khusus";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "banner")->get();
        return view('dashboard.haji.banner.index', $send);
    }

    public function create_banner()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Banner Haji Khusus";
        $send['condition'] = "Tambah";
        $send['slug'] = $this->slug;
        $send['data'] = [];
        return view('dashboard.haji.banner.store', $send);
    }

    public function edit_banner($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Banner Haji Khusus";
        $send['condition'] = "Perbarui";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where('id', $id)->where("slug", $this->slug)->where("type", "banner")->first();

        if (!$send['data']) {
            return  redirect()->route('cms.haji.khusus.banner.index')->with("error", "Data tidak ditemukan");
        }
        return view('dashboard.haji.banner.store', $send);
    }

    public function label_banner()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Label Banner Haji Khusus";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "label_banner")->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.label-banner.index', $send);
    }

    public function section_1()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section 1 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "section-1")->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.section-1.index', $send);
    }

    public function section_2()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section 2 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "section-2")->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.section-2.index', $send);
    }

    public function section_3()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section 3 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "section-3")->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.section-3.index', $send);
    }

    public function section_4()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section 4 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "section-4")->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.section-4.index', $send);
    }

    // Section List 1 -------------------
    public function section_list_1()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 1 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['type'] = 'section-list-1';
        $send['route'] = '.section.list.1';
        $send['no'] = '1';
        $send['image'] = true;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", $send['type'])->get();
        $send['order'] = sorter($send['data'], "haji-".$this->slug, $send['type'], true);

        return view('dashboard.haji.section-list.index', $send);
    }

    public function create_section_list_1()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 1 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['condition'] = "Tambah";
        $send['type'] = 'section-list-1';
        $send['route'] = '.section.list.1';
        $send['image'] = true;
        $send['no'] = '1';
        $send['data'] = [];
        $send['json'] = [];

        return view('dashboard.haji.section-list.store', $send);
    }

    public function edit_section_list_1($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 1 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['condition'] = "Perbarui";
        $send['image'] = true;
        $send['no'] = '1';
        $send['type'] = 'section-list-1';
        $send['route'] = '.section.list.1';
        $send['data'] = Haji::where("id", $id)->where("slug", $this->slug)->where("type", $send['type'])->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.section-list.store', $send);
    }


    // Section List 2 -------------------
    public function section_list_2()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 2 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['type'] = 'section-list-2';
        $send['route'] = '.section.list.2';
        $send['no'] = '2';
        $send['image'] = false;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", $send['type'])->get();
        $send['order'] = sorter($send['data'], "haji-".$this->slug, $send['type'], true);

        return view('dashboard.haji.section-list.index', $send);
    }

    public function create_section_list_2()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 2 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['condition'] = "Tambah";
        $send['type'] = 'section-list-2';
        $send['route'] = '.section.list.2';
        $send['image'] = false;
        $send['no'] = '2';
        $send['data'] = [];
        $send['json'] = [];

        return view('dashboard.haji.section-list.store', $send);
    }

    public function edit_section_list_2($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 2 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['condition'] = "Perbarui";
        $send['image'] = false;
        $send['no'] = '2';
        $send['type'] = 'section-list-2';
        $send['route'] = '.section.list.2';
        $send['data'] = Haji::where("id", $id)->where("slug", $this->slug)->where("type", $send['type'])->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.section-list.store', $send);
    }

    // Section List 3 -------------------
    public function section_list_3()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 3 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['type'] = 'section-list-3';
        $send['route'] = '.section.list.3';
        $send['no'] = '3';
        $send['image'] = true;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", $send['type'])->get();
        $send['order'] = sorter($send['data'], "haji-".$this->slug, $send['type'], true);

        return view('dashboard.haji.section-list.index', $send);
    }

    public function create_section_list_3()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 3 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['condition'] = "Tambah";
        $send['type'] = 'section-list-3';
        $send['route'] = '.section.list.3';
        $send['image'] = true;
        $send['no'] = '3';
        $send['data'] = [];
        $send['json'] = [];

        return view('dashboard.haji.section-list.store', $send);
    }

    public function edit_section_list_3($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 3 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['condition'] = "Perbarui";
        $send['image'] = true;
        $send['no'] = '3';
        $send['type'] = 'section-list-3';
        $send['route'] = '.section.list.3';
        $send['data'] = Haji::where("id", $id)->where("slug", $this->slug)->where("type", $send['type'])->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.section-list.store', $send);
    }

    // Section List 4 -------------------
    public function section_list_4()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 4 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['type'] = 'section-list-4';
        $send['route'] = '.section.list.4';
        $send['no'] = '4';
        $send['image'] = false;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", $send['type'])->get();
        $send['order'] = sorter($send['data'], "haji-".$this->slug, $send['type'], true);

        return view('dashboard.haji.section-list.index', $send);
    }

    public function create_section_list_4()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 4 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['condition'] = "Tambah";
        $send['type'] = 'section-list-4';
        $send['route'] = '.section.list.4';
        $send['image'] = false;
        $send['no'] = '4';
        $send['data'] = [];
        $send['json'] = [];

        return view('dashboard.haji.section-list.store', $send);
    }

    public function edit_section_list_4($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section List 4 Haji Khusus";
        $send['slug'] = $this->slug;
        $send['condition'] = "Perbarui";
        $send['image'] = false;
        $send['no'] = '4';
        $send['type'] = 'section-list-4';
        $send['route'] = '.section.list.4';
        $send['data'] = Haji::where("id", $id)->where("slug", $this->slug)->where("type", $send['type'])->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.section-list.store', $send);
    }
}
