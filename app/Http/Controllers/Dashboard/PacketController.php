<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Other;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PacketController extends Controller
{
    private $slug = "packet";
    private $type = "lainnya";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Paket Lainnya";
        $send['data'] = Other::where('slug', $this->slug)->where('type', $this->type)->get();
        return view('dashboard.other.packet.index', $send);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Paket Lainnya";
        return view('dashboard.other.packet.create', $send);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file("image");
        $imageName = 'upload/other/packet/' . nameFile($image);

        try {
            DB::beginTransaction();
            $data = new Other;
            $image->move(public_path('upload/other/packet'), $imageName);
            $data->slug = $this->slug;
            $data->type = $this->type;
            $json = [
                "url" => $request->url ?? "#",
                "image" => $imageName
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.other.packet.index')
                ->with("success", "Tambah Paket Lainnya Berhasil");
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Paket Lainnya";
        $send['data'] = Other::where('slug', $this->slug)->where('type', $this->type)->where("id", $id)->first();
        if (!$send['data']) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->route('cms.other.packet.index');
        }
        return view('dashboard.other.packet.edit', $send);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Other::where("id", $id)->where('slug', $this->slug)->where('type', $this->type)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }
        $image = $request->file("image");
        $json = json_decode($data->json);
        $imageName = $json->image;
        if ($image) {
            $imageName = 'upload/other/packet/' . nameFile($image);
        }

        try {
            DB::beginTransaction();
            if ($image) {
                $image->move(public_path('upload/other/packet'), $imageName);
            }
            $json = [
                "url" => $request->url ?? "#",
                "image" => $imageName
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.other.packet.index')
                ->with("success", "Perbarui Paket Lainnya Berhasil");
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Other::where("id", $id)->where('slug', $this->slug)->where('type', $this->type)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }

        $json = json_decode($data->json);

        try {
            DB::beginTransaction();
            deletedFile($json->image);
            $data->delete();
            DB::commit();
            http_response_code(200);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Session::flash('eror', 'Terjadi kesalahan');
            http_response_code(400);
        }
    }
}
