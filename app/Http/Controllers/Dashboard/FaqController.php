<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $slug = "faq";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "FAQ";
        $send['data'] = Content::where('slug', $this->slug)->where('type', 'list')->get();
        return view('dashboard.content.faq.index', $send);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Tambah FAQ";
        return view('dashboard.content.faq.create', $send);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Content;

        try {
            DB::beginTransaction();
            $data->slug = $this->slug;
            $data->type = "list";
            $json = [
                "name" => $request->name,
                "text" => $request->text,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.content.faq.list.index')
                ->with("success", "Tambah FAQ Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Perbarui FAQ";
        $send['data'] = Content::where('slug', $this->slug)->where('type', 'list')->where('id', $id)->first();
        return view('dashboard.content.faq.edit', $send);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Content::where("id", $request->id)->first();

        if(!$data){
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        try {
            DB::beginTransaction();
            $json = [
                "name" => $request->name,
                "text" => $request->text,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.content.faq.list.index')
                ->with("success", "Perbarui FAQ Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $data = Content::where("id", $request->id)->first();

        if(!$data){
            Session::flash('warning', 'Data tidak ada ditemukan');
            http_response_code(400);
        }

        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            http_response_code(200);
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function phone()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Perbarui Kontak";
        $send['data'] = Content::where('slug', $this->slug)->where('type', 'phone')->first();
        return view('dashboard.content.faq.phone', $send);
    }

    public function update_phone(Request $request)
    {
        $data = Content::where('slug', $this->slug)->where('type', 'phone')->first();

        if(!$data){
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        try {
            DB::beginTransaction();            
            $data->json = $request->phone;
            $data->save();
            DB::commit();
            return redirect()->route('cms.content.faq.phone.index')
                ->with("success", "Perbarui Kontak FAQ Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }
}
