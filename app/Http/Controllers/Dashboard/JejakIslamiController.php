<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class JejakIslamiController extends Controller
{
    private $slug = "jejak-islami";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function section1()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Section 1 - Jejak Islami";
        $send['data'] = Content::where("slug", $this->slug,)->where('type', "section-1")->first();
        return view('dashboard.content.jejak-islami.section-1.index', $send);
    }

    public function update_section1(Request $request, $id)
    {
        $data = Content::where('slug', $this->slug)->where('type', 'section-1')->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        $image = $request->file("image");
        $imageName = json_decode($data->json)->image;
        if ($image) {
            $imageName = 'upload/jejak-islami/section-1/' . nameFile($image);
        }

        try {
            DB::beginTransaction();
            if ($image) {
                deletedFile(json_decode($data->json)->image);
                $image->move(public_path('upload/jejak-islami/section-1'), $imageName);
            }
            $json = [
                "image" => $imageName,
                "price" => $request->price,
                "name" => $request->name,
                "phone" => $request->phone,
                "text_wa" => rawurlencode($request->text_wa),
                "label_wa" => $request->label_wa,
                "event" => $request->event,
                "event_label" => $request->event_label,
                "event_category" => $request->event_category,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.content.jejak.islami.section-1.index')
                ->with("success", "Perbarui Section 1 Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }
}
