<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SosialMediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Sosial Media";
        $send['data'] = sosial_media();
        return view('dashboard.content.sosial-media.index', $send);
    }

    public function store(Request $request)
    {
        $email = Content::where('slug', 'sosial-media')->where('type', 'email')->first();
        $facebook = Content::where('slug', 'sosial-media')->where('type', 'facebook')->first();
        $twitter = Content::where('slug', 'sosial-media')->where('type', 'twitter')->first();
        $youtube = Content::where('slug', 'sosial-media')->where('type', 'youtube')->first();
        $instagram = Content::where('slug', 'sosial-media')->where('type', 'instagram')->first();
        $whatsapp = Content::where('slug', 'sosial-media')->where('type', 'whatsapp')->first();

        try {
            DB::beginTransaction();
            // set data
            $email->json = $request->email;
            $facebook->json = $request->facebook;
            $twitter->json = $request->twitter;
            $youtube->json = $request->youtube;
            $instagram->json = $request->instagram;
            $whatsapp->json = json_encode(["phone" => $request->whatsapp, "text" => $request->text]);
            // save data
            $email->save();
            $facebook->save();
            $twitter->save();
            $youtube->save();
            $instagram->save();
            $whatsapp->save();
            DB::commit();
            return redirect()->route('cms.content.sosial.media.index')
                ->with("success", "Perbarui Sosial Media Berhasil");
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }
}
