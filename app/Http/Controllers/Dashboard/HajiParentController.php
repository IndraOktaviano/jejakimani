<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Haji;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HajiParentController extends Controller
{
    protected $slug = "parent";

    public function banner()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Banner Haji Khusus";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "banner")->get();
        return view('dashboard.haji.banner.index', $send);
    }

    public function create_banner()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Banner Haji Khusus";
        $send['condition'] = "Tambah";
        $send['slug'] = $this->slug;
        $send['data'] = [];
        return view('dashboard.haji.banner.store', $send);
    }

    public function edit_banner($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Banner Haji Khusus";
        $send['condition'] = "Perbarui";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where('id', $id)->where("slug", $this->slug)->where("type", "banner")->first();

        if (!$send['data']) {
            return  redirect()->route('cms.haji.khusus.banner.index')->with("error", "Data tidak ditemukan");
        }
        return view('dashboard.haji.banner.store', $send);
    }

    public function label_banner()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Label Banner Haji Khusus";
        $send['slug'] = $this->slug;
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "label_banner")->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.label-banner.index', $send);
    }

    public function haji_khusus()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Content Haji Khusus";
        $send['slug'] = $this->slug;
        $send['type'] = 'haji_plus';
        $send['route'] = '.haji_khusus';
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "haji_plus")->first();

        return view('dashboard.haji.haji-content.index', $send);
    }

    public function haji_furoda()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Content Haji Furoda";
        $send['slug'] = $this->slug;
        $send['type'] = 'haji_furoda';
        $send['route'] = '.haji_furoda';
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "haji_furoda")->first();

        return view('dashboard.haji.haji-content.index', $send);
    }

    public function testimoni()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Testimoni";
        $send['slug'] = $this->slug;
        $send['type'] = 'testimoni';
        $send['data'] = Haji::where("slug", $this->slug)->where("type", "testimoni")->get();

        return view('dashboard.haji.testimoni.index', $send);
    }

    public function testimoni_create()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Testimoni";
        $send['slug'] = $this->slug;
        $send['type'] = 'testimoni';
        $send['condition'] = "Tambah";
        $send['data'] = [];
        $send['json'] = [];

        return view('dashboard.haji.testimoni.store', $send);
    }

    public function testimoni_edit(Request $request, $id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Testimoni";
        $send['slug'] = $this->slug;
        $send['type'] = 'testimoni';
        $send['condition'] = "Perbaharui";
        $send['data'] = Haji::where("id", $id)->where("slug", $this->slug)->where("type", $send['type'])->first();
        $send['json'] = json_decode($send['data']->json);

        return view('dashboard.haji.testimoni.store', $send);
    }
}
