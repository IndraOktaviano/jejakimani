<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ContactUsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function description()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Hubungi Kami - Deskripsi";
        $send['type'] = "description";
        $send['data'] = Content::where("slug", 'contact-us',)->where('type', $send['type'])->first();
        return view('dashboard.content.contact-us.index', $send);
    }

    public function head_office()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Hubungi Kami - Head Office";
        $send['type'] = "head-office";
        $send['data'] = Content::where("slug", 'contact-us',)->where('type', $send['type'])->first();
        return view('dashboard.content.contact-us.index', $send);
    }

    public function store(Request $request)
    {
        $data = Content::where("slug", 'contact-us',)->where('type', $request->type)->first();

        $data->json = $request->data;

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            return redirect()->back()
                ->with("success", "Perbarui " . ucwords(str_replace("-", " ", $request->type)) . " Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function cabang()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Hubungi Kami - Cabang";
        $send['data'] = Content::where("slug", 'contact-us',)->where('type', "cabang")->get();
        return view('dashboard.content.contact-us.cabang', $send);
    }

    public function store_cabang(Request $request)
    {
        $data = new Content;

        $data->slug = "contact-us";
        $data->type = "cabang";
        $data->json = json_encode(["text" => $request->text, "name" => $request->name]);

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            Session::flash("success", "Tambah Cabang Berhasil");
            http_response_code(200);
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function update_cabang(Request $request)
    {
        $data = Content::where("id", $request->id)->where("slug", 'contact-us',)->where('type', "cabang")->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }

        $data->json = json_encode(["text" => $request->text, "name" => $request->name]);

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            Session::flash("success", "Perbarui Cabang Berhasil");
            http_response_code(200);
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function destroy_cabang(Request $request)
    {
        $data = Content::where("id", $request->id)->where("slug", 'contact-us',)->where('type', "cabang")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }
        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            http_response_code(200);
        } catch (\Exception $e) {

            DB::rollback();
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    
    public function mitra()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Hubungi Kami - Mitra";
        $send['data'] = Content::where("slug", 'contact-us',)->where('type', "mitra")->get();
        return view('dashboard.content.contact-us.mitra', $send);
    }

    public function store_mitra(Request $request)
    {
        $data = new Content;

        $data->slug = "contact-us";
        $data->type = "mitra";
        $data->json = json_encode(["text" => $request->text, "name" => $request->name, "phone" => $request->phone]);        

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            Session::flash("success", "Tambah Mitra Berhasil");
            http_response_code(200);
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function update_mitra(Request $request)
    {
        $data = Content::where("id", $request->id)->where("slug", 'contact-us',)->where('type', "mitra")->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }

        $data->json = json_encode(["text" => $request->text, "name" => $request->name, "phone" => $request->phone]);        

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            Session::flash("success", "Perbarui Mitra Berhasil");
            http_response_code(200);
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function destroy_mitra(Request $request)
    {
        $data = Content::where("id", $request->id)->where("slug", 'contact-us',)->where('type', "mitra")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }
        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            http_response_code(200);
        } catch (\Exception $e) {

            DB::rollback();
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }
}
