<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Artikel";
        $send['data'] = Article::get();
        return view('dashboard.article.index', $send);
    }

    public function create()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Tambah Artikel";
        return view('dashboard.article.create', $send);
    }

    public function store(Request $request)
    {
        $image = $request->file("image");
        $imageName = 'upload/article/' . nameFile($image);

        try {
            DB::beginTransaction();
            $data = new Article;
            $image->move(public_path('upload/article'), $imageName);
            $data->name = $request->name;
            $data->slug = strtolower(str_replace(" ", "_", $request->slug)) ?? strtolower(str_replace(" ", "_", $request->name));
            $data->image = $imageName;
            $data->json = $request->text;
            $data->save();
            DB::commit();
            return redirect()->route('cms.article.index')
                ->with("success", "Tambah Artikel Berhasil");
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function edit($id)
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Perbarui Artikel";
        $send['data'] = Article::where('id', $id)->first();
        return view('dashboard.article.edit', $send);
    }

    public function update(Request $request)
    {

        $data = Article::where("id", $request->id)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }
        $image = $request->file("image");
        $imageName = $data->image;
        if ($image) {
            $imageName = 'upload/article/' . nameFile($image);
        }

        try {
            DB::beginTransaction();
            if ($image) {
                deletedFile($data->image);                
                $image->move(public_path('upload/article'), $imageName);
            }
            $data->name = $request->name;
            $data->slug = strtolower(str_replace(" ", "_", $request->slug)) ?? strtolower(str_replace(" ", "_", $request->name));
            $data->image = $imageName;
            $data->json = $request->text;
            $data->save();
            DB::commit();
            return redirect()->route('cms.article.index')
                ->with("success", "Tambah Artikel Berhasil");
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function destroy(Request $request)
    {
        $data = Article::where("id", $request->id)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }

        try {
            DB::beginTransaction();
            deletedFile($data->image);
            $data->delete();
            DB::commit();
            http_response_code(200);
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('eror', 'Terjadi kesalahan');
            http_response_code(400);
            DB::rollBack();
        }
    }
}
