<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Umroh;
use App\Models\UmrohCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UmrohCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application umroh.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Kategori Umroh";
        $send['data'] = UmrohCategory::where('id', ">", 1)->get();
        return view('dashboard.umroh category.index', $send);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = new UmrohCategory();
            $data->name = $request->name;
            $data->text = $request->name;
            $data->save();
            DB::commit();
            Session::flash('success', 'Tambah Kategori Berhasil');
            http_response_code(200);
        } catch (\Exception $e) {

            DB::rollback();
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function update(Request $request)
    {
        $data = UmrohCategory::where('id', $request->id)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }
        try {
            DB::beginTransaction();
            $data->name = $request->name;
            $data->text = $request->text;
            $data->save();
            DB::commit();
            Session::flash('success', 'Perbaharui Kategori Berhasil');
            http_response_code(200);
        } catch (\Exception $e) {

            DB::rollback();
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function destroy(Request $request)
    {
        $data = UmrohCategory::where("id", $request->id)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
        }
        try {
            DB::beginTransaction();
            if ($data->delete()) {
                $items = Umroh::where("umroh_category_id", $request->id)->get();
                if ($items) {
                    foreach ($items as $item) {
                        $sub = Umroh::where('id', $item->id)->first();
                        $sub->umroh_category_id = 1;
                        $sub->slug = "tanpa-kategori-" . $item->id;
                        $sub->save();
                    }
                }
                DB::commit();
            }
        } catch (\Exception $e) {

            DB::rollback();
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }
}
