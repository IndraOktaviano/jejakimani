<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Haji;
use App\Models\Sorter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HajiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // for store and update banner
    public function store_banner(Request $request)
    {
        if (!$request->slug) {
            Session::flash('error', 'Terjadi kesalahan, silahkan coba kembali');
            return redirect()->back()
                ->withInput($request->all());
        }
        $image = $request->file("image");
        $imageName = 'upload/haji/banner/' . nameFile($image);

        if (!$request->id) {
            $data = new Haji;
            $data->slug = $request->slug;
            $data->type = "banner";
        } else {
            $data = Haji::where("id", $request->id);
        }
        $data->json = $imageName;

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            $image->move(public_path('upload/haji/banner'), $imageName);
            return redirect()->route('cms.haji.' . $request->slug . '.banner.index')
                ->with("success", "Tambah Banner Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function destroy_banner(Request $request)
    {
        $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", "banner")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back()->withInput($request->all());
        }
        try {
            DB::beginTransaction();
            deletedFile($data->json);
            $data->delete();
            DB::commit();
            Session::flash("success", "Hapus Banner Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function store_label_banner(Request $request)
    {
        $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", "label_banner")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back()->withInput($request->all());
        }
        $json = [
            "name" => $request->name,
            "text" => $request->text,
            "phone" => $request->phone,
            "text_wa" => rawurlencode($request->text_wa),
            "label_wa" => $request->label_wa,
            "event" => $request->event,
            "event_label" => $request->event_label,
            "event_category" => $request->event_category,
        ];

        try {
            DB::beginTransaction();
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            Session::flash("success", "Hapus Banner Haji " . ucwords($request->slug) . " Berhasil");
            return redirect()->route('cms.haji.' . $request->slug . '.label.banner.index')
                ->with("success", "Perbarui Label Banner Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function store_section_1(Request $request)
    {
        $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", "section-1")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back()->withInput($request->all());
        }

        $image = $request->file("image");
        $imageName = json_decode($data->json)->image;
        if ($image) {
            $imageName = 'upload/haji/banner/' . nameFile($image);
        }

        try {
            DB::beginTransaction();
            if ($image) {
                deletedFile(json_decode($data->json)->image);
                $image->move(public_path('upload/haji/banner'), $imageName);
            }
            $json = [
                "text" => $request->text,
                "image" => $imageName
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.haji.' . $request->slug . '.section.1.index')
                ->with("success", "Perbarui Section 1 Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function store_section_2(Request $request)
    {
        $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", "section-2")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back()->withInput($request->all());
        }

        $image = $request->file("image");
        $imageName = json_decode($data->json)->image;
        if ($image) {
            $imageName = 'upload/haji/banner/' . nameFile($image);
        }

        $price = str_replace(".", "", $request->price);
        $price = str_replace(",", ".", $price);

        try {
            DB::beginTransaction();
            if ($image) {
                deletedFile(json_decode($data->json)->image);
                $image->move(public_path('upload/haji/banner'), $imageName);
            }
            $json = [
                "name" => $request->name,
                "price" => $price,
                "label_price" => $request->label_price,
                "phone" => $request->phone,
                "text_wa" => rawurlencode($request->text_wa),
                "label_wa" => $request->label_wa,
                "text" => $request->text,
                "image" => $imageName,
                "event" => $request->event,
                "event_label" => $request->event_label,
                "event_category" => $request->event_category,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.haji.' . $request->slug . '.section.2.index')
                ->with("success", "Perbarui Section 2 Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function store_section_3(Request $request)
    {
        $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", "section-3")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back()->withInput($request->all());
        }
        try {
            DB::beginTransaction();
            $json = [
                "name" => $request->name,
                "text" => $request->text,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.haji.' . $request->slug . '.section.3.index')
                ->with("success", "Perbarui Section 3 Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function store_section_4(Request $request)
    {
        $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", "section-4")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back()->withInput($request->all());
        }
        try {
            DB::beginTransaction();
            $json = [
                "name" => $request->name,
                "text" => $request->text,
                "phone" => $request->phone,
                "text_wa" => rawurlencode($request->text_wa),
                "label_wa" => $request->label_wa,
                "event" => $request->event,
                "event_label" => $request->event_label,
                "event_category" => $request->event_category,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.haji.' . $request->slug . '.section.4.index')
                ->with("success", "Perbarui Section 4 Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function store_section_list(Request $request)
    {
        $image = $request->file("image");
        $imageName = "";
        $old = null;
        $order = false;
        if (!$request->id) {
            $data = new Haji;
            $data->slug = $request->slug;
            $data->type = $request->type;
            $order = Sorter::where('slug', "haji-" . $request->slug)->where('type', $request->type)->first();
            $arr = json_decode($order->json);
        } else {
            $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", $request->type)->first();
            if (!$data) {
                Session::flash('warning', 'Data tidak ditemukan');
                return redirect()->back()->withInput($request->all());
            }
            if ($request->is_image) {
                $imageName = json_decode($data->json)->image;
                $old = $imageName;
            }
        }

        if ($request->is_image && !$image && !$request->id) {
            Session::flash('warning', 'Gambar Wajib dilampirkan');
            return redirect()->back()->withInput($request->all());
        }
        if ($image) {
            $imageName = 'upload/haji/section-list-' . $request->no . '/' . nameFile($image);
        }

        $json = [
            "name" => $request->name,
            "text" => $request->text,
        ];
        try {
            DB::beginTransaction();
            if ($image) {
                $image->move(public_path('upload/haji/section-list-' . $request->no . ''), $imageName);
            }
            if ($request->is_image) {
                $json['image'] = $imageName;
            }
            $data->json = json_encode($json);
            $data->save();
            if ($order) {
                array_push($arr, $data->id);
                $order->json = json_encode($arr);
                $order->save();
            }
            if ($old != $imageName) {
                deletedFile($old);
            }
            DB::commit();
            return redirect()->route('cms.haji.' . $request->slug . $request->route . '.index')
                ->with("success",  $request->id ? "Perbaharui" : "Tambah" . "Section List " . $request->no . " Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function destroy_section_list(Request $request)
    {
        $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", $request->type)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            dd("Data tidak ditemukan");
        }
        $image = json_decode($data->json)->image;
        $order = Sorter::where('slug', "haji-" . $request->slug)->where('type', $request->type)->first();
        $arr = json_decode($order->json);
        foreach ($arr as $key => $value) {
            if ($value == $request->id) {
                unset($arr[$key]);
            }
        }
        $order->json = json_encode($arr);
        try {
            DB::beginTransaction();
            deletedFile($image);
            $data->delete();
            $order->save();
            DB::commit();
            Session::flash("success", "Hapus Section List " . $request->no . " Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
        }
    }

    public function order_section_list(Request $request)
    {
        $data = Sorter::where('slug', "haji-" . $request->slug)->where('type', $request->type)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back();
        }

        $data->json = json_encode($request->id);

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            return redirect()->route('cms.haji.' . $request->slug . $request->route . '.index')
                ->with("success", "Perbarui Urutan Section List " . $request->no . " Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back();
        }
    }

    public function haji_content_store(Request $request)
    {
        $data = Haji::where('slug', $request->slug)->where('type', $request->type)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            return redirect()->back();
        }
        try {
            DB::beginTransaction();
            $data->json = $request->text;
            $data->save();
            DB::commit();
            return redirect()->route('cms.haji.' . $request->slug . $request->route . '.index')
                ->with("success", "Perbarui Content" . ucwords(str_replace('.', '', str_replace('_', ' ', $request->route))) . " Berhasil");
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back();
        }
    }

    public function store_testimoni(Request $request)
    {
        $image = $request->file("image");
        $imageName = "";
        $old = null;
        if (!$request->id) {
            $data = new Haji;
            $data->slug = $request->slug;
            $data->type = $request->type;
        } else {
            $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", $request->type)->first();
            if (!$data) {
                Session::flash('warning', 'Data tidak ditemukan');
                return redirect()->back()->withInput($request->all());
            }
            $imageName = json_decode($data->json)->image;
            $old = $imageName;
        }

        if (!$image && !$request->id) {
            Session::flash('warning', 'Gambar Wajib dilampirkan');
            return redirect()->back()->withInput($request->all());
        }
        if ($image) {
            $imageName = 'upload/haji/testimoni/' . nameFile($image);
        }

        $json = [
            "name" => $request->name,
            "text" => $request->text,
            "position" => $request->position
        ];
        try {
            DB::beginTransaction();
            if ($image) {
                $image->move(public_path('upload/haji/testimoni'), $imageName);
            }
            $json['image'] = $imageName;
            $data->json = json_encode($json);
            $data->save();
            if ($old != $imageName) {
                deletedFile($old);
            }
            DB::commit();
            return redirect()->route('cms.haji.' . $request->slug . '.testimoni.index')
                ->with("success", $request->id ? "Perbaharui" : "Tambah" . "Testimoni Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function destroy_testimoni(Request $request)
    {
        $data = Haji::where("id", $request->id)->where("slug", $request->slug)->where("type", $request->type)->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            dd("Data tidak ditemukan");
        }
        $image = json_decode($data->json)->image;
        $order = Sorter::where('slug', "haji-" . $request->slug)->where('type', $request->type)->first();
        $arr = json_decode($order->json);
        foreach ($arr as $key => $value) {
            if ($value == $request->id) {
                unset($arr[$key]);
            }
        }
        $order->json = json_encode($arr);
        try {
            DB::beginTransaction();
            deletedFile($image);
            $data->delete();
            $order->save();
            DB::commit();
            Session::flash("success", "Hapus Section List " . $request->no . " Haji " . ucwords($request->slug) . " Berhasil");
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan');
        }
    }
}
