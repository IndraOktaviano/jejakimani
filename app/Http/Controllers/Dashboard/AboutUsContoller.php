<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AboutUsContoller extends Controller
{
    private $slug = "about-us";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sejarah()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Perbarui Sejarah";
        $send['data'] = Content::where('slug', $this->slug)->where('type', 'sejarah')->first();
        return view('dashboard.content.about-us.sejarah.index', $send);
    }

    public function update_sejarah(Request $request, $id)
    {
        $data = Content::where('slug', $this->slug)->where('type', 'sejarah')->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        $image = $request->file("image");
        $imageName = json_decode($data->json)->image;
        if ($image) {
            $imageName = 'upload/about-us/' . nameFile($image);
        }

        try {
            DB::beginTransaction();
            if ($image) {
                deletedFile(json_decode($data->json)->image);
                $image->move(public_path('upload/about-us'), $imageName);
            }
            $json = [
                "image" => $imageName,
                "text" => $request->text,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.content.about.us.sejarah.index')
                ->with("success", "Perbarui Sejarah Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function visi()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Perbarui Sejarah";
        $send['data'] = Content::where('slug', $this->slug)->where('type', 'visi')->first();
        return view('dashboard.content.about-us.visi.index', $send);
    }

    public function update_visi(Request $request, $id)
    {
        $data = Content::where('slug', $this->slug)->where('type', 'visi')->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        try {
            DB::beginTransaction();
            $data->json = $request->text;
            $data->save();
            DB::commit();
            return redirect()->route('cms.content.about.us.visi.index')
                ->with("success", "Perbarui Visi Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function misi()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Perbarui Sejarah";
        $send['data'] = Content::where('slug', $this->slug)->where('type', 'misi')->first();
        return view('dashboard.content.about-us.misi.index', $send);
    }

    public function update_misi(Request $request, $id)
    {
        $data = Content::where('slug', $this->slug)->where('type', 'misi')->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        try {
            DB::beginTransaction();
            $data->json = $request->text;
            $data->save();
            DB::commit();
            return redirect()->route('cms.content.about.us.misi.index')
                ->with("success", "Perbarui Visi Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }

    public function eksekutif()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Eksekutif";
        $send['data'] = Content::where('slug', $this->slug)->where('type', 'eksekutif')->get();
        return view('dashboard.content.about-us.eksekutif.index', $send);
    }

    public function store_eksekutif(Request $request)
    {
        $data = new Content;

        $data->slug = $this->slug;
        $data->type = "eksekutif";
        $data->json = json_encode(["text" => $request->text, "name" => $request->name, "jabatan" => $request->jabatan]);

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            Session::flash("success", "Tambah Eksekutif Berhasil");
            http_response_code(200);
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function update_eksekutif(Request $request)
    {
        $data = Content::where('slug', $this->slug)->where('type', 'eksekutif')->where("id", $request->id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }

        $data->json = json_encode(["text" => $request->text, "name" => $request->name, "jabatan" => $request->jabatan]);

        try {
            DB::beginTransaction();
            $data->save();
            DB::commit();
            Session::flash("success", "Perbarui Eksekutif Berhasil");
            http_response_code(200);
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollBack();
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function destroy_eksekutif(Request $request)
    {
        $data = Content::where("id", $request->id)->where("slug", $this->slug)->where('type', "eksekutif")->first();
        if (!$data) {
            Session::flash('warning', 'Data tidak ditemukan');
            http_response_code(400);
        }
        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            http_response_code(200);
        } catch (\Exception $e) {

            DB::rollback();
            /* Transaction failed. */
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            http_response_code(400);
        }
    }

    public function sambutan()
    {
        $send['auth'] = Auth::user();
        $send['title'] = "Perbarui Sambutan";
        $send['data'] = Content::where('slug', $this->slug)->where('type', 'sambutan')->first();
        return view('dashboard.content.about-us.sambutan.index', $send);
    }

    public function update_sambutan(Request $request, $id)
    {
        $data = Content::where('slug', $this->slug)->where('type', 'sambutan')->where("id", $id)->first();

        if (!$data) {
            Session::flash('warning', 'Data tidak ada ditemukan');
            return redirect()->back()
                ->withInput($request->all());
        }

        $image = $request->file("image");
        $imageName = json_decode($data->json)->image;
        if ($image) {
            $imageName = 'upload/about-us/' . nameFile($image);
        }

        try {
            DB::beginTransaction();
            if ($image) {
                deletedFile(json_decode($data->json)->image);
                $image->move(public_path('upload/about-us'), $imageName);
            }
            $json = [
                "image" => $imageName,
                "text" => $request->text,
            ];
            $data->json = json_encode($json);
            $data->save();
            DB::commit();
            return redirect()->route('cms.content.about.us.sambutan.index')
                ->with("success", "Perbarui Sambutan Berhasil");
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            Session::flash('error', 'Terjadi kesalahan saat menyimpan data');
            return redirect()->back()
                ->withInput($request->all());
        }
    }
}
