<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    // public function login(Request $request)
    // {

    //     $this->validateLogin($request);

    //     if ($this->attemptLogin($request)) {
    //         Session::flash('success', 'Login berhasil');
    //         return $this->sendLoginResponse($request);
    //     }
    // }

    // protected function attemptLogin(Request $request)
    // {
    //     $email = $request->email;
    //     $pass = $request->password;

    //     $data = User::where('email', $email)->first();
    //     if ($data) {
    //         // dd($data->remember_token);
    //         if (Hash::check($pass, $data->password)) {

    //             Session::flash('success', 'Berhasil login');
    //             return $this->guard()->attempt(
    //                 $this->credentials($request)
    //             );
    //         }
    //     }
    //     Session::flash('error', 'Email atau Password salah');
    //     return $this->sendFailedLoginResponse($request);
    // }
}
