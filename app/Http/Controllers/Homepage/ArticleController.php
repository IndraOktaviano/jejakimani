<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $send['sosial_media'] = sosial_media();
        $send['article'] = Article::get();
        return view('homepage.article', $send);
    }

    public function view($slug)
    {
        $send['sosial_media'] = sosial_media();
        $send['article'] = Article::where('slug', $slug)->first();
        $send['others'] = Article::whereNotIn('slug', [$slug])->limit(3)->get();
        return view('homepage.view-article', $send);
    }
}
