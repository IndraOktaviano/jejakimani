<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TabunganUmrahController extends Controller
{
    public function index()
    {
        $send['other_packet'] = other_packet();
        $send['sosial_media'] = sosial_media();
        return view('homepage.tabungan-umrah', $send);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
