<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $send['description'] = Content::where('slug', 'contact-us')->where('type', 'description')->first();
        $send['head_office'] = Content::where('slug', 'contact-us')->where('type', 'head-office')->first();
        $send['cabang'] = Content::where('slug', 'contact-us')->where('type', 'cabang')->get();
        $send['sosial_media'] = sosial_media();
        return view('homepage.contact', $send);
    }
}
