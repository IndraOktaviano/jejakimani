<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $send['sosial_media'] = sosial_media();
        $send['sejarah'] = json_decode(Content::where('slug', 'about-us')->where('type', 'sejarah')->first()->json);
        $send['visi'] = Content::where('slug', 'about-us')->where('type', 'visi')->first();
        $send['misi'] = Content::where('slug', 'about-us')->where('type', 'misi')->first();
        $send['sambutan'] = json_decode(Content::where('slug', 'about-us')->where('type', 'sambutan')->first()->json);
        $send['eksekutif'] = sorter(Content::where('slug', 'about-us')->where('type', 'eksekutif')->get(), 'about-us', 'eksekutif');
        return view('homepage.about', $send);
    }
}
