<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Models\Haji;
use Illuminate\Http\Request;

class HajiController extends Controller
{
    private $slug = 'parent'; 

    public function index() {
        $send['other_packet'] = other_packet();
        $send['sosial_media'] = sosial_media();
        $send['banner'] = Haji::where('slug', $this->slug)->where('type', 'banner')->get();        
        $send['label_banner'] = json_decode(Haji::where('slug', $this->slug)->where('type', 'label_banner')->first()->json);
        $send['haji_plus'] = Haji::where('slug', $this->slug)->where('type', 'haji_plus')->first()->json;
        $send['haji_furoda'] = Haji::where('slug', $this->slug)->where('type', 'haji_furoda')->first()->json;
        $send['testimoni'] = Haji::where('slug', $this->slug)->where('type', 'testimoni')->get();
        return view('homepage.haji-jejak-imani', $send);
    }
}
