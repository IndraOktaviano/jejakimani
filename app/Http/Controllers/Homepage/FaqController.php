<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index()
    {
        $send['sosial_media'] = sosial_media();
        return view('homepage.faq', $send);
    }
}
