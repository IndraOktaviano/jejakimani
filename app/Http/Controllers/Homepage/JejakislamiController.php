<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class JejakislamiController extends Controller
{
    protected $slug = "jejak-islami";

    public function index()
    {
        $send['sosial_media'] = sosial_media();
        $send['section_1'] = json_decode(Content::where('slug', $this->slug)->where('type', 'section-1')->first()->json);
        $send['provit'] = sorter(Content::where('slug', $this->slug)->where('type', 'provit')->get(), $this->slug, 'provit');
        $send['trip_plan'] = sorter(Content::where('slug', $this->slug)->where('type', 'trip-plan')->get(), $this->slug, 'trip-plan');
        $send['optional'] = Content::where('slug', $this->slug)->where('type', 'optional')->first()->json;
        $send['trip_facility'] = sorter(Content::where('slug', $this->slug)->where('type', 'trip-facility')->get(), $this->slug, 'trip-facility');
        $send['no_include'] = sorter(Content::where('slug', $this->slug)->where('type', 'no-include')->get(), $this->slug, 'no-include');
        return view('homepage.jejakislami', $send);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
