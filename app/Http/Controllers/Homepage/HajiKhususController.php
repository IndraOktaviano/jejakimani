<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Models\Haji;
use Illuminate\Http\Request;

class HajiKhususController extends Controller
{
    protected $slug = "khusus";

    public function index()
    {
        $send['sosial_media'] = sosial_media();
        $send['other_packet'] = other_packet();
        $send['banner'] = Haji::where('slug', $this->slug)->where('type', 'banner')->get();
        $send['label_banner'] = json_decode(Haji::where('slug', $this->slug)->where('type', 'label_banner')->first()->json);
        $send['section_1'] = json_decode(Haji::where('slug', $this->slug)->where('type', 'section-1')->first()->json);
        $send['section_2'] = json_decode(Haji::where('slug', $this->slug)->where('type', 'section-2')->first()->json);
        $send['section_3'] = json_decode(Haji::where('slug', $this->slug)->where('type', 'section-3')->first()->json);
        $send['section_4'] = json_decode(Haji::where('slug', $this->slug)->where('type', 'section-4')->first()->json);
        $send['section_list_1'] = sorter(Haji::where('slug', $this->slug)->where('type', 'section-list-1')->get(), "haji-".$this->slug, 'section-list-1');
        $send['section_list_2'] = sorter(Haji::where('slug', $this->slug)->where('type', 'section-list-2')->get(), "haji-".$this->slug, 'section-list-2');
        $send['section_list_3'] = sorter(Haji::where('slug', $this->slug)->where('type', 'section-list-3')->get(), "haji-".$this->slug, 'section-list-3');
        $send['section_list_4'] = sorter(Haji::where('slug', $this->slug)->where('type', 'section-list-4')->get(), "haji-".$this->slug, 'section-list-4');
        return view('homepage.haji-khusus', $send);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
