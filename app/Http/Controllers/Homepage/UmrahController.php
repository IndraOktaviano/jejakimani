<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Models\EventTracker;
use App\Models\Umroh;
use App\Models\UmrohCategory;
use Illuminate\Http\Request;

class UmrahController extends Controller
{
    public function index()
    {
        $send['other_packet'] = other_packet();
        $send['sosial_media'] = sosial_media();
        $send['category'] = UmrohCategory::get();
        $send['tracker'] = EventTracker::where('slug', 'umroh')->where('page', 'home')->first();
        return view('homepage.paket-umrah', $send);
    }

    public function badalUmrah()
    {
        $send['other_packet'] = other_packet();
        $send['sosial_media'] = sosial_media();
        return view('homepage.badal-umrah', $send);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($slug)
    {
        $send['other_packet'] = other_packet();
        $send['sosial_media'] = sosial_media();
        $send['tracker'] = EventTracker::where('slug', 'umroh')->where('page', 'detail')->first();
        $send['data']  = Umroh::where('slug', $slug)->first();
        return view('homepage.detail-paket', $send);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
