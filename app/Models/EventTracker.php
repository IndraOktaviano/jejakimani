<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventTracker extends Model
{
    use HasFactory;
    protected $table = 'event_tracker';
    protected $guarded = [];
}
