<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Umroh extends Model
{
    use HasFactory;

    protected $table = 'umroh';

    public function category(){
        return $this->belongsTo(UmrohCategory::class, "umroh_category_id", "id");
    }
    public function getCategoryAttribute(){
        if($this->category()->exists()){
            return $this->category()->first();
        }
        return null;
    }
}
