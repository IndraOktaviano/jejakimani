<?php

//Membuat generate nama file dg format tahun bulan tanggal jam menit

use App\Models\Content;
use App\Models\Other;
use App\Models\Sorter;

function nameFile($file)
{
    if (isset($file)) {
        $extention = $file->getClientOriginalExtension();
        $nameFile = date('YmdHis') . '-' . strtoupper(Str::random(5)) . '.' . $extention;
        return $nameFile;
    }
}

// deleteFile if exists
function deletedFile($path)
{
    if (!str_contains($path, "default") || !str_contains($path, "http")) {
        if (file_exists(public_path($path)) && $path) {
            unlink(public_path($path));
        }
    }
}

function ordered($slug, $type)
{
    return json_decode(Sorter::where("slug", $slug)->where("type", $type)->first()->json);
}

function sorter($data, $slug, $type, $only_name = false)
{
    $arr = [];
    $order = ordered($slug, $type);
    foreach ($order as $item) {
        foreach ($data as $val) {
            if ($val->id == $item) {
                $json = json_decode($val->json);
                if ($only_name) {
                    array_push($arr, ["id" => $val->id, "name" => $json->name]);
                } else {
                    array_push($arr, $val);
                }
            }
        }
    }
    return $arr;
}

function sosial_media()
{
    $email = Content::where('slug', 'sosial-media')->where('type', 'email')->first();
    $facebook = Content::where('slug', 'sosial-media')->where('type', 'facebook')->first();
    $twitter = Content::where('slug', 'sosial-media')->where('type', 'twitter')->first();
    $youtube = Content::where('slug', 'sosial-media')->where('type', 'youtube')->first();
    $instagram = Content::where('slug', 'sosial-media')->where('type', 'instagram')->first();
    $whatsapp = Content::where('slug', 'sosial-media')->where('type', 'whatsapp')->first();

    return [
        "email" => $email,
        "facebook" => $facebook,
        "twitter" => $twitter,
        "youtube" => $youtube,
        "instagram" => $instagram,
        "whatsapp" => $whatsapp,
    ];
}

function other_packet()
{
    return Other::where('slug', 'packet')->where('type', 'lainnya')->get()->toArray();
}
