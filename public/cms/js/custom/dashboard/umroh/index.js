/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
    var __webpack_exports__ = {};
    /*!*************************************************************************************!*\
      !*** ../../../themes/metronic/html/demo1/src/js/custom/apps/umrohs/list/list.js ***!
      \*************************************************************************************/


    // Class definition
    var KTCustomersList = function () {
        // Define shared variables
        var datatable;
        var table

        // Private functions
        var initJS = function () {


            // Init datatable --- more info on datatables: https://datatables.net/manual/
            datatable = $(table).DataTable({
                "info": false,
                'order': [],
                'columnDefs': [
                    { orderData: 6, targets: 5 },
                    { orderable: false, targets: 6 },
                    { orderable: false, targets: 7 },
                    { orderable: false, targets: 8 },
                ]
            });

            // Re-init functions on every table re-draw -- more info: https://datatables.net/reference/event/draw
            datatable.on('draw', function () {
                handleDeleteRows();
            });
        }

        // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
        var handleSearchDatatable = () => {
            const filterSearch = document.querySelector('[data-kt-umroh-table-filter="search"]');
            filterSearch.addEventListener('keyup', function (e) {
                datatable.search(e.target.value).draw();
            });
        }

        // Delete umroh
        var handleDeleteRows = () => {
            // Select all delete buttons
            const deleteButtons = table.querySelectorAll('[data-kt-umroh-table-filter="delete_row"]');

            deleteButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function (e) {
                    e.preventDefault();

                    // Select parent row
                    const parent = e.target.closest('tr');

                    // Get umroh name
                    const umrohName = parent.querySelectorAll('td')[2].innerText;
                    const data = parent.querySelectorAll('td')[8].childNodes[1].childNodes[3];
                    const id = data.dataset.id
                    const token = data.dataset.token

                    // SweetAlert2 pop up --- official docs reference: https://sweetalert2.github.io/
                    Swal.fire({
                        text: "Apakah anda yakin untuk mengahpus paket " + umrohName + "?",
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Hapus!",
                        cancelButtonText: "Batalkan",
                        customClass: {
                            confirmButton: "btn fw-bold btn-danger",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function (result) {
                        if (result.value) {
                            Swal.fire({
                                text: "Mohon tunggu, Data sedang diproses!",
                                icon: "info",
                                buttonsStyling: false,
                                timer: 2000,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            }).then(function () {
                                const url = `${window.location.href}/${id}`
                                console.log(url);
                                $.ajax({
                                    url: url,
                                    method: "DELETE",
                                    data: { _token: token, id: id },
                                    success: function () {
                                        toastr.success("Paket " + umrohName + " berhasil dihapus");
                                        datatable.row($(parent)).remove().draw();
                                    },
                                    error: function(){
                                        toastr.error("Terjadi kesalahan");
                                    }
                                })
                                // Remove current row
                                // datatable.row($(parent)).remove().draw();
                            });
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: "Hapus Paket " + umrohName + " dibatalkan.",
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        // Public methods
        return {
            init: function () {
                table = document.querySelector('#kt_umrohs_table');

                if (!table) {
                    return;
                }

                initJS();
                handleSearchDatatable();
                handleDeleteRows();
            }
        }
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KTCustomersList.init();
    });
    /******/
})()
    ;
//# sourceMappingURL=list.js.map