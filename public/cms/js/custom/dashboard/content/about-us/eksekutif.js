/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
    var __webpack_exports__ = {};
    /*!*************************************************************************************!*\
      !*** ../../../themes/metronic/html/demo1/src/js/custom/apps/categorys/list/list.js ***!
      \*************************************************************************************/


    // Class definition
    var KTCustomersList = function () {
        // Define shared variables
        var datatable;
        var table;
        var validator;

        // Private functions
        var initJS = function () {


            // Init datatable --- more info on datatables: https://datatables.net/manual/
            datatable = $(table).DataTable({
                "info": false,
                'order': [],
                'columnDefs': [
                    { orderable: false, targets: 0 },
                    { orderable: false, targets: 5 },
                    { searchable: false, targets: 0 },
                    { searchable: false, targets: 4 },
                    { searchable: false, targets: 5 },
                ]
            });

            // Re-init functions on every table re-draw -- more info: https://datatables.net/reference/event/draw
            datatable.on('draw', function () {
                handleDeleteRows();
            });
        }

        // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
        var handleSearchDatatable = () => {
            const filterSearch = document.querySelector('[data-kt-table-filter="search"]');
            filterSearch.addEventListener('keyup', function (e) {
                datatable.search(e.target.value).draw();
            });
        }

        // Create Handle
        var createForm = function (e) {
            // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
            let form = document.querySelector('#kt_create_form')
            validator = FormValidation.formValidation(
                form,
                {
                    fields: {
                        'name': {
                            validators: {
                                notEmpty: {
                                    message: 'Nama tidak boleh kosong'
                                }
                            },
                        },
                        'jabatan': {
                            validators: {
                                notEmpty: {
                                    message: 'Jabatan tidak boleh kosong'
                                }
                            },
                        },
                        'text': {
                            validators: {
                                notEmpty: {
                                    message: 'Deskripsi tidak boleh kosong'
                                }
                            },
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row'
                        })
                    }
                }
            );

            let submitButton = document.querySelector('#submit-create-modal')

            // Handle form submit
            submitButton.addEventListener('click', function (e) {
                // Prevent button default action
                e.preventDefault();

                // Validate form
                validator.validate().then(function (status) {
                    if (status == 'Valid') {
                        // Show loading indication
                        submitButton.setAttribute('data-kt-indicator', 'on');

                        // Disable button to avoid multiple click 
                        submitButton.disabled = true;


                        // Simulate ajax request
                        setTimeout(function () {
                            // Hide loading indication
                            submitButton.removeAttribute('data-kt-indicator');

                            // Enable button
                            submitButton.disabled = false;

                            // Show message popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                            Swal.fire({
                                text: "Mohon tunggu, Data sedang diproses!",
                                icon: "info",
                                buttonsStyling: false,
                                timer: 2000,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            }).then(function () {
                                // form.submit(); // submit form
                                const url = `${window.location.href}`
                                // const data = new FormData(document.getElementById('kt_create_form'))
                                const data = {
                                    name: form.querySelector("input[name='name']").value,
                                    _token: form.querySelector("input[name='_token']").value,
                                    jabatan: form.querySelector("input[name='jabatan']").value,
                                    text: form.querySelector("textarea[name='text']").value,
                                }
                                $.ajax({
                                    url: url,
                                    method: "POST",
                                    data: data,
                                    success: function () {
                                        window.location.reload()
                                    },
                                    error: function () {
                                        toastr.error("Terjadi kesalahan");
                                    }
                                })
                            });
                        }, 2000);
                    } else {
                        // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                        Swal.fire({
                            text: "Mohon perhatikan kembali isian form",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                    }
                });
            });
        }

        // Edit category
        var editForm = function (e) {
            // Select all edit buttons
            const editButtons = table.querySelectorAll('[data-kt-table-filter="edit_row"]');
            editButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function (e) {
                    e.preventDefault();
                    // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
                    // Select parent row
                    const parent = e.target.closest('tr');

                    document.getElementById("name-edit").value = parent.querySelectorAll('td')[1].innerText;
                    document.getElementById("id-edit").value = parent.querySelectorAll('td')[4].innerText;
                    document.getElementById("text-edit").innerText = parent.querySelectorAll('td')[3].innerText;
                    document.getElementById("jabatan-edit").value = parent.querySelectorAll('td')[2].innerText;
                    const form = document.querySelector('#kt_edit_form')
                    validator = FormValidation.formValidation(
                        form,
                        {
                            fields: {
                                'name': {
                                    validators: {
                                        notEmpty: {
                                            message: 'Nama tidak boleh kosong'
                                        }
                                    }
                                },
                                'jabatan': {
                                    validators: {
                                        notEmpty: {
                                            message: 'Jabatan tidak boleh kosong'
                                        }
                                    }
                                },
                                'text': {
                                    validators: {
                                        notEmpty: {
                                            message: 'Deskripsi tidak boleh kosong'
                                        }
                                    },
                                },
                            },
                            plugins: {
                                trigger: new FormValidation.plugins.Trigger(),
                                bootstrap: new FormValidation.plugins.Bootstrap5({
                                    rowSelector: '.fv-row'
                                })
                            }
                        }
                    );

                    const submitButton = document.querySelector('#submit-edit-modal')
                    // Handle form submit
                    submitButton.addEventListener('click', function (e) {
                        // Prevent button default action
                        e.preventDefault();

                        // Validate form
                        validator.validate().then(function (status) {
                            if (status == 'Valid') {
                                // Show loading indication
                                submitButton.setAttribute('data-kt-indicator', 'on');

                                // Disable button to avoid multiple click 
                                submitButton.disabled = true;


                                // Simulate ajax request
                                setTimeout(function () {
                                    // Hide loading indication
                                    submitButton.removeAttribute('data-kt-indicator');

                                    // Enable button
                                    submitButton.disabled = false;

                                    const data = {
                                        id: form.querySelector("input[name='id']").value,
                                        name: form.querySelector("input[name='name']").value,
                                        _token: form.querySelector("input[name='_token']").value,
                                        _method: form.querySelector("input[name='_method']").value,
                                        jabatan: form.querySelector("input[name='jabatan']").value,
                                        text: form.querySelector("textarea[name='text']").value,
                                    }

                                    // Show message popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                                    Swal.fire({
                                        text: "Mohon tunggu, Data sedang diproses!",
                                        icon: "info",
                                        buttonsStyling: false,
                                        timer: 2000,
                                        confirmButtonText: "Ok",
                                        customClass: {
                                            confirmButton: "btn btn-primary"
                                        }
                                    }).then(function (result) {
                                        // form.submit(); // submit form
                                        const url = `${window.location.href}`
                                        // const data = new FormData(document.getElementById('kt_create_form'))                                        
                                        $.ajax({
                                            url: url,
                                            method: "POST",
                                            data: data,
                                            success: function () {
                                                window.location.reload()
                                            },
                                            error: function () {
                                                toastr.error("Terjadi kesalahan");
                                            }
                                        })
                                    });
                                }, 2000);
                            } else {
                                // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                                Swal.fire({
                                    text: "Mohon perhatikan kembali isian form",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                            }
                        });
                    });
                })
            })
        }

        // Delete category
        var handleDeleteRows = () => {
            // Select all delete buttons
            const deleteButtons = table.querySelectorAll('[data-kt-table-filter="delete_row"]');

            deleteButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function (e) {
                    e.preventDefault();

                    // Select parent row
                    const parent = e.target.closest('tr');

                    // Get category name
                    const dataName = parent.querySelectorAll('td')[1].innerText;
                    const data = parent.querySelectorAll('td')[5].childNodes[1].childNodes[3];
                    const id = data.dataset.id
                    const token = data.dataset.token

                    // SweetAlert2 pop up --- official docs reference: https://sweetalert2.github.io/
                    Swal.fire({
                        text: "Apakah anda yakin untuk mengahpus cabang '" + dataName + "'?",
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Hapus!",
                        cancelButtonText: "Batalkan",
                        customClass: {
                            confirmButton: "btn fw-bold btn-danger",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function (result) {
                        if (result.value) {
                            Swal.fire({
                                text: "Mohon tunggu, Data sedang diproses!",
                                icon: "info",
                                buttonsStyling: false,
                                timer: 2000,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            }).then(function () {
                                const url = `${window.location.href}`
                                $.ajax({
                                    url: url,
                                    method: "POST",
                                    data: { _token: token, id: id, _method: "delete" },
                                    success: function () {
                                        toastr.success("Cabang '" + dataName + "' Berhasil dihapus");
                                        datatable.row($(parent)).remove().draw();
                                    },
                                    error: function () {
                                        toastr.error("Terjadi kesalahan");
                                    }
                                })
                                // Remove current row
                                // datatable.row($(parent)).remove().draw();
                            });
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: "Cabang " + dataName + " berhasil dibatalkan.",
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        // Public methods
        return {
            init: function () {
                table = document.querySelector('#kt_table');

                if (!table) {
                    return;
                }

                initJS();
                handleSearchDatatable();
                handleDeleteRows();
                createForm();
                editForm();
            }
        }
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KTCustomersList.init();
    });
    /******/
})()
    ;
//# sourceMappingURL=list.js.map