/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
    var __webpack_exports__ = {};
    /*!*******************************************************************************************!*\
      !*** ../../../themes/metronic/html/demo1/src/js/custom/authentication/sign-in/general.js ***!
      \*******************************************************************************************/


    // Class definition
    var KThajiCreate = function () {
        // Elements
        var form;
        var submitButton;
        var validator;

        // Handle form
        var handleForm = function (e) {
            if (form) {
                // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
                validator = FormValidation.formValidation(
                    form,
                    {
                        fields: {
                            'name': {
                                validators: {
                                    notEmpty: {
                                        message: 'Judul tidak boleh kosong'
                                    },
                                }
                            },
                            'text': {
                                validators: {
                                    callback: {
                                        message: 'Deskripsi tidak boleh kosong',
                                        callback: function () {
                                            // const input = tinymce.get('mytiny').getContent();
                                            // if(!input){
                                            //     return false
                                            // }
                                            return true
                                        }
                                    }
                                }
                            },
                            'image': {
                                validators: {
                                    file: {
                                        extension: 'png,jpg,jpeg',
                                        maxSize: '5242880',
                                        message: 'Gambar Banner hanya dapat berupa file PNG, JPG, JPEG & Maximum File 5Mb'
                                    }
                                }
                            },
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: '.fv-row'
                            })
                        }
                    }
                );

                // Handle form submit
                submitButton.addEventListener('click', function (e) {
                    // Prevent button default action
                    e.preventDefault();

                    // Validate form
                    validator.validate().then(function (status) {
                        if (status == 'Valid') {
                            // Show loading indication
                            submitButton.setAttribute('data-kt-indicator', 'on');

                            // Disable button to avoid multiple click 
                            submitButton.disabled = true;


                            // Simulate ajax request
                            setTimeout(function () {
                                // Hide loading indication
                                submitButton.removeAttribute('data-kt-indicator');

                                // Enable button
                                submitButton.disabled = false;

                                // Show message popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                                Swal.fire({
                                    text: "Mohon tunggu, Data sedang diproses!",
                                    icon: "info",
                                    buttonsStyling: false,
                                    timer: 2000,
                                    confirmButtonText: "Ok",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        // form.querySelector('[name="email"]').value = "";
                                        // form.querySelector('[name="password"]').value = "";
                                    }
                                    form.submit(); // submit form
                                });
                            }, 2000);
                        } else {
                            // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                            Swal.fire({
                                text: "Mohon perhatikan kembali isian form",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });
                        }
                    });
                });
            }
        }

        // Delete form
        var deleteForm = () => {
            // Select all delete buttons
            const parent = document.querySelector("#parent-card")
            // const deleteButton = parent.querySelectorAll("[data-destory='true']")
            const deleteButtons = parent.querySelector(".row").querySelectorAll('button[type="submit"]')

            deleteButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function (e) {
                    e.preventDefault();

                    const url = d.dataset.url
                    const id = d.dataset.id
                    const _token = d.dataset.token
                    const slug = d.dataset.slug
                    const route = d.dataset.route
                    const type = d.dataset.type
                    const no = d.dataset.no

                    const data = { id, _token, slug, route, type, no }

                    // SweetAlert2 pop up --- official docs reference: https://sweetalert2.github.io/
                    Swal.fire({
                        text: "Apakah anda yakin untuk mengahpus Data?",
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Hapus!",
                        cancelButtonText: "Batalkan",
                        customClass: {
                            confirmButton: "btn fw-bold btn-danger",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function (result) {
                        if (result.value) {
                            Swal.fire({
                                text: "Mohon tunggu, Data sedang diproses!",
                                icon: "info",
                                buttonsStyling: false,
                                timer: 2000,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            }).then(function () {
                                $.ajax({
                                    url: url,
                                    method: "DELETE",
                                    data: data,
                                    success: function () {
                                        // window.location.reload()
                                    },
                                    error: function () {
                                        toastr.error("Terjadi kesalahan");
                                    }
                                })
                                // Remove current row
                                // datatable.row($(parent)).remove().draw();
                            });
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: "Hapus Data dibatalkan.",
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        // Public functions
        return {
            // Initialization
            init: function () {
                form = document.querySelector('#kt_haji_section_list_form');
                submitButton = document.querySelector('#kt_haji_section_list_submit');

                handleForm();
                deleteForm();
            }
        };
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KThajiCreate.init();
    });

    /******/
})()
    ;
//# sourceMappingURL=general.js.map