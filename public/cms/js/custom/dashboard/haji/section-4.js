/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
    var __webpack_exports__ = {};
    /*!*******************************************************************************************!*\
      !*** ../../../themes/metronic/html/demo1/src/js/custom/authentication/sign-in/general.js ***!
      \*******************************************************************************************/


    // Class definition
    var KTUmrohCreate = function () {
        // Elements
        var form;
        var submitButton;
        var validator;

        // Handle form
        var handleForm = function (e) {
            // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
            validator = FormValidation.formValidation(
                form,
                {
                    fields: {
                        'name': {
                            validators: {
                                notEmpty: {
                                    message: 'Judul tidak boleh kosong'
                                }
                            }
                        },
                        'text': {
                            validators: {
                                callback: {
                                    message: 'Deskripsi tidak boleh kosong',
                                    callback: function () {
                                        // const input = tinymce.get('mytiny').getContent();
                                        // if(!input){
                                        //     return false
                                        // }
                                        return true
                                    }
                                }
                            }
                        },
                        'phone': {
                            validators: {
                                notEmpty: {
                                    message: 'Nomor WA tidak boleh kosong'
                                },
                                stringLength: {
                                    min: 10,
                                    max: 13,
                                    message: 'Nomor WA minimal 10 digit dan maksimal 13 digit'
                                }
                            }
                        },
                        'label_wa': {
                            validators: {
                                notEmpty: {
                                    message: 'Label Tombol WA tidak boleh kosong'
                                }
                            }
                        },
                        'text_wa': {
                            validators: {
                                notEmpty: {
                                    message: 'Deskripsi WA tidak boleh kosong'
                                },
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row'
                        })
                    }
                }
            );

            // Handle form submit
            submitButton.addEventListener('click', function (e) {
                // Prevent button default action
                e.preventDefault();

                // Validate form
                validator.validate().then(function (status) {
                    if (status == 'Valid') {
                        // Show loading indication
                        submitButton.setAttribute('data-kt-indicator', 'on');

                        // Disable button to avoid multiple click 
                        submitButton.disabled = true;


                        // Hide loading indication
                        submitButton.removeAttribute('data-kt-indicator');

                        // Enable button
                        submitButton.disabled = false;

                        // Show message popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                        Swal.fire({
                            text: "Apa Anda yakin untuk melanjutkan?",
                            icon: "question",
                            showCancelButton: true,
                            buttonsStyling: false,
                            confirmButtonText: "Simpan",
                            cancelButtonText: "Batalkan",
                            customClass: {
                                confirmButton: "btn fw-bold btn-success",
                                cancelButton: "btn fw-bold btn-secondary"
                            }
                        }).then(function (result) {
                            if (result.value) {
                                Swal.fire({
                                    text: "Mohon tunggu, Data sedang diproses!",
                                    icon: "info",
                                    buttonsStyling: false,
                                    timer: 2000,
                                    confirmButtonText: "Ok",
                                    customClass: {
                                        confirmButton: "btn fw-bold btn-primary",
                                    }
                                }).then(function () {
                                    form.submit(); // submit form
                                })
                            }
                            else {
                                Swal.fire({
                                    text: "Proses dibatalkan",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok",
                                    customClass: {
                                        confirmButton: "btn fw-bold btn-primary",
                                    }
                                });
                            }
                        });
                    } else {
                        // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                        Swal.fire({
                            text: "Mohon perhatikan kembali isian form",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                    }
                });
            });
        }

        // Public functions
        return {
            // Initialization
            init: function () {
                form = document.querySelector('#kt_haji_section_4_form');
                submitButton = document.querySelector('#kt_haji_section_4_submit');

                handleForm();
            }
        };
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KTUmrohCreate.init();
    });

    /******/
})()
    ;
//# sourceMappingURL=general.js.map