<?php

use App\Http\Controllers\Dashboard\AboutUsContoller as DashboardAboutUsContoller;
use App\Http\Controllers\Dashboard\ArticleController as DashboardArticleController;
use App\Http\Controllers\Dashboard\ContactUsController as DashboardContactUsController;
use App\Http\Controllers\Dashboard\FaqController as DashboardFaqController;
use App\Http\Controllers\Dashboard\HajiController as DashboardHajiController;
use App\Http\Controllers\Dashboard\HajiFurodaController as DashboardHajiFurodaController;
use App\Http\Controllers\Dashboard\HajiKhususController as DashboardHajiKhususController;
use App\Http\Controllers\Dashboard\HajiParentController as DashboardHajiParentController;
use App\Http\Controllers\Dashboard\JejakIslami\NoIncludeController as DashboardJINoIncludeController;
use App\Http\Controllers\Dashboard\JejakIslami\OptionalController as DashboardOptionalController;
use App\Http\Controllers\Dashboard\JejakIslami\ProvitController as DashboardJIProvitController;
use App\Http\Controllers\Dashboard\JejakIslami\SorterJejakIslamiController as DashboardSorterJejakIslamiController;
use App\Http\Controllers\Dashboard\JejakIslami\TripFacilityController as DashboardJITripFacilityController;
use App\Http\Controllers\Dashboard\JejakIslami\TripPlanController as DashboardJITripPlanController;
use App\Http\Controllers\Dashboard\JejakIslamiController as DashboardJejakIslamiController;
use App\Http\Controllers\Dashboard\PacketController as DashboardPacketController;
use App\Http\Controllers\Dashboard\SosialMediaController as DashboardSosialMedia;
use App\Http\Controllers\Dashboard\UmrohCategoryController;
use App\Http\Controllers\Dashboard\UmrohController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Homepage\AboutController;
use App\Http\Controllers\Homepage\ArticleController;
use App\Http\Controllers\Homepage\ContactController;
use App\Http\Controllers\Homepage\FaqController;
use App\Http\Controllers\Homepage\HajiController;
use App\Http\Controllers\Homepage\HajiFurodaController;
use App\Http\Controllers\Homepage\HajiKhususController;
use App\Http\Controllers\Homepage\HomepageController;
use App\Http\Controllers\Homepage\JejakislamiController;
use App\Http\Controllers\Homepage\KeunggulanController;
use App\Http\Controllers\Homepage\MitraController;
use App\Http\Controllers\Homepage\TabunganUmrahController;
use App\Http\Controllers\Homepage\UmrahController;
use App\Http\Controllers\Homepage\UmrahDiamondController;
use App\Http\Controllers\Homepage\UstadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::prefix('/')->group(function () {
    Route::resource('/', HomepageController::class);
    Route::resource('about', AboutController::class);
    Route::resource('contact', ContactController::class);
    Route::resource('article', ArticleController::class);
    Route::resource('haji-jejak-imani', HajiController::class);
    Route::resource('haji-khusus', HajiKhususController::class);
    Route::resource('haji-furoda', HajiFurodaController::class);
    Route::resource('paket-umrah', UmrahController::class);
    Route::resource('tabungan-umrah', TabunganUmrahController::class);
    Route::resource('jejakislami', JejakislamiController::class);
    Route::resource('keunggulan-jejak-imani', KeunggulanController::class);
    Route::resource('profil-ustadz', UstadController::class);
    Route::resource('umroh-diamond', UmrahDiamondController::class);
    Route::resource('cabang-mitra', MitraController::class);
    Route::get('articel/{slug}', [ArticleController::class, 'view'])->name('article.view');
    Route::get('faq', [FaqController::class, 'index'])->name('faq');
    Route::get('badal-umrah', [UmrahController::class, 'badalUmrah'])->name('badal-umrah');
});

Route::get('cms-admin', function () {
    return redirect('/cms-admin/home');
})->name('auto');
Route::prefix('/cms-admin')->group(function () {
    Auth::routes();

    Route::name("cms.")->group(function () {

        Route::get('/home', [HomeController::class, 'index'])->name('home');
        Route::post('/tinymce-upload', [HomeController::class, 'tinymce_upload'])->name('tinymce.upload');

        Route::resource("umroh", UmrohController::class);
        Route::post('umroh/tracker', [UmrohController::class, 'store_tracker'])->name('umroh.tracker');
        Route::resource("umroh-category", UmrohCategoryController::class);

        // Haji
        Route::prefix('/haji')->name("haji.")->group(function () {
            // Banner
            Route::post("banner", [DashboardHajiController::class, "store_banner"])->name("banner.store");
            Route::delete("banner/{id}", [DashboardHajiController::class, "destroy_banner"])->name("banner.destroy");
            Route::post("label-banner", [DashboardHajiController::class, "store_label_banner"])->name("label.banner.store");
            Route::post("section-1", [DashboardHajiController::class, "store_section_1"])->name("section.1.store");
            Route::post("section-2", [DashboardHajiController::class, "store_section_2"])->name("section.2.store");
            Route::post("section-3", [DashboardHajiController::class, "store_section_3"])->name("section.3.store");
            Route::post("section-4", [DashboardHajiController::class, "store_section_4"])->name("section.4.store");
            Route::post("section-list", [DashboardHajiController::class, "store_section_list"])->name("section.list.store");
            Route::post("section-list-order", [DashboardHajiController::class, "order_section_list"])->name("section.list.order");
            Route::post("haji-content/{id}", [DashboardHajiController::class, "haji_content_store"])->name("content.store");
            Route::delete("section-list/{id}", [DashboardHajiController::class, "destroy_section_list"])->name("section.list.destroy");
            Route::post("testimoni", [DashboardHajiController::class, "store_testimoni"])->name("testimoni.store");
            Route::delete("testimoni/{id}", [DashboardHajiController::class, "destroy_testimoni"])->name("testimoni.destroy");
        });

        // Parent
        Route::prefix('/haji-parent')->name("haji.parent.")->group(function () {
            // Banner
            Route::get("banner", [DashboardHajiParentController::class, "banner"])->name("banner.index");
            Route::get("banner/create", [DashboardHajiParentController::class, "create_banner"])->name("banner.create");
            Route::get("banner/{id}", [DashboardHajiParentController::class, "edit_banner"])->name("banner.edit");

            // Label Banner
            Route::get("label-banner", [DashboardHajiParentController::class, "label_banner"])->name("label.banner.index");

            Route::get("haji-khusus", [DashboardHajiParentController::class, "haji_khusus"])->name("haji_khusus.index");
            Route::get("haji-furoda", [DashboardHajiParentController::class, "haji_furoda"])->name("haji_furoda.index");
            Route::get("testiomni", [DashboardHajiParentController::class, "testimoni"])->name("testimoni.index");
            Route::get("testiomni/create", [DashboardHajiParentController::class, "testimoni_create"])->name("testimoni.create");
            Route::get("testimoni/{id}", [DashboardHajiParentController::class, "testimoni_edit"])->name("testimoni.edit");
        });

        // Khusus
        Route::prefix('/haji-khusus')->name("haji.khusus.")->group(function () {
            // Banner
            Route::get("banner", [DashboardHajiKhususController::class, "banner"])->name("banner.index");
            Route::get("banner/create", [DashboardHajiKhususController::class, "create_banner"])->name("banner.create");
            Route::get("banner/{id}", [DashboardHajiKhususController::class, "edit_banner"])->name("banner.edit");

            // Label Banner
            Route::get("label-banner", [DashboardHajiKhususController::class, "label_banner"])->name("label.banner.index");

            // section 1-4
            Route::get("section-1", [DashboardHajiKhususController::class, "section_1"])->name("section.1.index");
            Route::get("section-2", [DashboardHajiKhususController::class, "section_2"])->name("section.2.index");
            Route::get("section-3", [DashboardHajiKhususController::class, "section_3"])->name("section.3.index");
            Route::get("section-4", [DashboardHajiKhususController::class, "section_4"])->name("section.4.index");

            // section list 1-4 index
            Route::get("section-list-1", [DashboardHajiKhususController::class, "section_list_1"])->name("section.list.1.index");
            Route::get("section-list-2", [DashboardHajiKhususController::class, "section_list_2"])->name("section.list.2.index");
            Route::get("section-list-3", [DashboardHajiKhususController::class, "section_list_3"])->name("section.list.3.index");
            Route::get("section-list-4", [DashboardHajiKhususController::class, "section_list_4"])->name("section.list.4.index");

            // section list 1-4 create
            Route::get("section-list-1/create", [DashboardHajiKhususController::class, "create_section_list_1"])->name("section.list.1.create");
            Route::get("section-list-2/create", [DashboardHajiKhususController::class, "create_section_list_2"])->name("section.list.2.create");
            Route::get("section-list-3/create", [DashboardHajiKhususController::class, "create_section_list_3"])->name("section.list.3.create");
            Route::get("section-list-4/create", [DashboardHajiKhususController::class, "create_section_list_4"])->name("section.list.4.create");

            // section list 1-4 edit
            Route::get("{id}/section-list-1", [DashboardHajiKhususController::class, "edit_section_list_1"])->name("section.list.1.edit");
            Route::get("{id}/section-list-2", [DashboardHajiKhususController::class, "edit_section_list_2"])->name("section.list.2.edit");
            Route::get("{id}/section-list-3", [DashboardHajiKhususController::class, "edit_section_list_3"])->name("section.list.3.edit");
            Route::get("{id}/section-list-4", [DashboardHajiKhususController::class, "edit_section_list_4"])->name("section.list.4.edit");
        });

        // Furoda
        Route::prefix('/haji-furoda')->name("haji.furoda.")->group(function () {
            // Banner
            Route::get("banner", [DashboardHajiFurodaController::class, "banner"])->name("banner.index");
            Route::get("banner/create", [DashboardHajiFurodaController::class, "create_banner"])->name("banner.create");
            Route::get("banner/{id}", [DashboardHajiFurodaController::class, "edit_banner"])->name("banner.edit");

            // Label Banner
            Route::get("label-banner", [DashboardHajiFurodaController::class, "label_banner"])->name("label.banner.index");

            // section 1-4
            Route::get("section-1", [DashboardHajiFurodaController::class, "section_1"])->name("section.1.index");
            Route::get("section-2", [DashboardHajiFurodaController::class, "section_2"])->name("section.2.index");
            Route::get("section-3", [DashboardHajiFurodaController::class, "section_3"])->name("section.3.index");
            Route::get("section-4", [DashboardHajiFurodaController::class, "section_4"])->name("section.4.index");

            // section list 1-4 index
            Route::get("section-list-1", [DashboardHajiFurodaController::class, "section_list_1"])->name("section.list.1.index");
            Route::get("section-list-2", [DashboardHajiFurodaController::class, "section_list_2"])->name("section.list.2.index");
            Route::get("section-list-3", [DashboardHajiFurodaController::class, "section_list_3"])->name("section.list.3.index");
            Route::get("section-list-4", [DashboardHajiFurodaController::class, "section_list_4"])->name("section.list.4.index");

            // section list 1-4 create
            Route::get("section-list-1/create", [DashboardHajiFurodaController::class, "create_section_list_1"])->name("section.list.1.create");
            Route::get("section-list-2/create", [DashboardHajiFurodaController::class, "create_section_list_2"])->name("section.list.2.create");
            Route::get("section-list-3/create", [DashboardHajiFurodaController::class, "create_section_list_3"])->name("section.list.3.create");
            Route::get("section-list-4/create", [DashboardHajiFurodaController::class, "create_section_list_4"])->name("section.list.4.create");

            // section list 1-4 edit
            Route::get("{id}/section-list-1", [DashboardHajiFurodaController::class, "edit_section_list_1"])->name("section.list.1.edit");
            Route::get("{id}/section-list-2", [DashboardHajiFurodaController::class, "edit_section_list_2"])->name("section.list.2.edit");
            Route::get("{id}/section-list-3", [DashboardHajiFurodaController::class, "edit_section_list_3"])->name("section.list.3.edit");
            Route::get("{id}/section-list-4", [DashboardHajiFurodaController::class, "edit_section_list_4"])->name("section.list.4.edit");
        });

        // Content
        Route::prefix('/content')->name("content.")->group(function () {
            Route::get("sosial-media", [DashboardSosialMedia::class, 'index'])->name('sosial.media.index');
            Route::post("sosial-media/store", [DashboardSosialMedia::class, 'store'])->name('sosial.media.store');

            // Contact US
            Route::prefix('/contact-us')->name("contact.us.")->group(function () {
                Route::get('description', [DashboardContactUsController::class, 'description'])->name('description.index');
                Route::get('head-office', [DashboardContactUsController::class, 'head_office'])->name('head.office.index');
                Route::post('store', [DashboardContactUsController::class, 'store'])->name('store');
                Route::get('cabang', [DashboardContactUsController::class, 'cabang'])->name('cabang.index');
                Route::post('cabang', [DashboardContactUsController::class, 'store_cabang'])->name('cabang.store');
                Route::put('cabang', [DashboardContactUsController::class, 'update_cabang'])->name('cabang.update');
                Route::delete('cabang', [DashboardContactUsController::class, 'destroy_cabang'])->name('cabang.destroy');
                Route::get('mitra', [DashboardContactUsController::class, 'mitra'])->name('mitra.index');
                Route::post('mitra', [DashboardContactUsController::class, 'store_mitra'])->name('mitra.store');
                Route::put('mitra', [DashboardContactUsController::class, 'update_mitra'])->name('mitra.update');
                Route::delete('mitra', [DashboardContactUsController::class, 'destroy_mitra'])->name('mitra.destroy');
            });

            // About US
            Route::prefix('/about-us')->name("about.us.")->group(function () {
                Route::get('sejarah', [DashboardAboutUsContoller::class, 'sejarah'])->name('sejarah.index');
                Route::put('sejarah/{id}', [DashboardAboutUsContoller::class, 'update_sejarah'])->name('sejarah.update');

                Route::get('visi', [DashboardAboutUsContoller::class, 'visi'])->name('visi.index');
                Route::put('visi/{id}', [DashboardAboutUsContoller::class, 'update_visi'])->name('visi.update');

                Route::get('misi', [DashboardAboutUsContoller::class, 'misi'])->name('misi.index');
                Route::put('misi/{id}', [DashboardAboutUsContoller::class, 'update_misi'])->name('misi.update');

                // Eksekutif
                Route::get('eksekutif', [DashboardAboutUsContoller::class, 'eksekutif'])->name('eksekutif.index');
                Route::post('eksekutif', [DashboardAboutUsContoller::class, 'store_eksekutif'])->name('eksekutif.store');
                Route::put('eksekutif', [DashboardAboutUsContoller::class, 'update_eksekutif'])->name('eksekutif.update');
                Route::delete('eksekutif', [DashboardAboutUsContoller::class, 'destroy_eksekutif'])->name('eksekutif.destroy');

                Route::get('sambutan', [DashboardAboutUsContoller::class, 'sambutan'])->name('sambutan.index');
                Route::put('sambutan/{id}', [DashboardAboutUsContoller::class, 'update_sambutan'])->name('sambutan.update');
            });

            // Jejak Islami
            Route::prefix('/jejak-islami')->name("jejak.islami.")->group(function () {
                Route::get('section-1', [DashboardJejakIslamiController::class, 'section1'])->name('section-1.index');
                Route::put('section-1/{id}', [DashboardJejakIslamiController::class, 'update_section1'])->name('section-1.update');

                Route::resource('provit', DashboardJIProvitController::class);
                Route::resource('trip-plan', DashboardJITripPlanController::class);
                Route::resource('trip-facility', DashboardJITripFacilityController::class);
                Route::resource('no-include', DashboardJINoIncludeController::class);

                Route::get('optional', [DashboardOptionalController::class, 'index'])->name('optional.index');
                Route::put('optional', [DashboardOptionalController::class, 'update'])->name('optional.update');

                Route::put('sorter', [DashboardSorterJejakIslamiController::class, 'update'])->name('sorter.update');
            });

            // FAQ
            Route::prefix('/faq')->name("faq.")->group(function () {
                Route::resource('list', DashboardFaqController::class);

                Route::get('phone', [DashboardFaqController::class, 'phone'])->name('phone.index');
                Route::put('phone/{id}', [DashboardFaqController::class, 'update_phone'])->name('phone.update');
            });
        });

        // Article
        Route::resource('article', DashboardArticleController::class);

        // Other
        Route::prefix('/other')->name("other.")->group(function () {
            Route::resource('packet', DashboardPacketController::class);
        });
    });
});
