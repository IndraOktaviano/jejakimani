<!DOCTYPE html>
<!--
Author: Sian
Product Name: Jejak Imani Web

Template: Metronic v2.0.87
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <title>@yield('title')</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Jejak Imani" />
    <meta name="keywords" content="Jejak Imani Umroh Haji" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    {{--
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title"
        content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
    <meta property="og:url" content="https://keenthemes.com/metronic" />
    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" /> --}}

    <link rel="shortcut icon" href="{{ asset('cms/media/logos/favicon.ico') }}" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{ asset('cms/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('cms/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->

    <style>
        /* Chrome, Safari, Edge, Opera */
        .hide-arrow,
        .hide-arrow:hover {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        .hide-arrow,
        .hide-arrow:hover {
            -moz-appearance: textfield;
        }
    </style>

    {{-- begin:Custom CSS --}}
    @yield('customCSS')
    {{-- end:Custom CSS --}}
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body"
    class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <div id="app">
        <!--begin::Main-->
        <!--begin::Root-->
        <div class="d-flex flex-column flex-root">
            <!--begin::Page-->
            <div class="page d-flex flex-row flex-column-fluid">
                <!--begin::Aside-->
                @include('components.layouts.aside.aside')
                <!--end::Aside-->
                <!--begin::Wrapper-->
                <div id="kt_wrapper" class="d-flex flex-column flex-row-fluid wrapper">
                    <!--begin::Header-->
                    @include('components.layouts.header.header')
                    <!--end::Header-->
                    @include('components.layouts.header.toolbar')
                    <!--begin::Content-->
                    <div id="kt_content" class="content d-flex flex-column flex-column-fluid">

                        {{-- <div class="post d-flex flex-column-fluid"> --}}
                            <div id="kt_content_container" class="container-xxl">
                                @yield('content')
                            </div>
                            {{--
                        </div> --}}
                    </div>
                    <!--end::Content-->
                    <!--begin::Footer-->
                    <div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
                        @include('components.layouts.footer.footer')
                    </div>
                    <!--end::Footer-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Page-->
        </div>
        <!--end::Root-->

        <!--begin::Scrolltop-->
        <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
            <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
            <span class="svg-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                        fill="black" />
                    <path
                        d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                        fill="black" />
                </svg>
            </span>
            <!--end::Svg Icon-->
        </div>
        <!--end::Scrolltop-->
    </div>
    <!--end::Main-->
    <!--begin::Javascript-->
    <script src="{{ asset('js/jquery-3.6.0.js') }}"></script>
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="{{ asset('cms/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('cms/js/scripts.bundle.js') }}"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="{{ asset('cms/js/custom/widgets.js') }}"></script>
    <script src="{{ asset('cms/js/custom/apps/chat/chat.js') }}"></script>
    <script src="{{ asset('cms/js/custom/modals/create-app.js') }}"></script>
    <script src="{{ asset('cms/js/custom/modals/upgrade-plan.js') }}"></script>
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->

    @include('components.layouts.footer.alert')

    {{-- begin:Custom JS --}}
    @yield('customJS')
    {{-- end:Custom JS --}}

    <script>
        function close_toast() {
            let toast = document.querySelectorAll(".tox-notification__dismiss")
            toast.forEach(function(e) {
                e.click();
                e.click();
            })
        }        
        setTimeout(() => {        
            close_toast();
        }, 2000);
    </script>
</body>
<!--end::Body-->
</div>

</html>