<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="shortcut icon" href="{{ asset('images/logos/Logo-Master-Vertikal-Haji-Kuning.png') }}">

    {{-- css --}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('css')

    {{-- icon --}}
    <script src="https://kit.fontawesome.com/4c648c1491.js" crossorigin="anonymous"></script>
</head>

<body>
    <div id="app">
        <div class="splash">
            <img class="fade-in" src="{{ asset('images/logos/Logo-Master-Vertikal-Haji-Kuning.png') }}" alt=""
                srcset="">
        </div>
        <x-homepage.header></x-homepage.header>
        <div id="content"></div>
        @yield('content')

        @include('components.homepage.footer')
    </div>

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery-3.6.0.js') }}"></script>
    <script>
        const all = document.querySelectorAll('.menu-hover')
        // console.log(all);
        document.querySelectorAll('.menu-hover').forEach((el) => {
            el.addEventListener('mouseover', function() {
                this.click()
            })
        })
    </script>
    <script>
        const splash = document.querySelector('.splash');

        document.addEventListener('DOMContentLoaded', (e) => {
            setTimeout(() => {
                splash.classList.add('display-none');
            }, 1000);
        })
    </script>
    @yield('js')
</body>

</html>
