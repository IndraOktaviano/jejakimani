<section id="other-package">
    <div class="container">
        <div class="row mx-auto my-auto justify-content-center">
            <div id="recipeCarousel" class="carousel carousel-other-package slide" data-bs-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    @php
                        $i = 0;
                    @endphp
                    @foreach ($other_packet as $item)
                        <div class="carousel-item item-carousel-other-package {{ !$i++ ? 'active' : '' }}">
                            <div class="card p-1 text-center w-100">
                                @php
                                    $json = json_decode($item['json']);
                                @endphp
                                <div class="card-img">
                                    <img src="{{ asset($json->image) }}" class="img-fluid"
                                        alt="{{ asset($json->image) }}">
                                    <a href="{{ $json->url }}"
                                        class="btn btn-warning text-decoration-none btn-shake tombol-detail">LIHAT
                                        DETAIL <i class="fas fa-arrow-right">
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev bg-transparent w-aut" href="#recipeCarousel" role="button"
                    data-bs-slide="prev">
                    <i class="fas fa-chevron-circle-left fa-2x orange"></i>
                </a>
                <a class="carousel-control-next bg-transparent w-aut" href="#recipeCarousel" role="button"
                    data-bs-slide="next">
                    <i class="fas fa-chevron-circle-right fa-2x orange"></i>
                </a>
            </div>
        </div>
    </div>
    
    <script>
        let items = document.querySelectorAll('.carousel-other-package .item-carousel-other-package')

        items.forEach((el) => {
            const minPerSlide = 4
            let next = el.nextElementSibling
            for (var i = 1; i < minPerSlide; i++) {
                if (!next) {
                    // wrap carousel by using first child
                    next = items[0]
                }
                let cloneChild = next.cloneNode(true)
                el.appendChild(cloneChild.children[0])
                next = next.nextElementSibling
            }
        })
    </script>
</section>
