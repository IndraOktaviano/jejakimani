<header>
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="{{ route('index') }}">
                <img src="{{ asset('images/logos/Logo-Master-Vertikal-Haji-Kuning.png') }}" alt="" height="90">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="fas fa-bars text-light"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item dropdown mx-1 ms-md-3 text-start text-md-center">
                        <a class="nav-link pb-0" href="{{ route('haji-jejak-imani.index') }}">
                            Haji
                        </a>
                        <i class="fas fa-chevron-down menu-hover" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false"></i>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="{{ route('haji-khusus.index') }}">Haji Plus Haji
                                    Khusus
                                    Jejak Imani</a></li>
                            <li><a class="dropdown-item" href="{{ route('haji-furoda.index') }}">Haji Furoda Jejak
                                    Imani</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown mx-1 ms-md-3 text-start text-md-center">
                        <a class="nav-link pb-0" href="{{ route('paket-umrah.index') }}">
                            Umrah
                        </a>
                        <i class="fas fa-chevron-down menu-hover" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false"></i>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="{{ route('tabungan-umrah.index') }}">Tabungan
                                    Umrah</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown mx-1 ms-md-3 text-start text-md-center">
                        <a class="nav-link pb-0" href="#">
                            Jejak Islami
                        </a>
                        <i class="fas fa-chevron-down menu-hover" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false"></i>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li class="dropdown-submenu">
                                <a tabindex="-1" class="dropdown-item" href="#">Turkey</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" class="dropdown-item" href="{{ route('jejakislami.index') }}">Turki Ottoman(barat)</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item mx-1 ms-md-3">
                        <a class="nav-link" href="{{ route('article.index') }}">
                            Artikel Islami
                        </a>
                    </li>
                    <li class="nav-item mx-1 ms-md-3">
                        <a class="nav-link" href="{{ route('about.index') }}">
                            Tentang Kami
                        </a>
                    </li>
                    <li class="nav-item mx-1 ms-md-3">
                        <a class="nav-link" href="{{ route('contact.index') }}">
                            Hubungi Kami
                        </a>
                    </li>
                    <li class="nav-item mx-1 ms-md-3">
                        <a class="nav-link" href="https://kelasonline.jejakimani.com/">
                            Kelas Online
                        </a>
                    </li>
                    <li class="nav-item mx-1 ms-md-3">
                        <a class="nav-link" href="{{ url('faq') }}">
                            FAQ
                        </a>
                    </li>
                    <li class="nav-item mx-1 ms-md-3">
                        <a class="nav-link btn btn-warning btn-register" target="_blank"
                            href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20Bustomi%20Saya%20lihat%20dari%20web%20jejakimani%20,%20mau%20tanya">
                            Daftar
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
