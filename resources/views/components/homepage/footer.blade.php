<footer class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="row justify-content-center">
                    <div class="col-auto">
                        <a href="{{route('keunggulan-jejak-imani.index')}}" class="mx-2 text-decoration-none">Keunggulan</a>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('profil-ustadz.index')}}" class="mx-2 text-decoration-none">Profil Ustadz</a>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('cabang-mitra.index')}}" class="mx-2 text-decoration-none">Cabang/Mitra</a>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('about.index')}}" class="mx-2 text-decoration-none">Tentang Kami</a>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('contact.index')}}" class="mx-2 text-decoration-none">Hubungi Kami</a>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3">
                <hr>
            </div>
            <div class="col-12 mt-3 d-flex justify-content-center">
                @if (!$sosial_media['twitter']->json || $sosial_media['twitter']->json != "0")
                <a href="https://twitter.com/{{$sosial_media['twitter']->json}}/">
                    <i class="fab fa-twitter mx-3"></i>
                </a>
                @endif
                @if (!$sosial_media['facebook']->json || $sosial_media['facebook']->json != "0")
                <a href="https://www.facebook.com/{{$sosial_media['facebook']->json}}/">
                    <i class="fab fa-facebook-f mx-3"></i>
                </a>
                @endif
                @if (!$sosial_media['youtube']->json || $sosial_media['youtube']->json != "0")
                <a href="https://www.youtube.com/channel/{{$sosial_media['youtube']->json}}">
                    <i class="fab fa-youtube mx-3"></i>
                </a>
                @endif
                @if (!$sosial_media['instagram']->json || $sosial_media['instagram']->json != "0")
                <a href="https://www.instagram.com/{{$sosial_media['instagram']->json}}/">
                    <i class="fab fa-instagram mx-3"></i>
                </a>
                @endif
                @if (!$sosial_media['whatsapp']->phone || $sosial_media['whatsapp']->phone != "0")
                <a
                    href="https://api.whatsapp.com/send?phone={{$sosial_media['whatsapp']->phone}}&text={{$sosial_media['whatsapp']->text}}">
                    <i class="fab fa-whatsapp mx-3"></i>
                </a>
                @endif
            </div>
            <div class="row mt-5 pt-5">
                <div class="col-12 col-md-6">
                    <p>
                        Kantor Pusat:
                        <br> Jl Siliwangi No.4, Pondok Benda, Kec. Pamulang, Kota Tangerang Selatan, Banten 15417
                        <br> Telepon Pusat: 021-2274-1322 Hotline Pusat: 0857-2002-8100
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    Kantor Yogyakarta :
                    <br> Jl Salakan II No. 222, Saman, Bangunharjo, Kec. Sewon, Bantul, Daerah Istimewa Yogyakarta
                    <br> Telepon Yogyakarta : 0274-379-700 Hotline Yogyakarta : 0811-299-5755
                </div>
            </div>
        </div>
    </div>
</footer>