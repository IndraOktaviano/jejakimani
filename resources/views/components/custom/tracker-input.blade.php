<div class="col-md-12 fv-row mb-10">
    <label class="form-label fs-6 fw-bolder text-dark">Event Tracker</label>
    <input type="text" name="event" value="{{ old('event', $tracker?->event ?? '') }}"
        placeholder="Harus di isi jika Event Catgeory / Event Label di isi Agar Event Tracker bekerja "
        class="form-control @error('event') is-invalid @enderror">
    @error('event')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="col-md-12 fv-row mb-10">
    <label class="form-label fs-6 fw-bolder text-dark">Event Category</label>
    <input type="text" name="event_category" value="{{ old('event_category', $tracker?->event_category ?? '') }}"
        class="form-control @error('event_category') is-invalid @enderror">
    @error('event_category')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>


<div class="col-md-12 fv-row mb-10">
    <label class="form-label fs-6 fw-bolder text-dark">Event Label</label>
    <input type="text" name="event_label" value="{{ old('event_label', $tracker?->event_label ?? '') }}"
        class="form-control @error('event_label') is-invalid @enderror">
    @error('event_label')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
