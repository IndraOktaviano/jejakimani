<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
    id="#kt_aside_menu" data-kt-menu="true">
    <div class="menu-item">
        <a class="menu-link {{ str_contains(request()->url(), 'home') ? 'active' : '' }}" href="{{route('cms.home')}}">
            <span class="menu-icon">
                <i class="bi bi-speedometer fs-3"></i>
            </span>
            <span class="menu-title">Dashboard</span>
        </a>
    </div>
    {{-- Separator................... --}}
    <div class="menu-item">
        <div class="menu-content pt-8 pb-2">
            <span class="menu-section text-muted text-uppercase fs-8 ls-1">Master Data</span>
        </div>
    </div>
    <div data-kt-menu-trigger="click"
        class="menu-item menu-accordion {{ str_contains(request()->url(), 'umroh')  ? 'show' : '' }}">
        <span class="menu-link">
            <span class="menu-icon">
                <i class="bi bi-bag-check fs-3"></i>
            </span>
            <span class="menu-title">Umroh</span>
            <span class="menu-arrow"></span>
        </span>
        <div class="menu-sub menu-sub-accordion menu-active-bg">
            <div class="menu-item">
                <a class="menu-link {{ str_contains(request()->url(), 'umroh') && !str_contains(request()->url(), 'umroh-') ? 'active' : '' }}"
                    href=" {{route('cms.umroh.index')}} ">
                    <span class="menu-bullet">
                        <span class="bullet bullet-dot"></span>
                    </span>
                    <span class="menu-title">Paket</span>
                </a>
            </div>
            <div class="menu-item">
                <a class="menu-link {{ str_contains(request()->url(), 'umroh-category') ? 'active' : '' }}"
                    href="{{route('cms.umroh-category.index')}}">
                    <span class="menu-bullet">
                        <span class="bullet bullet-dot"></span>
                    </span>
                    <span class="menu-title">Kategori</span>
                </a>
            </div>
        </div>
    </div>
    {{-- Separator................... --}}
    <div data-kt-menu-trigger="click"
        class="menu-item menu-accordion {{ str_contains(request()->url(), '/haji-')  ? 'show' : '' }}">
        <span class="menu-link">
            <span class="menu-icon">
                <i class="bi bi-stack fs-3"></i>
            </span>
            <span class="menu-title">Konten Haji</span>
            <span class="menu-arrow"></span>
        </span>
         {{-- PARENT ---------------- --}}
         <div class="menu-sub menu-sub-accordion menu-active-bg">
            <div data-kt-menu-trigger="click"
                class="menu-item menu-accordion {{ str_contains(request()->url(), 'haji-parent')? 'show' : '' }}">
                <span class="menu-link">
                    <span class="menu-icon">
                        <i class="bi bi-house-fill fs-3"></i>
                    </span>
                    <span class="menu-title">Parent</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg">
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-parent/banner')? 'active' : '' }}"
                            href=" {{route('cms.haji.parent.banner.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Banner</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-parent/label') ? 'active' : '' }}"
                            href="{{route('cms.haji.parent.label.banner.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Label Banner</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-parent/haji-khusus') ? 'active' : '' }}"
                            href="{{route('cms.haji.parent.haji_khusus.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Haji Khusus</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-parent/haji-furoda') ? 'active' : '' }}"
                            href="{{route('cms.haji.parent.haji_furoda.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Haji Furoda</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-parent/testimoni') ? 'active' : '' }}"
                            href="{{route('cms.haji.parent.testimoni.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Testimoni</span>
                        </a>
                    </div>                    
                </div>
            </div>
        </div>
        {{-- KHUSUS ---------------- --}}
        <div class="menu-sub menu-sub-accordion menu-active-bg">
            <div data-kt-menu-trigger="click"
                class="menu-item menu-accordion {{ str_contains(request()->url(), 'haji-khusus')? 'show' : '' }}">
                <span class="menu-link">
                    <span class="menu-icon">
                        <i class="bi bi-moon-stars-fill fs-3"></i>
                    </span>
                    <span class="menu-title">Haji Khusus</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg">
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/banner')? 'active' : '' }}"
                            href=" {{route('cms.haji.khusus.banner.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Banner</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/label') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.label.banner.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Label Banner</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/section-1') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.section.1.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 1</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/section-2') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.section.2.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 2</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/section-3') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.section.3.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 3</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/section-4') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.section.4.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 4</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/section-list-1') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.section.list.1.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section List 1</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/section-list-2') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.section.list.2.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section List 2</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/section-list-3') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.section.list.3.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section List 3</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-khusus/section-list-4') ? 'active' : '' }}"
                            href="{{route('cms.haji.khusus.section.list.4.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section List 4</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {{-- FURODA ---------------- --}}
        <div class="menu-sub menu-sub-accordion menu-active-bg">
            <div data-kt-menu-trigger="click"
                class="menu-item menu-accordion {{ str_contains(request()->url(), 'haji-furoda')? 'show' : '' }}">
                <span class="menu-link">
                    <span class="menu-icon">
                        <i class="bi bi-moon-stars fs-3"></i>
                    </span>
                    <span class="menu-title">Haji Furoda</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg">
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/banner')? 'active' : '' }}"
                            href=" {{route('cms.haji.furoda.banner.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Banner</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/label') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.label.banner.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Label Banner</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/section-1') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.section.1.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 1</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/section-2') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.section.2.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 2</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/section-3') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.section.3.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 3</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/section-4') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.section.4.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 4</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/section-list-1') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.section.list.1.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section List 1</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/section-list-2') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.section.list.2.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section List 2</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/section-list-3') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.section.list.3.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section List 3</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '-furoda/section-list-4') ? 'active' : '' }}"
                            href="{{route('cms.haji.furoda.section.list.4.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section List 4</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Separator................... --}}
    <div data-kt-menu-trigger="click"
        class="menu-item menu-accordion {{ str_contains(request()->url(), '/content')  ? 'show' : '' }}">
        <span class="menu-link">
            <span class="menu-icon">
                <i class="bi bi-menu-down fs-3"></i>
            </span>
            <span class="menu-title">Website</span>
            <span class="menu-arrow"></span>
        </span>
        {{-- Sub Menu --}}
        <div class="menu-sub menu-sub-accordion menu-active-bg">
            <div class="menu-item">
                <a class="menu-link {{ str_contains(request()->url(), '/content/sosial-media') ? 'active' : '' }}"
                    href=" {{route('cms.content.sosial.media.index')}} ">
                    <span class="menu-icon">
                        <i class="bi bi-telegram fs-3"></i>
                    </span>
                    <span class="menu-title">Sosial Media</span>
                </a>
            </div>
            <div data-kt-menu-trigger="click"
                class="menu-item menu-accordion {{ str_contains(request()->url(), '/content/contact-us')? 'show' : '' }}">
                <span class="menu-link">
                    <span class="menu-icon">
                        <i class="bi bi-person-circle fs-3"></i>
                    </span>
                    <span class="menu-title">Hubungi Kami</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg">
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/contact-us/description')? 'active' : '' }}"
                            href=" {{route('cms.content.contact.us.description.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Deskripsi</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/contact-us/head-office')? 'active' : '' }}"
                            href=" {{route('cms.content.contact.us.head.office.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Head Office</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/contact-us/cabang')? 'active' : '' }}"
                            href=" {{route('cms.content.contact.us.cabang.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Cabang</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/contact-us/mitra')? 'active' : '' }}"
                            href=" {{route('cms.content.contact.us.mitra.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Mitra</span>
                        </a>
                    </div>
                </div>
            </div>
            <div data-kt-menu-trigger="click"
                class="menu-item menu-accordion {{ str_contains(request()->url(), '/content/about-us')? 'show' : '' }}">
                <span class="menu-link">
                    <span class="menu-icon">
                        <i class="bi bi-house-fill fs-3"></i>
                    </span>
                    <span class="menu-title">Tentang Kami</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg">
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/about-us/sejarah')? 'active' : '' }}"
                            href=" {{route('cms.content.about.us.sejarah.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Sejarah</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/about-us/visi')? 'active' : '' }}"
                            href=" {{route('cms.content.about.us.visi.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Visi</span>
                        </a>
                    </div>         
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/about-us/misi')? 'active' : '' }}"
                            href=" {{route('cms.content.about.us.misi.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Misi</span>
                        </a>
                    </div>                   
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/about-us/eksekutif')? 'active' : '' }}"
                            href=" {{route('cms.content.about.us.eksekutif.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Eksekutif</span>
                        </a>
                    </div>   
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/about-us/sambutan')? 'active' : '' }}"
                            href=" {{route('cms.content.about.us.sambutan.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Sambutan</span>
                        </a>
                    </div>                   
                </div>
            </div>
            <div data-kt-menu-trigger="click"
                class="menu-item menu-accordion {{ str_contains(request()->url(), '/content/faq')? 'show' : '' }}">
                <span class="menu-link">
                    <span class="menu-icon">
                        <i class="bi bi-question-diamond fs-3"></i>
                    </span>
                    <span class="menu-title">FAQ</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg">
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/faq/list')? 'active' : '' }}"
                            href=" {{route('cms.content.faq.list.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Daftar</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/faq/phone')? 'active' : '' }}"
                            href=" {{route('cms.content.faq.phone.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Nomor Kontak</span>
                        </a>
                    </div>                   
                </div>
            </div>
            <div data-kt-menu-trigger="click"
                class="menu-item menu-accordion {{ str_contains(request()->url(), '/content/jejak-islami')? 'show' : '' }}">
                <span class="menu-link">
                    <span class="menu-icon">
                        <i class="bi bi-cloud-moon-fill fs-3"></i>
                    </span>
                    <span class="menu-title">Jejak Islami</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg">
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/jejak-islami/section-1')? 'active' : '' }}"
                            href=" {{route('cms.content.jejak.islami.section-1.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Section 1</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/jejak-islami/provit')? 'active' : '' }}"
                            href=" {{route('cms.content.jejak.islami.provit.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Keunggulan</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/jejak-islami/trip-plan')? 'active' : '' }}"
                            href=" {{route('cms.content.jejak.islami.trip-plan.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Rencana Perjalanan</span>
                        </a>
                    </div>  
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/jejak-islami/trip-facility')? 'active' : '' }}"
                            href=" {{route('cms.content.jejak.islami.trip-facility.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Fasilitas Perjalanan</span>
                        </a>
                    </div>  
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/jejak-islami/no-include')? 'active' : '' }}"
                            href=" {{route('cms.content.jejak.islami.no-include.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Biaya Tidak Termasuk</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), '/content/jejak-islami/optional')? 'active' : '' }}"
                            href=" {{route('cms.content.jejak.islami.optional.index')}} ">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Opsional</span>
                        </a>
                    </div>                   
                </div>
            </div>
        </div>        
    </div>
    {{-- Separator................... --}}
    <div class="menu-item">
        <a class="menu-link {{ str_contains(request()->url(), 'article') ? 'active' : '' }}" href="{{route('cms.article.index')}}">
            <span class="menu-icon">
                <i class="bi bi-journals fs-3"></i>
            </span>
            <span class="menu-title">Artikel</span>
        </a>
    </div>
    {{-- Separator................... --}}
    <div class="menu-item">
        <a class="menu-link {{ str_contains(request()->url(), 'other/packet') ? 'active' : '' }}" href="{{route('cms.other.packet.index')}}">
            <span class="menu-icon">
                <i class="bi bi-journals fs-3"></i>
            </span>
            <span class="menu-title">Paket Lainnya</span>
        </a>
    </div>
</div>