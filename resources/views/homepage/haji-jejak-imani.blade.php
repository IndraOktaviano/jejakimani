@extends('layouts.homepage.master')

@section('title')
    Daftar Haji Plus Furoda Terbaik di Jakarta Bogor Depok Tangerang Bekasi
@endsection

@section('css')
    <style>
        #carouselHero .carousel-item {
            height: 600px;
            background: rgba(0, 0, 0, 0.781);
        }

        #carouselHero .carousel-item h1 {
            font-family: 'gotham-black';
            font-size: 60px;
        }

        #carouselHero .carousel-item p {
            font-family: 'gotham';
        }

        #carouselHero .carousel-item img {
            opacity: 0.3;
        }

        #carouselHero .carousel-caption {
            color: white;
            top: 45%;
            transform: translateY(-50%);
        }

        #carouselHero .carousel-caption .btn {
            background-color: #EEB22C;
            border-color: #EEB22C;
            border-radius: 50px;
            color: white;
            font-weight: bold;
        }

        #package {
            color: white;
            font-family: 'gotham';
        }

        #package .package1 {
            background: linear-gradient(0deg, rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.747)), url("{{ asset('images/haji/image-1.jpeg') }}");
            background-repeat: no-repeat;
            height: 600px;
            background-size: cover;
            background-position: center;
            border-radius: 15px;
        }

        #package .package2 {
            background: linear-gradient(0deg, rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.747)), url("{{ asset('images/haji/image-2.jpg') }}");
            background-repeat: no-repeat;
            height: 600px;
            background-size: cover;
            background-position: center;
            border-radius: 15px;
        }

        #package h2 {
            font-family: 'gotham-black';
        }

        #package p {
            font-size: 13px;
        }

        #package .btn {
            background: #EEAE2C;
            border-radius: 25px;
            color: white;
            font-weight: bold;
        }

        #carouselTestimoni .carousel-inner {
            /* height: 300px; */
            border-radius: 15px;
        }

        #carouselTestimoni .carousel-item {
            /* height: 300px; */
            background: rgba(0, 0, 0, 0.781);
        }

        #carouselTestimoni .carousel-item .row {
            /* height: 300px; */
        }

        #carouselTestimoni .carousel-item .col-6 {
            /* height: 300px; */
        }

        #carouselTestimoni .carousel {
            height: 300px;
        }

        #form .form {
            background-image: linear-gradient(0deg, rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.747)), url("{{ asset('images/jejak-imani-haji-form-bg.png') }}");
            color: white;
        }

        #form h1 {
            font-family: 'gotham-black';
        }

        #form p {
            font-family: 'gotham';
        }

        #others-package h1 {
            font-family: 'gotham-black';
        }

        @media (max-width: 430px) {
            #carouselHero .carousel-caption {
                top: 40%;
            }

            #carouselHero .carousel-item h1 {
                font-size: 30pt;
            }

            #carouselHero .carousel-item p {
                font-size: 9pt;
            }

            #carouselHero .carousel-item img {
                height: 100%;
                width: auto !important;
            }

            #carouselHero .carousel-caption .btn {
                font-size: 10pt;
            }

            #package .package1, #package .package2 {
                height: auto;
            }
        }

    </style>
@endsection

@section('content')
    <section id="hero">
        <div class="container-fluid p-0">
            <div id="carouselHero" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    @php
                        $i = 0;
                    @endphp
                    @foreach ($banner as $item)
                        <div class="carousel-item  {{ !$i++ ? 'active' : '' }}">
                            <img src="{{ asset($item->json) }}" class="d-md-block w-100" alt="{{ asset($item->json) }}">
                            <div class="carousel-caption d-md-block">
                                {!! $label_banner->name !!}
                                <p>{!! $label_banner->text !!}</p>
                                <br>
                                <br>
                                <a target="_blank"
                                    href="https://api.whatsapp.com/send?phone={{ $label_banner->phone }}&text{{ $label_banner->text_wa }}"
                                    @if ($label_banner->event) onclick="gtag('event', {{ $label_banner->event }}, { 'event_category': {{ $label_banner->event_category ?? '' }}, 'event_label': {{ $label_banner->event_label ?? '' }}});" @endif
                                    class="btn btn-warning text-light btn-shake">{{ $label_banner->label_wa }} <i
                                        class="fas fa-arrow-right"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselHero" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselHero" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </section>

    <section id="package">
        <div class="container pt-5 pb-4">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="package1 py-3 px-4 mb-3 mb-md-0">
                        {!! $haji_plus !!}
                        <a href="{{ route('haji-khusus.index') }}" class="btn btn-shake px-4">
                            LIHAT DETAILS
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="package2 py-3 px-4">
                        {!! $haji_furoda !!}
                        <a href="{{ route('haji-furoda.index') }}" class="btn btn-shake px-4">
                            LIHAT DETAILS
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="testimoni" class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="carouselTestimoni" class="carousel slide" data-bs-ride="carousel" data-bs-touch="true">
                        @php
                            $i = 0;
                        @endphp
                        <div class="carousel-inner" style="max-height: max-content">
                            @foreach ($testimoni as $item)
                                <div class="carousel-item {{ !$i++ ? 'active' : '' }}">
                                    <div class="d-flex justify-content-between row">
                                        @php
                                            $json = json_decode($item->json);
                                        @endphp
                                        <div
                                            class="col-md-7 col-sm-12 order-md-0 order-sm-1 text-light d-flex align-items-center ps-md-5 bg-black pt-3 pt-md-0">
                                            <p class="mx-3 mx-md-0">
                                                {{ $json->text }}
                                                <br><br>
                                                <span class="orange">
                                                    <strong>{{ $json->name }}</strong>, {{ $json->position }}
                                                </span>
                                            </p>
                                        </div>
                                        <div class="col-md-5 col-sm-12 order-md-1 order-sm-0 bg-black">
                                            <img src="{{ asset($json->image) }}" class="img-fluid"
                                                alt="{{ asset($json->image) }}">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="form" class="mb-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 bg-orange pt-3 pb-1">
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/3wnUTPTLLFE"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="col-12 col-md-6 form p-5">
                    <h1>FORM</h1>
                    <p>Jika anda ingin mengetahui lebih lanjut mengenai informasi Haji Bersama Jejak Imani, silakan isi form
                        dibawah ini dan klik tombol Submit. Kami akan menghubungi Anda.</p>
                    <form action="">
                        <div class="row g-3 align-items-center">
                            <div class="col-4">
                                <label for="namaLengkap" class="col-form-label">Nama Lengkap <span
                                        class="text-danger">*</span></label>
                            </div>
                            <div class="col-auto">
                                <input type="text" id="namaLengkap" name="namaLengkap" class="form-control">
                            </div>
                        </div>
                        <div class="row g-3 align-items-center mt-3">
                            <div class="col-4">
                                <label for="noHP" class="col-form-label">No.Handphone/WA <span
                                        class="text-danger">*</span></label>
                            </div>
                            <div class="col-auto">
                                <input type="text" id="noHP" name="noHP" class="form-control">
                            </div>
                        </div>
                        <div class="row g-3 align-items-center mt-3">
                            <div class="col-4">
                                <label for="emailAddress" class="col-form-label">Alamat Email <span
                                        class="text-danger">*</span></label>
                            </div>
                            <div class="col-auto">
                                <input type="text" id="emailAddress" name="emailAddress" class="form-control">
                            </div>
                        </div>
                        <div class="row g-3 align-items-center mt-3">
                            <div class="col-4">
                                <label for="message" class="col-form-label">Pesan <span
                                        class="text-danger">*</span></label>
                            </div>
                            <div class="col-auto">
                                <input type="text" id="message" name="message" class="form-control">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning btn-lg mt-3">
                            Kirim
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    @include('components.homepage.paket-lainnya')
@endsection
