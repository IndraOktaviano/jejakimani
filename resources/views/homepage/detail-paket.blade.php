@extends('layouts.homepage.master')

@section('title')
    [Detail Paket]
@endsection

@section('css')
    <style>
        #section-1 img {
            border-radius: 25px;
        }

        #section-1 h1 {
            font-size: 40pt;
            font-weight: bold;
        }

        #why-us .card {
            border: none;
            border-radius: 15px;
        }

        #why-us .card img {
            border-radius: 15px;
        }

        #form .form-label {
            font-family: 'gotham';
            font-weight: bold;
        }

        #fasilitas {
            background: #F2F2F2;
        }

        #form .form-control {
            border-radius: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-color: black;
            background-color: transparent;
            box-shadow: none;
        }

        #program .col-6 {
            position: relative;
            height: 405px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        #program .col-6 a {
            position: absolute;
            bottom: 10%;
            left: 20%;
            text-decoration: none;
            font-weight: bold;
            font-family: 'gotham';
        }

        #btn-wa .col-4 {
            border-radius: 45px;
        }

    </style>
@endsection

@section('content')
    <section id="section-1">
        <div class="container py-5">
            <div class="row">
                <div class="col-4">
                    <img src="{{ asset($data->image) }}" alt="{{ asset($data->image) }}"
                        srcset="{{ asset($data->image) }}" class="img-fluid">
                </div>
                <div class="col-8">
                    <h1>
                        {{ $data->name }}
                    </h1>
                    <br>
                    <a href="" class="btn btn-lg btn-warning text-light">
                        <h4 class="m-0 fw-normal">Klik disini untuk konsultasi gratis</h4>
                    </a>
                </div>
            </div>
            <div class="row justify-content-evenly mt-5">
                <div class="col-3 border-bottom border-dark">
                    <h5 class="text-center fw-normal mb-3">Maskapai</h5>
                </div>
                <div class="col-3 border-bottom border-dark">
                    <h5 class="text-center fw-normal mb-3">Harga Paket</h5>
                </div>
                <div class="col-3 border-bottom border-dark">
                    <h5 class="text-center fw-normal mb-3">Jadwal</h5>
                </div>
            </div>
            <div class="row justify-content-evenly mt-5">
                <div class="col-3">
                    <img src="{{ asset($data->maskapai) }}" alt="{{ asset($data->maskapai) }}" class="img-fluid">
                </div>
                <div class="col-3">
                    <h5 class="text-center fw-normal mb-3">Rp. {{ number_format($data->price, 2, ',', '.') }}</h5>
                </div>
                <div class="col-3">
                    <h5 class="text-center fw-normal mb-3">{{ date('d-m-Y') }}</h5>
                </div>
            </div>
        </div>
    </section>

    <section id="why-us" class="bg-grey">
        <div class="container bg-grey py-5">
            <h1 class="text-center sub-title orange mb-5">
                Mengapa Umrah Lebih Nyaman <br>
                bersama Jejak Imani?
            </h1>
            <div class="row">
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-07.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-07.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Saudia Arabian Airlines Landing Madinah
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-02.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-02.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Terakreditasi A
                                    </h4>
                                    <p class="card-text fw-bold">
                                        Akreditasi PPIU: No. 2020-PPIU 1-0011
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-04.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-04.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Hotel Lebih Dekat
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-16.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-16.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Dibimbing oleh Assatidz Mumpuni
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-11.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-11.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Menggunakan Radiophone
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-06.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-06.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        DP Cukup Rp 5 Juta
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-10.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-10.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Manasik di Hotel
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-01.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-01.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Berizin Resmi
                                        Kementerian Agama
                                    </h4>
                                    <p class="card-text fw-bold text-truncate">
                                        Izin Resmi PIHK: No. 394 Tahun 2021 <br>
                                        Izin Resmi PPIU: No. U.533 Tahun 2020
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="perjalanan">
        <div class="container mt-5">
            <h1 class="text-center">
                Itinerary Perjalanan
            </h1>
            <div class="row justify-content-center">
                <div class="col-12">
                    <img src="{{ asset('images/umrah/paket-umrah/Itinerary6A4B-1522FEB-1465x2048.jpg') }}"
                        alt="{{ asset('images/umrah/paket-umrah/Itinerary6A4B-1522FEB-1465x2048.jpg') }}"
                        class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section id="fasilitas">
        <div class="container mt-5">
            <h1 class="text-center">
                Fasilitas Perjalanan
            </h1>
            <div class="row justify-content-center">
                <div class="col-12">
                    <img src="{{ asset('images/umrah/paket-umrah/FD-NN-S-23JAN.jpg') }}"
                        alt="{{ asset('images/umrah/paket-umrah/FD-NN-S-23JAN.jpg') }}" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section id="form">
        <div class="container-fluid mb-5">
            <div class="row d-flex justify-content-evenly">
                <div class="col-12 col-md-6 bg-grey text-light p-4">
                    <h1 class="mb-3">Video Highlight</h1>

                    <iframe src="https://www.youtube.com/embed/QtT8swjxvSo" title="YouTube video player" height="80%"
                        width="100%" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                    </iframe>
                </div>
                <div class="col-12 col-md-6 bg-light p-4">
                    <form method="POST">
                        @csrf
                        @method('POST')
                        <div class="mb-4">
                            <h4>
                                Kami akan menghubungi anda
                            </h4>
                            <p>
                                Jika anda ingin mengetahui lebih lanjut mengenai informasi Haji Plus/Khusus Bersama Jejak
                                Imani, silakan isi form dibawah ini dan klik tombol Submit. Kami akan menghubungi Anda.
                            </p>
                        </div>
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">No. Handphone/WA <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="no_hp" name="no_hp">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Alamat Email <span
                                    class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="message" class="form-label">Pesan <span
                                    class="text-danger">*</span></label>
                            <textarea class="form-control" name="message" id="message" cols="30" rows="1"></textarea>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-warning text-light">SUBMIT <i
                                    class="fas fa-arrow-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="program">
        <div class="container-fluid">
            <h3>Program Umrah Jejak Imani</h3>
            <div class="row">
                <div class="col-6 bg-light">
                    <img src="{{ asset('images/umrah/paket-umrah/Tabungan-1.png') }}" alt="" srcset="">
                    <a href="" class="text-dark">Selengkapnya <i class="fas fa-chevron-circle-right"></i></a>
                </div>
                <div class="col-6 bg-grey">
                    <img src="{{ asset('images/umrah/paket-umrah/New-Project-1.png') }}" alt="" srcset="">
                    <a href="" class="text-light">Selengkapnya <i class="fas fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section id="btn-wa">
        <div class="container-fluid bg-orange py-5 d-flex justify-content-center mb-5">
            <div class="col-4 text-center shadow btn bg-light">
                <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Paket%20Umroh%20yang%20Di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website."
                    @if ($tracker) onclick="gtag('event', {{ $tracker->event }}, { 'event_category': {{ $tracker->event_category ?? '' }}, 'event_label': {{ $tracker->event_label ?? '' }}});" @endif
                    target="_blank" class="text-decoration-none text-dark">
                    <div class="row d-flex align-items-center">
                        <div class="col-3">
                            <i class="fab fa-whatsapp fa-5x"></i>
                        </div>
                        <div class="col-9">
                            <h4 class="text-center grey fw-normal">
                                Butuh Info Detail? <br>
                                Konsultasi Sekarang
                            </h4>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    @include('components.homepage.paket-lainnya')
@endsection
