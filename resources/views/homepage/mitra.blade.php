@extends('layouts.homepage.master')

@section('title')
    Cabang / Mitra Jejak Imani
@endsection

@section('css')
    <style>
        #hero {
            padding: 250px 0;
            color: #fff;
            background: #000;
        }

        #hero h1 {
            font-size: 55pt;
        }

        hr {
            opacity: 1;
            height: 2px !important;
            color: #000;
        }

        #mitra {
            background: #EFEFEF;
        }

    </style>
@endsection

@section('content')
    <section id="hero">
        <div class="container-fluid text-center">
            <h1>
                Cabang/Mitra Jejak Imani
            </h1>
        </div>
    </section>

    <section id="cabang">
        <div class="container py-5">
            <h3>Cabang</h3>
            <hr>
            <div class="row">
                <div class="col-4">
                    <h5 class="fw-bold">Jejak Imani Jogja</h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Jl Salakan II nomer 222 , Saman, Bangunharjo, Sewon, Bantul, Yogyakarta
                    </h6>
                </div>
                <div class="col-4">
                    <a href="http://wa.me/628112995755" class="orange fw-normal text-decoration-none">
                        <h6 class="fw-normal">
                            Kontak Sekarang Juga
                        </h6>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="mitra">
        <div class="container py-5">
            <h3>Mitra</h3>
            <hr>
            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Nur Cahyono
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Komplek Pamulang Regancy B1 No.15 Tangerang Selatan, Banten
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0859-6661-4393
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Anggi Muliawati
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        JL. Kota Bambu Utara II No. 08
                        Palmerah, Jakarta Barat
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0812-9731-5523
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Hendra Wahyudi
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        JL. Salvia 4 Blok UD/11 Sektor 1.2
                        Bumi Serpong Damai, Tangerang Selatan, Banten
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0821-1162-2345
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Tasrip
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        JL. Ir. Soekarno No. 96c
                        Kendari, Sulawesi Tenggara
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0812-4277-7700
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Mira Primadona
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        JL. Benda Barat 10, Blok D20 No.3
                        Pamulang 1 Tangerang Selatan, Banten
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0877-7155-2525
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Siti Roisah
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Villa indah Permai Blok I 26/26 Rt 008/036
                        Kel. Teluk Pucung Kec. Bekasi Utara, Kota Bekasi
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0821-1264-6120
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Hasan Ali Afi Kasuba
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Griya Pesona Barokah 3 No. 1 Kp. Pedurenan RT 004/011
                        Kel. Jatiluhur, Kec. Jatiasih, Kota Bekasi
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0852-1396-1755
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Hj. Puji Rahma Wijayanti
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        JL. Sakti Raya No. 23 RT/RW 05/06, Sangrilla Indah II
                        Kel. Patukangan Selatan, Kec. Pasanggahan, Jakarta Selatan
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0818-0810-1080
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Etik Mulyaningsih
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Pamulang Elok Blok P.1 No. 19 RT 003/014
                        Kel. Pondok Petir Kec. Bojongsari, Kota Depok
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0812-9366-2931
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Nurti P. Purbasari, ST
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Villa Pamulang, Cluster Bella Rosa RT 007/017
                        Kel. Pondok Benda Kec. Pamulang, Tangerang Selatan, Banten
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0821-2406-5740
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Winarni
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Parakan Langgah RT 09 RW 11
                        Banjarnegara, Jawa Tengah
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0813-2761-4388
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Ida Niyoman Heru Siswono
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Perum Dramaga Regancy 2 Blok F No. 3
                        Kabupaten Bogor
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0815-1985-1760
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Alfian Muhammad Zen
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        JL. Poncol II No. 1
                        Gandaria Selatan, Cilandak, Jakarta Selatan
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0878-7154-9739
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Linggo Busono
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        JL. Kemuning II Blok E. No. 22 RT 006/007
                        Taman Kedaung Pamulang, Tangerang Selatan
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0813-8728-6000
                    </h6>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-4">
                    <h5 class="fw-bold">
                        Indi Ayu Maretia
                    </h5>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        Komplek Marsan Sejahtera JL. IKA II No. 4A
                        Pekanbaru, Riau
                    </h6>
                </div>
                <div class="col-4">
                    <h6 class="fw-normal">
                        0812-1278-1776
                    </h6>
                </div>
            </div>
        </div>
    </section>
@endsection
