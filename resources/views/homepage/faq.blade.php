@extends('layouts.homepage.master')

@section('title')
    FAQ
@endsection

@section('css')
    <style>
        #hero {
            height: 100vh;
            padding: 250px 0;
            color: #fff;
            background: #000;
        }

        #hero h1 {
            font-size: 55pt;
        }

        #faq .linktr {
            font-family: 'gotham-black';
            font-size: 25pt;
            color: #000;
        }

        #faq .accordion-button {
            font-size: 20pt;
            font-family: 'gotham-black';
            font-weight: bold;
            color: #595959;
        }

        #faq .accordion-button:hover {
            color: #EEB12C;
        }

        #faq .accordion-button::after {
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z'/%3e%3c/svg%3e");
        }

        #faq .accordion-button:not(.collapsed)::after {
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z'/%3e%3c/svg%3e");
        }

        #faq .accordion-button:not(.collapsed) {
            color: #EEB12C;
            background-color: #fff;
            box-shadow: none;
        }

        #faq .accordion-item {
            border-left: none;
            border-right: none;
            border-top: none;
        }

        @media (max-width: 430px) {
            #hero h1 {
                font-size: 25pt;
            }

            #faq .accordion-button {
                font-size: 14pt;
            }

            #faq h1 {
                font-size: 11pt;
            }
        }

    </style>
@endsection

@section('content')
    <section id="hero">
        <div class="container-fluid text-center">
            <h1>Frequently Asked Question</h1>
        </div>
    </section>

    <section id="faq">
        <div class="container py-5 mt-5">
            <div class="accordion mb-5" id="accordionExample">
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Paket perjalanan apa saja yang tersedla dl Jejak Imanl?
                        </button>
                    </h1>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Jejak Imani Travel Umrah & Islamic Tours mempunyai Paket:
                            <br><br>
                            Umrah Emerald & Shapphire (Reguler) <br>
                            Umrah Ruby (Super Hemat) <br>
                            Jejak Islami (Halal Tour)
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Apa perbedaan paket Emerald, Sapphire & Ruby
                        </button>
                    </h1>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Perbedaan paket Emerald, Shapphire & Ruby antara lain:
                            <br><br>
                            Paket Ruby memakai hotel bintang 3, paket Emerald memakai hotel bintang 4 dan paket Shapphire
                            memakai hotel bintang 5 <br>
                            Koper paket Ruby & Emerald dari bahan kain, sementara paket Shapphire dari bahan fiber
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Adakah Program Unggulan dari Jejak Imani?
                        </button>
                    </h1>
                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Ada, Promo Unggulan dari Jejak Imani yaitu:
                            <br><br>
                            Umrah Jejak Nabi yang dibimbing langsung oleh ustadz Salim A. Fillah <br>
                            Umrah Awal Ramadhan yang dibimbing langsung oleh ustadz Salim A. Fillah <br>
                            Jejak Islami Turkey yang dipandu langsung oleh ustadz Salim A. Fillah
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Apa perbedaan kamar Quad, Triple, dan Double?
                        </button>
                    </h1>
                    <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Kamar Quad adalah kamar hotel yang diisi oleh empat orang. <br>
                            Kamar Triple adalah kamar hotel yang diisi oleh tiga orang <br>
                            Kamar Double adalah kamar hotel yang diisi oleh dua orang.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingFive">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Kapan dan dimanakah tempat pelaksanaan manasik?
                        </button>
                    </h1>
                    <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Untuk Umrah Reguler/Jejak Islami pelaksanaan manasik dilakukan di Hotel Aston
                            Simatupang/setaraf. Untuk Umrah Super Hemat dilakukan di Swiss-Belhotel Airport/setaraf.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingSix">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Apa saja dokumen yang perlu saya persiapkan untuk umrah?
                        </button>
                    </h1>
                    <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Dokumen yang harus disiapkan antara lain:
                            <br><br>
                            KTP <br>
                            Passport Aktif minimal 8 bulan dan terdiri dari 2 suku kata <br>
                            Kartu Keluarga <br>
                            Akta Lahir (untuk anak dibawah 17 tahun) <br>
                            Surat Nikah <br>
                            Pasphoto 4×6 <br>
                            Surat Meningitis Aktif <br>
                            Mengisi Formulir Umrah yang disediakan Jejak Imani
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingSeven">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            Saya memerlukan bantuan dalam pembuatan paspor.
                        </button>
                    </h1>
                    <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Kami membantu para calon Tamu Allah dalam pembuatan surat rekomendasi Kementrian Agama untuk
                            pembuatan passport.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingEight">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                            Kapan saya harus bayar DP? Kapan batas akhir pelunasan biaya umrah?
                        </button>
                    </h1>
                    <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Para Calon Tamu Allah dapat membayar DP untuk booking seat setelah mengisi formulir dari Jejak
                            Imani. Batas pelunasan H-30 hari dari keberangkatan.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingNine">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                            Saya tidak bisa berangkat umrah ditanggal yang sepakati karena suatu hal. Saya ingin pindah
                            jadwal keberangkatan.
                        </button>
                    </h1>
                    <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Para Calon Tamu Allah bisa me<i><b>reschedule</b></i> pemberangkatan umrahnya maksimal dua kali.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="headingTen">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                            Budget saya tidak cukup untuk Umrah.
                        </button>
                    </h1>
                    <div id="collapseTen" class="accordion-collapse collapse" aria-labelledby="headingTen"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Ada. Untuk memudahkan Para Tamu Allah untuk melaksanakan Umrah kami membuka program:
                            <br><br>
                            Umrah Duluan Bayar Belakangan. Program pembiayaan Umrah ini bekerjasama dengan lembaga keuangan
                            syariah yang diawasi oleh Otoritas Jasa Keuangan dan Dewan Syariah Nasional MUI <br>
                            Tabungan Umrah. Program tabungan umrah kami bekerjasama dengan beberapa bank untuk memudah Para
                            Tamu Allah menabung umrah
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <a href="https://linktr.ee/jejakimani" class="text-decoration-none linktr">
                    <h1>
                        Bila anda membutuhkan bantuan <br>
                        lebih lanjut sila hubungi kami di:
                    </h1>
                </a>

                <h1 class="black mt-5 pt-5">
                    021 2274 1322 | 0857 2002 8100 | 0811 8246 988
                </h1>
            </div>
        </div>
    </section>
@endsection
