@extends('layouts.homepage.master')

@section('title')
    Travel Umroh Paket Umroh Terbaik di Jakarta Depok Tangerang Bekasi
@endsection

@section('css')
    <style>
        .hero-1 {
            margin-top: 100px;
        }

        .hero-2 {
            background: linear-gradient(rgba(10, 6, 6, 0.84), rgba(10, 6, 6, 0.84)), url('../images/download.jpg');
            background-size: cover;
            height: 700px;
        }

        .hero-3 {
            background: url('../images/bg-2.png');
            background-size: cover;
            height: 420px;
        }

        .hero-4 .box {
            background-color: black;
            height: 650px;
            width: 560px;
        }

        .hero-4 .box p {
            font-family: 'gotham-black';
        }

        .hero-4 .box h2 {
            font-family: 'gotham-black';
        }

        .hero-4 .box .text {
            font-family: 'gotham';
            font-size: 12pt;
        }

        #partner h1 {
            font-family: 'gotham';
        }

        #partner .card {
            border: 0;
        }

        @media (max-width: 767px) {
            .carousel-inner .carousel-item>div {
                display: none;
            }

            .carousel-inner .carousel-item>div:first-child {
                display: block;
            }
        }

        .carousel-inner .carousel-item.active,
        .carousel-inner .carousel-item-next,
        .carousel-inner .carousel-item-prev {
            display: flex;
        }

        /* medium and up screens */
        @media (min-width: 768px) {
            .carousel-inner .carousel-item-end.active,
            .carousel-inner .carousel-item-next {
                transform: translateX(25%);
            }

            .carousel-inner .carousel-item-start.active,
            .carousel-inner .carousel-item-prev {
                transform: translateX(-25%);
            }

            #partner .img-fluid {
                max-width: 80% !important;
            }
        }

        .carousel-inner .carousel-item-end,
        .carousel-inner .carousel-item-start {
            transform: translateX(0);
        }

        @media (max-width: 420px) {
            #hero-1 .sub-title {
                font-weight: 900;
                font-style: 'gotham-black';
            }
        }

    </style>
@endsection

@section('js')
    <script>
        let items = document.querySelectorAll('.carousel .carousel-item')

        items.forEach((el) => {
            const minPerSlide = 6
            let next = el.nextElementSibling
            for (var i = 1; i < minPerSlide; i++) {
                if (!next) {
                    // wrap carousel by using first child
                    next = items[0]
                }
                let cloneChild = next.cloneNode(true)
                el.appendChild(cloneChild.children[0])
                next = next.nextElementSibling
            }
        })
    </script>
@endsection

@section('content')
    <section class="hero-1">
        <div class="container py-5">
            <div class="row my-md-5">
                <div class="col-12 d-flex justify-content-center">
                    <p class="lh-1 text-center title-primary black">
                        UMRAH LEBIH NYAMAN <br>
                        <span style="font-family: gotham-medium">Bersama</span> JEJAK IMANI
                    </p>
                </div>
            </div>
            <div class="row mb-5 mt-md-0 mt-5">
                <div class="col-12 d-flex justify-content-center">
                    <p class="lh-1 text-center sub-title black fw-bold">
                        Umrah Lebih Nyaman, Landing Madinah, Hotel Lebih Dekat,
                        Tersedia Tabungan Umrah, Menggunakan Radiophone.
                    </p>
                </div>
                <div class="col-12 d-flex justify-content-center">
                    <a href="{{ route('paket-umrah.index') }}" class="text-decoration-none me-4"
                        style="font-family: gotham-black; font-size: 15pt; color: #333333">
                        Jadwal Umrah <i class="fas fa-chevron-circle-right ms-2"></i>
                    </a>
                    <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20bustomi%20Saya%20Mau%20tanya%20paket%20umrohnya%20atau%20hajinya"
                        target="_blank" class="text-decoration-none"
                        style="font-family: gotham-black; font-size: 15pt; color: #333333">
                        Daftar Sekarang <i class="fas fa-chevron-circle-right ms-2"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="hero-2 d-flex align-items-center">
        <div class="container-fluid">
            <div class="row mb-5 d-flex justify-content-center">
                <div class="col-12 col-lg-10 text-light">
                    <h1 class="fw-bold title">
                        UMRAH JEJAK NABI
                    </h1>
                    <p class="sub-title">
                        Mari bertamu pada Allah, menyusuri senyum
                        dan air mata Rasulullah ﷺ bersama kami.
                    </p>
                    <div class="row mt-5">
                        <div class="col-3">
                            <a href="{{ route('paket-umrah.index') }}" class="text-decoration-none text-light"
                                style="font-family: gotham-black; font-size: 15pt;">
                                Jadwal Umrah <i class="fas fa-chevron-circle-right ms-2"></i>
                            </a>
                        </div>
                        <div class="col-3">
                            <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20bustomi%20Saya%20Mau%20tanya%20paket%20umrohnya%20atau%20hajinya"
                                target="_blank" class="text-decoration-none text-light"
                                style="font-family: gotham-black; font-size: 15pt;">
                                Daftar Sekarang <i class="fas fa-chevron-circle-right ms-2"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="hero-3 d-flex align-items-center">
        <div class="container d-flex justify-content-center">
            <div class="row">
                <div class="col-12">
                    <h1 class="title black text-center">
                        JEJAK ISLAMI
                    </h1>
                    <p class="text black text-center">
                        Jejak Islami membuat perjalanan wisata anda lebih bermakna. <br> Lebih Nyaman Menggunakan
                        Radiophone.
                    </p>
                </div>
                <div class="col-12 d-flex justify-content-center">
                    <a href="{{ route('jejakislami.index') }}" class="text-decoration-none me-4"
                        style="font-family: gotham-black; font-size: 15pt; color: #000000">
                        Selengkapnya <i class="fas fa-chevron-circle-right ms-2"></i>
                    </a>
                    <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20bustomi%20Saya%20Mau%20tanya%20paket%20umrohnya%20atau%20hajinya"
                        target="_blank" class="text-decoration-none"
                        style="font-family: gotham-black; font-size: 15pt; color: #000000">
                        Daftar Sekarang <i class="fas fa-chevron-circle-right ms-2"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="hero-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 px-0">
                    <img src="{{ asset('images/Placeholder3-2.jpg') }}" class="img-fluid" alt="" srcset="">
                </div>
                <div class="col-12 col-md-6 d-flex align-items-center pe-5">
                    <div class="row d-flex justify-content-end">
                        <div class="col-12">
                            <h1 class="title black text-end">
                                UMRAH DIAMOND
                            </h1>
                        </div>
                        <div class="col-10 text-end">
                            <p class="text black">
                                Rencanakan perjalanan umrah khusus
                                bersama keluarga, kolega, atau
                                orang-orang terdekat anda.
                            </p>
                            <a href="{{ route('umroh-diamond.index') }}" class="text-decoration-none me-4"
                                style="font-family: gotham-black; font-size: 15pt; color: #000000">
                                Selengkapnya <i class="fas fa-chevron-circle-right ms-2"></i>
                            </a>
                            <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20bustomi%20Saya%20Mau%20tanya%20paket%20umrohnya%20atau%20hajinya"
                                target="_blank" class="text-decoration-none"
                                style="font-family: gotham-black; font-size: 15pt; color: #000000">
                                Rencanakan Sekarang <i class="fas fa-chevron-circle-right ms-2"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="hero-4 mt-3">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-md-6 box px-md-5 d-flex align-items-center">
                    <a href="{{ route('keunggulan-jejak-imani.index') }}" class="text-decoration-none">
                        <div class="row">
                            <div class="col-12 col-md-10">
                                <img class="img-fluid" src="{{ asset('images/image-1.png') }}" alt="">
                            </div>
                            <div class="col-12 mt-3 text-light">
                                <p class="fw-bold">
                                    Keunggulan Jejak Imani
                                </p>
                                <h2 class="fw-bold">
                                    Keunggulan Jejak Imani
                                </h2>
                                <p class="text">
                                    Jejak Imani Umrah & Islamic Tours memiliki 19 keunggulan yang akan memberikan kenyamanan
                                    bagi Jamaah selama melaksanakan ibadah Umrah.
                                </p>
                            </div>
                            <div class="col-12"></div>
                        </div>
                    </a>
                </div>

                <div class="col-12 col-md-6 box px-md-5 d-flex align-items-center">
                    <a href="{{ route('profil-ustadz.index') }}" class="text-decoration-none">
                        <div class="row">
                            <div class="col-12 col-md-10">
                                <img class="img-fluid" src="{{ asset('images/image-2.png') }}" alt="">
                            </div>
                            <div class="col-12 mt-3 text-light">
                                <p class="fw-bold">
                                    Profil Ustadz Pembimbing
                                </p>
                                <h2 class="fw-bold">
                                    Profil Ustadz Pembimbing
                                </h2>
                                <p class="text">
                                    Jejak Imani mempunyai Pembimbing Umrah yang mumpuni. Mumpuni dari segi keilmuan maupun
                                    dari segi kemampuan menyampaikan. Ustadz Pembimbing Jejak Imani antara lain Ustadz Salim
                                    A Fillah, Ustadz Ahmad Ridwan, Lc, Ustadz Muhammad Fauzil Adhim S.Psi, Ustadz Hafidz
                                    Devian, Lc dan ustadz-ustadz lainnya.
                                </p>
                            </div>
                            <div class="col-12"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="partner" class="hero-5 my-5">
        <h1 class="fw-bold text-center">
            PARTNER KAMI
        </h1>
        <div class="container">
            <div class="row mx-auto my-auto justify-content-center">
                <div id="recipeCarousel" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Kementerian-Agama.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-KAN.png') }}" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Sertifikasi-LSU-1.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-AMPHURI-1-3.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Saudia-Airlines.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-BSM-1.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Aston-Priority-Simatupang.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-5-Pasti-Umrah-1.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-KADIN-2.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Indosat.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Garuda-1.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Muamalat.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-SUCOFINDO-1.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-BNI-Syariah-01.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Paguyuban-PPIU-Banten.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Hijup.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-LAZADA-01.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Duta-Mode-1.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Bakerzine-01.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-SwissBel-Hotel.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Qaid-01.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Hafil.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Azka-Al-Safa.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Dar-Al-Eiman-01.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Oman-Air.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Emirates.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Qatar-Airways.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Etihad-Airways.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Turkish-Airlines.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Telkomsel.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Kopindosat.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-JSC.png') }}" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Hidayatullah.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Hidayatullahcom.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Alvin-Adam-Co.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Miss-Marina.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-ACT.png') }}" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Mbok-Moro-1.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Koperasi-Sahabat-Ihya.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{ asset('images/partner/Resize-Dhezine.png') }}"
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="video">
        <div class="container">
            <iframe width="100%" height="480" src="https://www.youtube.com/embed/86ihAFXmNEM" title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
        </div>
    </section>
@endsection
