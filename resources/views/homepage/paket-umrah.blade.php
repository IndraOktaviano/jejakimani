@extends('layouts.homepage.master')

@section('title')
    Paket Umroh Daftar Haji
@endsection

@section('css')
    <style>
        #hero {
            background: url("{{ 'images/umrah/paket-umrah/hero.jpg' }}");
            height: 500px;
            background-size: cover;
            background-position: center;
            display: flex;
            align-items: center;
        }

        #hero-2 .col-12 {
            position: relative;
            height: 405px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        #hero-2 .col-12 a {
            position: absolute;
            bottom: 10%;
            left: 20%;
            text-decoration: none;
            font-weight: bold;
            font-family: 'gotham';
        }

        .box-scroll {
            overflow: auto;
            white-space: nowrap;
        }

        .box-scroll .box-item {
            display: inline-block;
            max-width: 300px;
        }

        .box-scroll img {
            max-width: 300px;
            max-height: 300px;
            border-radius: 15px;
            box-shadow: 0 0 10px black;
        }

        .box-scroll h5 {
            font-family: 'gotham';
            font-weight: bold;
        }

        #why-us .card {
            border: none;
            border-radius: 15px;
        }

        #why-us .card img {
            border-radius: 15px;
        }

        #form .form-label {
            font-family: 'gotham';
            font-weight: bold;
        }

        #form .form-control {
            border-radius: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-color: black;
            background-color: transparent;
            box-shadow: none;
        }

        #btn-wa .col-4 {
            border-radius: 45px;
        }

        @media (max-width: 400px) {
            #hero-12 .col-12 {
                height: auto;
                width: 100%;
            }

            #hero {
                height: 235px;
            }

            .title {
                font-size: 16pt;
            }
        }

    </style>
@endsection

@section('content')
    <section id="hero">
        <div class="container-fluid d-flex justify-content-center text-light">
            <div class="text-center">
                <h1 class="title">UMRAH LEBIH NYAMAN</h1>
                <h1 class="title">bersama JEJAK IMANI</h1>
                <a href="" class="btn btn-warning btn-rounded-new btn-shake">
                    Klik disini untuk konsultasi gratis <i class="fab fa-whatsapp ms-2"></i>
                </a>
            </div>
        </div>
    </section>

    <section id="hero-2">
        <div class="container-fluid mb-5 shadow">
            <div class="row">
                <div class="col-12 col-md-6 bg-light">
                    <img src="{{ asset('images/umrah/paket-umrah/Tabungan-1.png') }}" class="img-fluid" alt=""
                        srcset="">
                    <a href="{{ route('tabungan-umrah.index') }}" class="text-dark btn-shake">Selengkapnya <i
                            class="fas fa-chevron-circle-right"></i></a>
                </div>
                <div class="col-12 col-md-6 bg-grey">
                    <img src="{{ asset('images/umrah/paket-umrah/New-Project-1.png') }}" class="img-fluid" alt=""
                        srcset="">
                    <a href="{{ url('badal-umrah') }}" class="text-light btn-shake">Selengkapnya <i
                            class="fas fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section id="paket">
        @foreach ($category as $index => $item)
            @php
                $data = \App\Models\Umroh::where('umroh_category_id', $item->id)
                    ->orderBy('id', 'DESC')
                    ->get();
            @endphp
            @if (count($data))
                <div class="container-fluid mb-5">
                    <h3 class="fw-bold mb-4">
                        {{ $item->text }}
                    </h3>
                    <div class="overflow-hidden position-relative">
                        <div class="box-scroll p-2 position-abosulte overflow-auto">
                            @foreach ($data as $sub)
                                <div class="box-item me-5">
                                    <a href="{{ route('paket-umrah.show', $sub->slug) }}">
                                        <img src="{{ asset($sub->image) }}" alt="{{ asset($sub->image) }}"
                                            srcset="{{ asset($sub->image) }}" class="mb-3 img-fluid">
                                    </a>
                                    <h5 class="text-truncate">
                                        {{ $sub->name }}
                                    </h5>
                                    <p class="text-muted ms-2">
                                        Rp. {{ number_format($sub->price, 2, ',', '.') }}
                                    </p>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    @if ($index + 1 != count($category))
                        <div class="row d-flex align-items-center mt-5">
                            <div class="col-2 col-md-4 pb-1">
                                <hr>
                            </div>
                            <div class="col-8 col-md-4 d-flex align-items-center justify-content-center">
                                <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Paket%20Umroh%20yang%20Di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website."
                                    target="_blank" rel="noopener noreferrer"
                                    @if ($tracker) onclick="gtag('event', {{ $tracker->event }}, { 'event_category': {{ $tracker->event_category ?? '' }}, 'event_label': {{ $tracker->event_label ?? '' }}});" @endif
                                    class="btn btn-lg btn-warning btn-rounded-new btn-shake">Tanya Dulu
                                    konsultasi gratis <i class="fab fa-whatsapp ms-2"></i></a>
                            </div>
                            <div class="col-2 col-md-4 pb-1">
                                <hr>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
        @endforeach
    </section>

    <section id="why-us" class="bg-grey">
        <div class="container bg-grey py-5 text-center">
            <h1 class="text-center sub-title orange mb-5">
                Mengapa Umrah Lebih Nyaman <br>
                bersama Jejak Imani?
            </h1>
            <div class="row collapse" id="collapseExample">
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-07.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-07.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Saudia Arabian Airlines Landing Madinah
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-02.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-02.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Terakreditasi A
                                    </h4>
                                    <p class="card-text fw-bold">
                                        Akreditasi PPIU: No. 2020-PPIU 1-0011
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-04.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-04.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Hotel Lebih Dekat
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-16.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-16.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Dibimbing oleh Assatidz Mumpuni
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-11.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-11.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Menggunakan Radiophone
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-06.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-06.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        DP Cukup Rp 5 Juta
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-10.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-10.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Manasik di Hotel
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3 bg-orange p-0">
                        <div class="row g-0 d-flex align-items-center">
                            <div class="col-md-5">
                                <img src="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-01.jpg') }}"
                                    class="img-fluid"
                                    alt="{{ asset('images/umrah/keunggulan/Grafis-Keunggulan-Web-01.jpg') }}">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body grey">
                                    <h4 class="card-title">
                                        Berizin Resmi
                                        Kementerian Agama
                                    </h4>
                                    <p class="card-text fw-bold text-truncate">
                                        Izin Resmi PIHK: No. 394 Tahun 2021 <br>
                                        Izin Resmi PPIU: No. U.533 Tahun 2020
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="orange" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
                aria-controls="collapseExample">
                <i class="fas fa-chevron-down"></i>
            </a>
        </div>
    </section>

    <section id="testimonial">
        <div class="container-fluid p-0">
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/umrah/paket-umrah/testimoni/New-Project-1.png') }}"
                            class="d-block w-100"
                            alt="{{ asset('images/umrah/paket-umrah/testimoni/New-Project-1.png') }}">
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('images/umrah/paket-umrah/testimoni/Testimoni-Desktop-Atika-1.jpg') }}"
                            class="d-block w-100"
                            alt="{{ asset('images/umrah/paket-umrah/testimoni/Testimoni-Desktop-Atika-1.jpg') }}">
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('images/umrah/paket-umrah/testimoni/Testimoni-Desktop-Megah-1.jpg') }}"
                            class="d-block w-100"
                            alt="{{ asset('images/umrah/paket-umrah/testimoni/Testimoni-Desktop-Megah-1.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="form">
        <div class="container-fluid mb-5">
            <div class="row d-flex justify-content-evenly">
                <div class="col-12 col-md-6 bg-grey text-light p-4">
                    <h1 class="mb-3">Video Highlight</h1>

                    <iframe src="https://www.youtube.com/embed/86ihAFXmNEM" title="YouTube video player" height="80%"
                        width="100%" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                    </iframe>
                </div>
                <div class="col-12 col-md-6 bg-light p-4">
                    <form method="POST">
                        @csrf
                        @method('POST')
                        <div class="mb-4">
                            <h4>
                                Kami akan menghubungi anda
                            </h4>
                            <p>
                                Jika anda ingin mengetahui lebih lanjut mengenai informasi Haji Plus/Khusus Bersama Jejak
                                Imani, silakan isi form dibawah ini dan klik tombol Submit. Kami akan menghubungi Anda.
                            </p>
                        </div>
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">No. Handphone/WA <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="no_hp" name="no_hp">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Alamat Email <span
                                    class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="message" class="form-label">Pesan <span
                                    class="text-danger">*</span></label>
                            <textarea class="form-control" name="message" id="message" cols="30" rows="1"></textarea>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-warning text-light btn-shake">SUBMIT <i
                                    class="fas fa-arrow-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="btn-wa">
        <div class="container-fluid bg-orange py-5 d-flex justify-content-center mb-5">
            <div class="col-12 col-md-4 text-center shadow btn bg-light">
                <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Paket%20Umroh%20yang%20Di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website."
                    target="_blank" class="text-decoration-none text-dark">
                    <div class="row d-flex align-items-center">
                        <div class="col-3">
                            <i class="fab fa-whatsapp fa-5x"></i>
                        </div>
                        <div class="col-9">
                            <h4 class="text-center grey fw-normal">
                                Butuh Info Detail? <br>
                                Konsultasi Sekarang
                            </h4>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    @include('components.homepage.paket-lainnya')
@endsection
