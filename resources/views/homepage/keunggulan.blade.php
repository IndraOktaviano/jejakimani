@extends('layouts.homepage.master')

@section('title')
Keunggulan Jejak Imani Archives - Jejak Imani | Paket Umroh Haji Resmi Terbaik Exclusive Berkualitas di Jakarta, Bogor, Depok, Tangerang, Bekasi, Bandung, Jabodetabek
@endsection

@section('css')
<style>
    h3 {
        font-size: 27pt;
        font-family: 'gotham-medium'
    }
    h1 {
        font-size: 57pt;
    }
</style>
@endsection

@section('content')
    <section>
        <div class="container py-5">
            <h3>Category</h3>
            <h1 class="mb-5">Keunggulan Jejak Imani</h1>
            <hr>
            <div class="row mt-5">
                <div class="col-12 col-md-6">
                    <p>
                        Kemenag RI : No. 924 Tahun 2017. Jejak Imani aman dan terpercaya dengan adanya legalitas dari Kemenag.
                        <br>
                        Radiophone sangat memudahkan jamaah dalam mendengarkan kajian oleh Ustadz Pembimbing.
                        <br>
                        Radius sampai 200 meter.
                    </p>
                </div>

                <div class="col-12 col-md-6">
                    <p>
                        Landing Madinah menghemat waktu 6 – 7 jam perjalanan. Jamaah dapat memaksimalkan waktunya untuk beribadah.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
