@extends('layouts.homepage.master')

@section('title')
    Paket Umroh Diamond
@endsection

@section('css')
    <style>
        #hero {
            color: #000000;
        }

        #hero h1 {
            font-size: 55pt;
        }

        #kategori .list-1 {
            background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url("{{ asset('images/umrah/umrah-diamond/Placeholder3-2.jpg') }}");
        }

        #kategori .list-1:hover {
            background: linear-gradient(rgba(0, 0, 0, 0.392), rgba(0, 0, 0, 0.392)), url("{{ asset('images/umrah/umrah-diamond/Placeholder3-2.jpg') }}");
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

        #kategori .list-2 {
            background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url("{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-11-17-at-12.32.46-AM.jpeg') }}");
        }

        #kategori .list-2:hover {
            background: linear-gradient(rgba(0, 0, 0, 0.392), rgba(0, 0, 0, 0.392)), url("{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-11-17-at-12.32.46-AM.jpeg') }}");
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

        #kategori .list-3 {
            background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url("{{ asset('images/umrah/umrah-diamond/Placeholder8-scaled.jpg') }}");
        }

        #kategori .list-3:hover {
            background: linear-gradient(rgba(0, 0, 0, 0.392), rgba(0, 0, 0, 0.392)), url("{{ asset('images/umrah/umrah-diamond/Placeholder8-scaled.jpg') }}");
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

        #kategori .col-12 {
            height: 40rem;
            color: white;
            display: flex;
            align-items: center;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

        #keunggulan {
            background: #CBCBCB;
        }

        #keunggulan h3,
        #keunggulan h4 {
            color: #000;
        }

        #keunggulan .card-img-wrap {
            overflow: hidden;
            position: relative;
        }

        #keunggulan .card-img-wrap:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            opacity: 0;
            transition: opacity .25s;
        }

        #keunggulan .card-img-wrap img {
            transition: transform .25s;
            width: 100%;
        }

        #keunggulan .card-img-wrap:hover img {
            transform: scale(1.2);
        }

        #keunggulan .card-img-wrap:hover:after {
            opacity: 1;
        }

        #keunggulan .card-body {
            height: 20rem;
            background: #000000;
        }

        #video {
            background: linear-gradient(rgba(228, 228, 228, 0.7), rgba(228, 228, 228, 0.7)), url("{{ asset('images/umrah/umrah-diamond/Placeholder8-scaled.jpg') }}");
            background-size: cover;
        }

        #video .container-fluid {
            height: 35rem;
        }

        #video i {
            transition: transform .2s;
        }

        #video i:hover {
            transform: scale(1.5);
        }

        #galeri .fill {
            display: flex;
            justify-content: center;
            align-items: center;
            overflow: hidden;
            height: 450px;
        }

        #galeri .fill img {
            object-fit: cover;
            width: 100%;
            flex-shrink: 0;
        }

        #galeri img {
            filter: brightness(40%);
            transition: .2s;
        }

        #galeri img:hover {
            filter: brightness(100%);
        }

        #cs {
            padding: 100px 0;
            color: #000;
        }

        #cs h4 {
            font-family: 'gotham';
        }

        #cs a:hover {
            color: #000;
        }

        #cs hr {
            opacity: 1;
        }

        .parent {
            width: 100%;
            overflow-x: hidden;
            float: left;
        }

        .child {
            width: 1000px;
            float: left;
            cursor: pointer;
        }

    </style>
@endsection

@section('js')
    <script>
        const slider = document.querySelector('.parent');
        let mouseDown = false;
        let startX, scrollLeft;

        let startDragging = function(e) {
            mouseDown = true;
            startX = e.pageX - slider.offsetLeft;
            scrollLeft = slider.scrollLeft;
        };
        let stopDragging = function(event) {
            mouseDown = false;
        };

        slider.addEventListener('mousemove', (e) => {
            e.preventDefault();
            if (!mouseDown) {
                return;
            }
            const x = e.pageX - slider.offsetLeft;
            const scroll = x - startX;
            slider.scrollLeft = scrollLeft - scroll;
        });

        // Add the event listeners
        slider.addEventListener('mousedown', startDragging, false);
        slider.addEventListener('mouseup', stopDragging, false);
        slider.addEventListener('mouseleave', stopDragging, false);
    </script>
@endsection

@section('content')
    <section id="hero">
        <div class="container-fluid text-center py-5 px-5">
            <h1 class="mb-5">
                UMRAH DIAMOND
            </h1>
            <h2 class="mb-5">
                Rencanakan perjalanan umrah khusus bersama keluarga,<br>
                kolega kerja, atau orang-orang terdekat anda.
            </h2>
            <h2 class="mb-5">
                Mulai dari 25 Jutaan
            </h2>
            <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20bustomi%20Saya%20Mau%20tanya%20paket%20umrohnya%20atau%20hajinya"
                class="fst-italic text-dark text-decoration-none fw-bold">Rencanakan Sekarang <i
                    class="fas fa-chevron-circle-right"></i></a>
        </div>
    </section>

    <section id="kategori">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-4 list-1 text-center">
                    <div class="row justify-content-center">
                        <div class="col-7">
                            <h2>
                                UMRAH PRIVATE FAMILY
                            </h2>
                            <h5 class="mb-5">
                                Merekatkan kembali makna keluarga sakinah mawaddah warahmah dalam nuansa Umrah bersama.
                            </h5>
                            <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20bustomi%20Saya%20Mau%20tanya%20paket%20umrohnya%20atau%20hajinya"
                                class="fst-italic text-light text-decoration-none fw-bold">Rencanakan Sekarang <i
                                    class="fas fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4 list-2 text-center">
                    <div class="row justify-content-center">
                        <div class="col-7">
                            <h2>
                                UMRAH CORPORATE
                            </h2>
                            <h5 class="mb-5">
                                Umrah bersama kolega kerja. Saling mendoakan dengan nuansa syahdu di depan Ka'bah.
                            </h5>
                            <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20bustomi%20Saya%20Mau%20tanya%20paket%20umrohnya%20atau%20hajinya"
                                class="fst-italic text-light text-decoration-none fw-bold">Rencanakan Sekarang <i
                                    class="fas fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4 list-3 text-center">
                    <div class="row justify-content-center">
                        <div class="col-7">
                            <h2>
                                UMRAH PRIVATE COMMUNITY
                            </h2>
                            <h5 class="mb-5">
                                Berkumpul bersama teman komunitas dalam nuansa beribadah Umrah dengan eratan silaturahim
                                yang tak pernah lepas.
                            </h5>
                            <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamualaikum%20Mas%20bustomi%20Saya%20Mau%20tanya%20paket%20umrohnya%20atau%20hajinya"
                                class="fst-italic text-light text-decoration-none fw-bold">Rencanakan Sekarang <i
                                    class="fas fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="keunggulan">
        <div class="container py-5">
            <h3 class="text-center mt-5">Keunggulan Jejak Imani</h3>
            <h4 class="text-center mt-4">Jejak Imani Umrah & Islamic Tours Memiliki 19 Keunggulan yang akan memberikan
                kenyamanan bagi jamaah selama melaksanakan Ibadah Umrah.</h4>
            <div class="row mt-5">
                <div class="col-4">
                    <div class="card">
                        <div class="card-img-wrap">
                            <img src="{{ asset('images/umrah/umrah-diamond/logo_kemenag-3-600x403.jpg') }}"
                                class="card-img-top"
                                alt="{{ asset('images/umrah/umrah-diamond/logo_kemenag-3-600x403.jpg') }}">
                        </div>
                        <div class="card-body p-5 text-light rounded-bottom">
                            <h4 class="card-title">Berizin Resml</h4>
                            <p class="card-text">
                                Kemenag RI : No. 924 Tahun 2017. Jejak Imani aman dan terpercaya
                                dengan adanya legalitas..
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="card">
                        <div class="card-img-wrap">
                            <img src="{{ asset('images/umrah/umrah-diamond/Radiophone1-600x403.jpg') }}"
                                class="card-img-top"
                                alt="{{ asset('images/umrah/umrah-diamond/Radiophone1-600x403.jpg') }}">
                        </div>
                        <div class="card-body p-5 text-light rounded-bottom">
                            <h4 class="card-title">Menggunakan Radiophone</h4>
                            <p class="card-text">
                                Radiophone sangat memudahkan
                                jamaah dalam mendengarkan
                                kajian oleh Ustadz Pembimbing.
                                Radius sampai 200 meter.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="card">
                        <div class="card-img-wrap">
                            <img src="{{ asset('images/umrah/umrah-diamond/Landing-Madinah-600x403.jpg') }}"
                                class="card-img-top"
                                alt="{{ asset('images/umrah/umrah-diamond/Landing-Madinah-600x403.jpg') }}">
                        </div>
                        <div class="card-body p-5 text-light rounded-bottom">
                            <h4 class="card-title">Landing Madinah</h4>
                            <p class="card-text">
                                Landing Madinah menghemat
                                waktu 6 - 7 jam perjalanan.
                                Jamaah dapat memaksimalkan
                                waktunya untuk beribadah.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="video">
        <div class="container-fluid d-flex align-items-center justify-content-center">
            <div class="text-center">
                <h1 class="mb-4">Umrah Diamond Private</h1>
                <h5 class="mb-5">Rencanakan perjalanan umrah khusus bersama keluarga, kolega kerja, atau
                    orang-orang terdekat anda.</h5>
                <a href="#" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    <i class="far fa-play-circle fa-7x text-dark"></i>
                </a>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                        <iframe height="494" src="https://www.youtube.com/embed/xQCPsfW8dnk" title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="galeri">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-md-6 p-0">
                            <img src="{{ asset('images/umrah/umrah-diamond/Koper-Hemat2.jpeg') }}" alt=""
                                class="img-fluid">
                        </div>
                        <div class="col-md-6 p-0">
                            <img src="{{ asset('images/umrah/umrah-diamond/Koper-Hemat2.jpeg') }}" alt=""
                                class="img-fluid">
                        </div>
                        <div class="col-md-6 p-0">
                            <img src="{{ asset('images/umrah/umrah-diamond/Koper-Hemat2.jpeg') }}" alt=""
                                class="img-fluid">
                        </div>
                        <div class="col-md-6 p-0">
                            <img src="{{ asset('images/umrah/umrah-diamond/Koper-Hemat2.jpeg') }}" alt=""
                                class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 p-0">
                    <div class="fill">
                        <img src="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}"
                            alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cs">
        <div class="container text-center">
            <h1>
                Rencanakan Perjalanan Umrah Khusus Bersama Keluarga, <br>
                Kolega Kerja, atau Orang-orang terdekat Anda.
            </h1>
            <hr>
            <h4 class="mb-5">
                SIAP MENJADI TAMU ALLAH KE TANAH SUCI?
            </h4>
            <a href="" class="orange text-decoration-none">
                <div class="row justify-content-center align-items-center">
                    <div class="col-3">
                        <h5>
                            Rencanakan Sekarang
                            <br> WHATSAPP KAMI
                            <br> (KONSULTASI GRATIS)
                        </h5>
                    </div>
                    <div class="col-1 text-start p-0">
                        <i class="fas fa-chevron-circle-right"></i>
                    </div>
                </div>
            </a>
            <hr>
        </div>
    </section>

    {{-- <section id="travel-lainnya">
        <div class="container-fluid parent">
            <div class="child">
                <div class="row">
                    <img class="img-fluid"
                        src="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}"
                        alt="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}">
                    <img class="img-fluid"
                        src="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}"
                        alt="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}">
                    <img class="img-fluid"
                        src="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}"
                        alt="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}">
                    <img class="img-fluid"
                        src="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}"
                        alt="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}">
                    <img class="img-fluid"
                        src="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}"
                        alt="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}">
                    <img class="img-fluid"
                        src="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}"
                        alt="{{ asset('images/umrah/umrah-diamond/WhatsApp-Image-2019-12-09-at-10.41.38.jpeg') }}">
                </div>
            </div>
        </div>
    </section> --}}
@endsection
