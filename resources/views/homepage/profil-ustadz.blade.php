@extends('layouts.homepage.master')

@section('title')
    Profil Ustadz Pembimbing
@endsection

@section('css')
    <style>
        #hero {
            background: #000000;
            color: white;
        }

        #hero .container-fluid {
            height: 85vh;
        }

        #hero h1 {
            font-size: 55pt;
        }

        #list {
            background: #333333;
            color: white;
        }

        #list hr {
            height: 3px;
            color: #EDB02C;
            background-color: #EDB02C;
            opacity: 1;
            width: 70%;
        }

    </style>
@endsection

@section('content')
    <section id="hero">
        <div class="container-fluid d-flex align-items-center justify-content-center">
            <h1>Profil Ustadz Pembimbing</h1>
        </div>
    </section>

    <section id="list">
        <div class="container-fluid">
            <div class="row align-items-center pb-5">
                <div class="col-6">
                    <img class="img-fluid" src="{{ asset('images/ustadz/Ustadz-Salim-A.-Fillah-scaled.jpg') }}"
                        alt="Profil Ustadz">
                </div>
                <div class="col-6">
                    <h2>USTADZ SALIM A. FILLAH</h2>
                    <hr>
                    <p>
                        Ustadz Salim A. Fillah lahir di Yogyakarta, Daerah Istimewa Yogyakarta, 21 Maret 1984. Beliau
                        seorang
                        penulis buku islami dari Yogyakarta, Indonesia. Namanya mulai dikenal luas setelah menerbitkan buku
                        Nikmatnya Pacaran setelah pernikahan (2003). Selain penulis buku, Salim A. Fillah merupakan penggiat
                        Masjid Jogokariyan Yogyakarta. Saat ini beliau menjadi Komisaris Utama PT. Jejak Imani Berkah
                        Bersama
                        (Jejak Imani).
                    </p>
                    <p class="text-end">
                        Umrah Bersama Ust. Salim A. Fillah
                    </p>
                </div>
            </div>
            <div class="row align-items-center pb-5">
                <div class="col-6">
                    <img class="img-fluid" src="{{ asset('images/ustadz/Ustadz-Ahmad-Ridwan-scaled.jpg') }}"
                        alt="Profil Ustadz">
                </div>
                <div class="col-6">
                    <h2>USTADZ AHMAD RIDWAN, Lc.</h2>
                    <hr>
                    <p>
                        Ustadz Ahmad Ridwan adalah Pengisi Acara Islam Itu Indah Trans TV.
                    </p>
                    <p class="text-end">
                        Umrah Bersama Ust. Ahmad Ridwan
                    </p>
                </div>
            </div>
            <div class="row align-items-center pb-5">
                <div class="col-6">
                    <img class="img-fluid" src="{{ asset('images/ustadz/Screen-Shot-2019-08-13-at-23.43.51.png') }}"
                        alt="Profil Ustadz">
                </div>
                <div class="col-6">
                    <h2>USTADZ FAUZIL ADHIM S. Psi</h2>
                    <hr>
                    <p>
                        Mohammad Fauzil Adhim. Itulah nama lengkapnya. Seorang Ustadz kosmopolitan yang memiliki segudang
                        karya. Selain sebagai seorang pendakwah, ustadz Fauzil (panggilannya) ini juga merupakan seorang
                        penulis yang aktif dan produktif. Beberapa tulisannya begitu menyentuh dan menginspirasi banyak
                        orang. Ya, dia adalah penulis buku tentang pendidikan anak-anak sekaligus pakar parenting Islam dan
                        konsultan pernikahan.
                    </p>
                    <p class="text-end">
                        Umrah Bersama Ust. Fauzil Adhim
                    </p>
                </div>
            </div>
            <div class="row align-items-center pb-5">
                <div class="col-6">
                    <img class="img-fluid" src="{{ asset('images/ustadz/Ustadz-Hepi-Andi-Bastoni.jpg') }}"
                        alt="Profil Ustadz">
                </div>
                <div class="col-6">
                    <h2>USTADZ HEPI ANDI BASTONI M.A. M.Pd.I</h2>
                    <hr>
                    <p>
                        Lahir dari keluarga sederhana di Baturaja, Sumatera Selatan, 5 Nopember 1975. Hepi Andi juga juga
                        dikenal sebagai pembicara tetap pada beberapa kajian rutin khususnya di bidang spesialisasinya:
                        Sirah Nabawiyah. Secara rutin, ia mengisi Kajian Rutin Sirah Nabawiyah di Radio MARS (Madinatur
                        Rasul) Pusat Pengembangan Islam (PPIB) Bogor saban Rabu pagi, Kajian Sabtu Sore di Masjid al-Ghifari
                        Bogor, Kajian Ahad Sore di Masjid At-Taqwa Pakojan Empang Bogor, Kajian Sirah Nabawiyah di Masjid
                        Mahabbatur Rasul Vila Bogor Indah Bogor, dan beberapa tempat lainnya. Di Jakarta, ia menjadi pengisi
                        tetap beberapa pengajian kantor, seperti Telkomsel Gatot Subroto, Nestle Simatupang, PT Jasamarga
                        dan lainnya.
                        <br>
                        Selain aktif mengisi seminar dan kajian ke-Islam di beberapa tempat, ia juga menjadi dosen beberapa
                        perguruan tinggi. Antara lain: sebagai dosen Mata Kuliah Jurnalistik Islam pada Sekolah Tinggi Agama
                        Islam Indonesia (STAI Indo) di Jakarta, dosen Mata Kuliah Sirah Nabawiyah di Ma’ha al-Khalifah
                        Jakarta Islamic School, dan dosen Mata Kuliah Agama Islam di Akademi Kimia Analis Bogor.
                    </p>
                    <p class="text-end">
                        Umrah Bersama Ust. Hepi Andi Bastoni
                    </p>
                </div>
            </div>
            <div class="row align-items-center pb-5">
                <div class="col-6">
                    <img class="img-fluid" src="{{ asset('images/ustadz/Oemar-Mita2-e1574924687732.jpeg') }}"
                        alt="Profil Ustadz">
                </div>
                <div class="col-6">
                    <h2>USTADZ OEMAR MITA, Lc.</h2>
                    <hr>
                    <p>
                        Oemar Mita lahir di Kudus, 14 Mei 1979. Sejak kecil, ia sudah akrab dengan nilai-nilai Islam dengan
                        menuntut ilmu di Pondok Pesantren Darusy Syahadah. Setelah menamatkan sekolah, ia pun melanjutkan
                        studinya di Lembaga Ilmu Pengetahuan Islam dan Arab (LIPIA) Fakultas Syariah.
                    </p>
                    <p class="text-end">
                        Umrah Bersama Ust. Oemar Mitta, Lc
                    </p>
                </div>
            </div>
            <div class="row align-items-center pb-5">
                <div class="col-6">
                    <img class="img-fluid" src="{{ asset('images/ustadz/Luqmanul-Hakim4.jpeg') }}"
                        alt="Profil Ustadz">
                </div>
                <div class="col-6">
                    <h2>USTADZ LUQMANULHAKIM</h2>
                    <hr>
                    <p>
                        Ustadz Luqmanulhakim adalah Founder dari PASKAS. Beliau adalah pengasuh dari Pesantren Modern Munzalan, Pontianak.
                    </p>
                    <p class="text-end">
                        Umrah Bersama Ust. Luqmanulhakim
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
