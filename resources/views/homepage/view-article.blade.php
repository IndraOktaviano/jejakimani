@extends('layouts.homepage.master')

@section('title')
    {{ $article->name }}
@endsection

@section('css')
    <style>
        #hero {
            background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("{{ asset($article->image) }}");
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }

        #hero .container {
            height: 30rem;
            color: white
        }

        #other-article {
            background: #F5F5F5;
        }

    </style>
@endsection

@section('content')
    <section id="hero">
        <div class="container d-flex align-items-center py-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10">
                    <h1 class="text-center title">
                        {{ $article->name }}
                    </h1>
                </div>
            </div>
        </div>
    </section>

    <section id="article">
        <div class="container py-5">
            <div class="row">
                <div class="col-12">
                    {!! $article->json !!}
                </div>
            </div>
        </div>
    </section>

    <section id="other-article">
        <div class="container py-5">
            <div class="row">
                @foreach ($others as $item)
                    <div class="col-12 col-sm-6 col-md-4">
                        <a href="" class="text-decoration-none text-dark">
                            <div class="card">
                                <img src="{{ asset($item->image) }}" class="card-img-top"
                                    alt="{{ asset($item->image) }}">
                                <div class="card-body">
                                    <p class="card-text fw-bold">{{ $item->name }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section id="reply">
        <div class="container py-5">
            <h3>Leave a Reply</h3>
            <form class="row g-3">
                <div class="col-12">
                    <label for="inputAddress" class="form-label">My Comment Is..</label>
                    <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="col-md-4">
                    <label for="nama" class="form-label">Name*</label>
                    <input type="text" class="form-control" id="nama">
                </div>
                <div class="col-md-4">
                    <label for="email" class="form-label">Email*</label>
                    <input type="email" class="form-control" id="email">
                </div>
                <div class="col-md-4">
                    <label for="website" class="form-label">Website</label>
                    <input type="text" class="form-control" id="website">
                </div>
                <div class="col-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck">
                        <label class="form-check-label" for="gridCheck">
                            Save My Name, Email, And Website In This Browser For The Next Time I Comment.
                        </label>
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-warning">Submit Comment</button>
                </div>
            </form>
        </div>
    </section>
@endsection
