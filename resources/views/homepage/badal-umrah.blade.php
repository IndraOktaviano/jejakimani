@extends('layouts.homepage.master')

@section('title')
    Paket Umrah Daftar Haji
@endsection

@section('css')
    <style>
        .container {
            font-size: 11pt;
        }

        .green {
            color: #3A810D;
        }

        table {
            width: 100%;
        }

        th,
        td {
            font-family: 'gotham';
            border: 1px solid rgba(128, 128, 128, 0.412);
            padding-top: 8px;
            padding-bottom: 8px;
            padding-left: 16px;
        }

    </style>
@endsection

@section('content')
    <section>
        <div class="container pt-5">
            <p class="mb-4">
                Badal Umroh 2021 – Badal umroh adalah mewakilkan pelaksanaan ibadah umroh untuk orang lain. Dalam
                pelaksanaan
                badal umroh dapat dilakukan oleh pihak keluarga maupun pihak lain (bukan anggota keluarga), dengan syarat
                yang
                melaksanakan badal umroh telah melaksanakan ibadah umroh untuk diri sendiri terlebih dahulu.
            </p>

            <div class="text-center mb-4">
                <figure>
                    <img src="{{ asset('images/umrah/badal-umrah/badal-umroh-2021-1.jpg') }}" alt="">
                    <figcaption>Source: Google.com</figcaption>
                </figure>
            </div>

            <p class="fw-bold mb-4">
                Hadis Abu Razin Al-Uqaili, beliau mendatangi Nabi Muhammad ﷻ dan bertanya,
            </p>

            <p class="text-center mb-4">
                Wahai Rasulullah, ayahku sudah sangat tua, tidak mampu haji, umrah, dan perjalanan. Beliau menjawab,
                “Hajikanlah
                ayahmu dan umrahkanlah.” (HR. Ibnu Majah, Tirmidzi dan Nasa’i)
            </p>

            <p class="mb-4">
                Dari hadis diatas, dijabarkan anjuran untuk melakukan badal umroh bagi seseorang yang tidak mampu
                melaksanakannya. Mari tunaikan badal umroh keluarga dan sanak saudara maupun kerabat insya Allah siapapun
                bisa
                berumroh, silakan kontak kami disini, tim kami dengan senang hati membantu:
            </p>

            <p class="text-center">
                <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Badal%20Umroh%20yang%20Di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website."
                    class="text-decoration-none orange mb-4">
                    WhatsApp Badal Umroh ? Tanya Dulu Konsultasi Gratis
                </a>
            </p>

            <p class="fw-bold text-center mb-4">
                Biaya Badal Umroh 2021: Rp 2,400,000 -an
            </p>

            <p class="fw-bold text-center mb-4">
                Kuota: <span class="green">Tersedia</span>
            </p>

            <table class="mb-5">
                <tr>
                    <th class="fw-bold">Fasilitas</th>
                    <th class="fw-bold">Persyaratan</th>
                </tr>
                <tr>
                    <td>Pelaksana Badal Umroh dari Tim Khidmat Jejak Imani di Tanah Suci.</td>
                    <td>Mengisi Formulir Pendaftaran Badal Umroh, <a
                            href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20isi%20formulir%20prihal%20Badal%20Umroh%20yang%20Di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website."
                            class="text-decoration-none orange">disini.</a></td>
                </tr>
                <tr>
                    <td>Satu Pelaksana Membadalkan Satu Jiwa.</td>
                    <td>Membayar Lunas Biaya Badal Umroh,</td>
                </tr>
                <tr>
                    <td>Sertifikat Badal Umroh. </td>
                    <td>Status yang Dibadalkan Harus yang Sudah Meninggal / Sakit Berat.</td>
                </tr>
                <tr>
                    <td>Dokumentasi Prosesi Badal Umroh</td>
                    <td>Mengikuti Kuota yang Tersedia.</td>
                </tr>
            </table>

            <p class="text-center">
                <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Badal%20Umroh%20yang%20Di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website."
                    class="text-decoration-none orange mb-4">WhatsApp Badal Umroh 2021 ? Tanya Dulu Konsultasi Gratis</a>
            </p>

            <p class="mb-4">
                Jejak Imani Travel Haji, Umroh & Islamic Tours adalah perusahaan yang bergerak di bidang jasa travel Biro
                Haji,
                Umroh & Islamic Halal Tour. Jejak Imani berdiri sejak tahun 2014 dengan berbentuk Perseroan Terbatas (PT.)
                dengan nama PT. Jejak Imani Berkah Bersama. Jejak Imani sudah berizin resmi, dengan nomor <b>PPIU No. U.553
                    Tahun
                    2020 dan akreditasi PPIU bernilai A</b> dengan <b>nomor 2020-PPIU 1-0011</b>. Anda juga dapat melihat
                detail
                jelasnya di
                sini <a href="{{ route('about.index') }}" class="text-decoration-none orange">Tentang Kami.</a>
            </p>

            <p class="mb-4">
                Ingin badal umroh di tahun 2021 yang aman dan terpercaya? Jejak Imani jawabannya.
            </p>

            <p class="text-center">
                <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Badal%20Umroh%20yang%20Di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website."
                    class="text-decoration-none orange mb-4">Tanya Dulu Badal Umroh 2021 disini Konsultasi Gratis</a>
            </p>

            <p class="mb-4">Semoga Bermanfaat!!</p>
        </div>
    </section>
@endsection
