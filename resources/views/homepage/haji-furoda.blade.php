@extends('layouts.homepage.master')

@section('title')
    Daftar Haji Furoda Online Resmi Jakarta Bogor Depok Tangerang Bekasi
@endsection

@section('css')
    <style>
        #app {
            background-color: #ECEDF0;
        }

        #hero {
            background: url("{{ asset('images/haji-furoda/Umrah-Jejak-Nabi.png') }}");
            background-size: cover;
            text-align: center;
            color: white;
            padding-top: 15%;
            padding-bottom: 15%;
        }

        #hero h1 {
            font-size: 45pt;
        }

        #desc .desc-1,
        #desc .desc-2,
        #desc .desc-3,
        #desc .tentatif,
        #desc .akomodasi,
        #desc .infografis {
            border-radius: 15px;
        }

        #desc .desc-2 {
            background: linear-gradient(0deg, rgba(0, 0, 0, 0.466), rgba(0, 0, 0, 0.466)),
                url("{{ asset($section_1->image) }}");
            height: 450px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            color: white;
        }

        #desc .desc-3 {
            background: linear-gradient(0deg, rgba(0, 0, 0, 0.466), rgba(0, 0, 0, 0.466)),
                url("{{ asset($section_2->image) }}");
            height: 450px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            color: white;
        }

        #desc .desc-3 .package {
            position: relative;
            height: 350px;
            width: 90%;
            color: white;
        }

        #desc .desc-3 .package .package-bottom {
            position: absolute;
            bottom: 0;
            left: 5;
        }

        #desc .tentatif .card,
        #desc .akomodasi .card {
            border: none;
            height: 350px;
        }

        #desc .akomodasi .card img {
            border-radius: 15px;
        }

        #desc .akomodasi p {
            font-size: 12pt;
        }

        #desc .infografis {
            background: linear-gradient(0deg, rgba(0, 0, 0, 0.466), rgba(0, 0, 0, 0.466)),
                url("{{ asset('images/haji-furoda/jejak-imani-tabungan-haji-alur-perjalanan-haji.jpg') }}");
            height: 650px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            color: white;
            position: relative;
        }

        #desc .infografis .center {
            position: absolute;
            top: 30%;
        }

        #desc .infografis .bottom {
            position: absolute;
            bottom: 30%;
        }

        #accordion .accordion-item {
            background-color: #567C49;
            border-radius: 25px;
        }

        #accordion .accordion-button {
            color: white;
            background-color: #163B15;
            border-radius: 25px;
            font-family: 'gotham-black';
            font-weight: bold;
            font-size: 20px;
        }

        #accordion .accordion-button:not(.collapsed) {
            color: white;
            background-color: #163B15;
        }

        #accordion .accordion-collapse {
            color: white;
            background-color: #567C49;
            border-bottom-left-radius: 25px;
            border-bottom-right-radius: 25px;
            font-family: 'gotham';
        }

        #form img {
            height: 450px;
            width: 100%;
            border-radius: 15px;
        }

        #form iframe {
            width: 100%;
            height: 500px;
        }

        #form .form-label {
            font-family: 'gotham';
            font-weight: bold;
        }

        #form .form-control {
            border-radius: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-color: black;
            background-color: transparent;
            box-shadow: none;
        }

        #form .rounded {
            border-radius: 15px !important;
        }

        @media (max-width: 430px) {
            #hero {
                height: 100vh;
                display: flex;
                align-items: center;
            }

            #hero h1 {
                font-size: 20pt;
            }
        }

    </style>
@endsection

@section('content')
    <section id="hero">
        <div class="container px-5">
            {!! $label_banner->name !!}
            {!! $label_banner->text !!}
            <br>
            <br>
            <a href="https://api.whatsapp.com/send?phone={{ $label_banner->phone }}&text={{ $label_banner->text_wa }}"
                @if ($label_banner->event) onclick="gtag('event', {{ $label_banner->event }}, { 'event_category': {{ $label_banner->event_category ?? '' }}, 'event_label': {{ $label_banner->event_label ?? '' }}});" @endif
                target="_blank" class="btn btn-rounded-new btn-shake">{{ $label_banner->label_wa }}</a>
        </div>
    </section>

    <section id="desc">
        <div class="container p-4">
            <div class="text-center">
                <h1>HAJI MANDIRI JEJAK IMANI TANPA ANTRI</h1>
                <p>Haji mandiri Jejak Imani adalah program Haji Khusus dengan menggunakan visa furoda/undangan dari Kerajaan
                    Arab Saudi. Program ini sangat membantu anda dalam kemudahan pendaftaran haji tanpa perlu menunggu. Hal
                    ini
                    dikarenakan visa haji furoda tidak menggunakan kuota visa haji yang disediakan oleh pemerintah Republik
                    Indonesia, melainkan menggunakan kuota haji Kerajaan Arab Saudi.
                    <br>
                    Maka jamaah haji mandiri dapat mendaftar tanpa perlu mengantri lama.
                </p>
            </div>

            <div class="row d-flex justify-content-evenly mb-4">
                <div class="col-12 col-md-5 desc-2 d-flex align-items-center mb-4 mb-md-0">
                    <div class="text-center">
                        {!! $section_1->text !!}
                    </div>
                </div>
                <div class="col-12 col-md-5 desc-3 d-flex align-items-center">
                    <div class="package px-3">
                        {!! $section_2->name !!}
                        <div class="package-bottom">
                            <h5>
                                {!! $section_2->label_price !!}
                            </h5>
                            <a href="https://api.whatsapp.com/send?phone={{ $section_2->phone }}&text{{ $section_2->text_wa }}"
                                @if ($section_2->event) onclick="gtag('event', {{ $section_2->event }}, { 'event_category': {{ $section_2->event_category ?? '' }}, 'event_label': {{ $section_2->event_label ?? '' }}});" @endif
                                target="_blank" class="btn btn-warning btn-rounded-new mb-3 mt-3 btn-shake">
                                {{ $section_2->label_wa }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-evenly mb-4">
                <div class="col-12 col-md-5 akomodasi bg-grey p-4 mb-4 mb-md-0">
                    <h2 class="text-light">Haji Lebih Nyaman Bersama Jejak Imani</h2>
                    <div id="layananControl" class="carousel slide" data-bs-touch="false" data-bs-interval="false">
                        <div class="carousel-inner">
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($section_list_1 as $item)
                                @php
                                    $json = json_decode($item->json);
                                @endphp
                                <div class="carousel-item {{ !$i++ ? 'active' : '' }}">
                                    <div class="col-auto">
                                        <div class="card bg-grey text-light">
                                            <img src="{{ asset($json->image) }}" class="card-img-top"
                                                alt="{{ asset($json->image) }}">
                                        </div>
                                        <div class="mt-4 text-light">
                                            <h5>{{ $json->name }}</h5>
                                            <p>{!! $json->text !!}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#layananControl"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#layananControl"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
                <div class="col-12 col-md-5 infografis p-4">
                    <h2>
                        Infografis Perjalanan Haji
                    </h2>
                    <div class="center pe-2">
                        <p>
                            Program Haji Furoda Jejak Imani Tanpa Antri selama 23 hari yaitu mulai 03 – 25 Dzulhijjah
                        </p>
                    </div>
                    <div class="bottom">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-warning mb-3 btn-rounded-new" data-bs-toggle="modal"
                            data-bs-target="#grafisModal">
                            LIHAT PERJALANAN HAJI
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="grafisModal" tabindex="-1" aria-labelledby="grafisModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="btn-close sticky-top" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                        <img class="w-100"
                                            src="{{ asset('images/haji-furoda/jejak-imani-tabungan-haji-alur-perjalanan-haji.jpg') }}"
                                            alt="{{ asset('images/haji-furoda/jejak-imani-tabungan-haji-alur-perjalanan-haji.jpg') }}"
                                            srcset="{{ asset('images/haji-furoda/jejak-imani-tabungan-haji-alur-perjalanan-haji.jpg') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <a href="{{ asset('images/haji-furoda/grafis_haji-compressed.pdf') }}" target="_blank"
                            download="Grafis Haji Jejak Imani" class="btn btn-warning btn-rounded-new">UNDUH PERJALANAN
                            HAJI</a>
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-evenly">
                <div class="col-12 col-md-5 tentatif bg-green p-4 mb-4 mb-md-0">
                    <h2 class="text-light">Haji Lebih Nyaman Bersama Jejak Imani</h2>
                    <div id="tentatifControl" class="carousel slide" data-bs-touch="false" data-bs-interval="false">
                        <div class="carousel-inner">
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($section_list_2 as $item)
                                @php
                                    $json = json_decode($item->json);
                                @endphp
                                <div class="carousel-item {{ !$i++ ? 'active' : '' }}">
                                    <div class="card bg-green text-light">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $json->name }}</h5>
                                            <span class="card-text">
                                                {!! $json->text !!}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#tentatifControl"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#tentatifControl"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
                <div class="col-12 col-md-5 akomodasi bg-green p-4">
                    <h2 class="text-light">Akomodasi</h2>
                    <div id="akomodasiControl" class="carousel slide" data-bs-touch="false" data-bs-interval="false">
                        <div class="carousel-inner">
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($section_list_3 as $item)
                                @php
                                    $json = json_decode($item->json);
                                @endphp
                                <div class="carousel-item {{ !$i++ ? 'active' : '' }}">
                                    <div class="col-auto">
                                        <div class="card bg-green text-light">
                                            <img src="{{ asset($json->image) }}" class="card-img-top"
                                                alt="{{ asset($json->image) }}">
                                        </div>
                                        <div class="mt-5 pt-2 text-light">
                                            <h5>{{ $json->name }}</h5>
                                            {!! $json->text !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#akomodasiControl"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#akomodasiControl"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="accordion">
        <div class="container mt-2">
            <div class="row d-flex justify-content-center">
                <div class="col-11">
                    <div class="accordion" id="accordionExample">
                        @foreach ($section_list_4 as $item)
                            @php
                                $json = json_decode($item->json);
                            @endphp
                            <div class="accordion-item mb-4">
                                <h2 class="accordion-header" id="{{'headingOne'.$item->id}}">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#{{'collapseOne'.$item->id}}" aria-expanded="true" aria-controls="{{'collapseOne'.$item->id}}">
                                        {{ $json->name }}
                                    </button>
                                </h2>
                                <div id="{{'collapseOne'.$item->id}}" class="accordion-collapse collapse" aria-labelledby="{{'headingOne'.$item->id}}"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        {!! $json->text !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="accordion-item mb-4">
                            <h2 class="accordion-header" id="headingSeven">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    FAQ
                                </button>
                            </h2>
                            <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <h5>
                                        Apa bedanya Haji Plus/Haji Khusus dengan Haji Reguler langsung dari Kemenag?
                                    </h5>
                                    <br>
                                    <p>
                                        Haji reguler dikelola oleh Kementerian Agama dan KBIH dengan waktu tunggu 20-35
                                        tahun
                                        tergantung
                                        kota, fasilitas ⭐️ 3 & 4. Haji Plus/Haji Khusus dikelola oleh swasta dengan waktu
                                        tunggu 5-8
                                        tahun, fasilitas ⭐️ 5, hotel yang digunakan didepan Masjid Nabawi & Masjidil Haram.
                                        <br><br>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                    <h5>
                                        Adakah flyer informasi Badal Haji?
                                    </h5>
                                    <br>
                                    <p>
                                        Silakan klik <a class="text-warning text-decoration-none"
                                            href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.">link
                                            ini</a>
                                        untuk informasi detail Badal Haji.

                                        <br><br>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                    <h5>
                                        Apa bisa jika suami/istri atau keluarga yang pendaftaran Haji Plus/Haji Khususnya
                                        berbeda
                                        tahun ingin berangkat di tahun yang sama?
                                    </h5>
                                    <br>
                                    <p>
                                        Apabila selisih pendaftarannya tidak lebih dari 2 tahun, maka masih bisa diupayakan.
                                        Bisa
                                        atau tidaknya juga tergantung dengan aturan terbaru Kementerian Agama.

                                        <br><br>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                    <h5>
                                        Apa kelebihan dan keunggulan Haji Plus/Haji Khusus bersama Jejak Imani?
                                    </h5>
                                    <br>
                                    <ul>
                                        <li>
                                            <b>Terakreditasi A</b> Jejak Imani dengan standar pelayanan yang sangat baik,
                                            telah
                                            terakreditasi A oleh Kementerian Agama RI
                                        </li>
                                        <li>
                                            <b>Profesional</b> Tim Khidmat Jejak Imani berpengalaman melayani tamu-tamu
                                            Allah dengan
                                            sepenuh
                                            hati
                                        </li>
                                        <li>
                                            <b>Bimbingan Intensif Pembimbing</b> Jejak Imani adalah ustadz dengan background
                                            keilmuan yang bisa
                                            dipertanggungjawabkan
                                        </li>
                                        <li>
                                            <b>Muthawwif berpengalaman</b> Anda akan selalu dibimbing oleh muthawwif dari
                                            mahasiswa
                                            universitas
                                            terbaik di Arab Saudi/Jazirah Arab
                                        </li>
                                        <li>
                                            <b>Tenda Mina Dekat</b> Jejak Imani menggunakan fasilitas VIP selama perjalanan
                                            haji,
                                            salah satunya
                                            tenda di Mina yang dekat dengan Jamarat antara Maktab 111-115
                                        </li>
                                        <li>
                                            <b>Maskapai Plat Merah</b> Demi kenyamanan dan kepastian keberangkatan, Jejak
                                            Imani
                                            memilih untuk
                                            menggunakan maskapai Garuda Indonesia atau Saudi Airlines, direct tanpa transit
                                        </li>
                                        <li>
                                            <b>Bus Eksekutif</b> Selama perjalanan di Tanah Suci, Anda akan berpindah tempat
                                            menggunakan bus AC
                                            eksklusif keluaran terbaru
                                        </li>
                                        <li>
                                            <b>Radiophone</b> Saat bimbingan thawaf, sa’i dan prosesi ritual haji, Anda akan
                                            selalu
                                            dibimbing
                                            oleh tim khidmat kami melalui radiophone
                                        </li>
                                    </ul>
                                    <p>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                    <h5>
                                        Apakah Haji Plus/Haji Khusus bisa dilakukan tanpa menunggu antrian seperti Haji
                                        Reguler?
                                    </h5>
                                    <br>
                                    <p>
                                        Haji Plus/Haji Khusus bisa dilakukan tanpa antri yang dinamakan Haji Furoda/Haji
                                        Mandiri.
                                        Kuotanya terbatas. Silakan klik <a class="text-warning text-decoration-none"
                                            href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.">link
                                            ini</a> untuk chat dengan tim sales Jejak Imani.
                                        <br><br>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                    <h5>
                                        Berapa biaya Badal Haji dan apa saja persyaratannya?
                                    </h5>
                                    <br>
                                    <p>
                                        Biaya Badal Haji antara $1.000-$1.500 tergantung saat tahun haji dilaksanakan.
                                        Persyaratan:
                                        foto KK terakhir untuk keterangan nama dan nama ayah, surat keterangan wafat atau
                                        sakit
                                        berat/tidak mampu secara fisik melaksanakan haji di masa depan.
                                        <br><br>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                    <h5>
                                        Berapa lama waktu yang dibutuhkan untuk mendapatkan nomor porsi Haji Plus/Haji
                                        Khusus
                                        setelah mendaftar?
                                    </h5>
                                    <br>
                                    <p>
                                        Insya Allah calon jamaah haji sudah mendapatkan nomor porsi Haji Plus/Haji Khusus
                                        paling
                                        lama 2 pekan setelah DP diterima oleh Jejak Imani.
                                        <br><br>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                    <h5>
                                        Jika jamaah sudah daftar langsung ke Kemenag dan sudah mendapatkan nomor porsi Haji
                                        Reguler
                                        tetapi ingin pindah ke Haji Plus/Haji Khusus supaya bisa berangkat haji lebih cepat,
                                        bagaimanakah prosedur dan biayanya?
                                    </h5>
                                    <br>
                                    <p>
                                        Calon jamaah haji bisa langsung mendaftar nomor porsi Haji Plus/Haji Khusus meski
                                        sudah
                                        terdaftar nomor Haji Reguler. Nomor porsi Haji Reguler dan Haji Plus/Haji Khusus
                                        berbeda
                                        sehingga tidak bisa dipindahkan, hanya bisa mendaftar nomor porsi baru Haji
                                        Plus/Haji
                                        Khusus. Silakan klik link ini untuk konsultasi dengan tim sales Jejak Imani.
                                        <br><br>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                    <h5>
                                        Mengapa Haji Plus/Haji Khusus lewat travel jauh lebih mahal dibandingkan Haji
                                        Reguler yang
                                        langsung daftar dari Kemenag?
                                    </h5>
                                    <br>
                                    <p>
                                        Haji Plus/Haji Khusus memiliki waktu tunggu yang lebih pendek dengan fasilitas ⭐️ 5
                                        hotel
                                        yang lebih dekat dengan Masjidil Haram dan Masjid Nabawi.
                                        <br><br>
                                        Apakah jawaban ini membantu Anda? <a href=""
                                            class="text-warning text-decoration-none">Ya</a>
                                    </p>
                                    <hr class="mt-4" style="height: 2px">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="form">
        <div class="container mb-5">
            <div class="row d-flex justify-content-evenly">
                <div class="col-12 col-md-5 rounded bg-light p-4">
                    <h1 class="mb-3">Video</h1>

                    <!-- Button trigger modal -->
                    <button type="button" class="border border-light" data-bs-toggle="modal"
                        data-bs-target="#exampleModal">
                        <img src="{{ asset('images/haji-furoda/0.jpg') }}" class="d-block w-100" alt="" srcset="">
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <iframe src="https://www.youtube.com/embed/QtT8swjxvSo" title="YouTube video player"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 rounded bg-light p-4">
                    <form method="POST">
                        @csrf
                        @method('POST')
                        <div class="mb-4">
                            <p>
                                Jika anda ingin mengetahui lebih lanjut mengenai informasi Haji Plus/Khusus Bersama Jejak
                                Imani, silakan isi form dibawah ini dan klik tombol Submit. Kami akan menghubungi Anda.
                            </p>
                        </div>
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">No. Handphone/WA <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="no_hp" name="no_hp">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Alamat Email <span
                                    class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="message" class="form-label">Pesan <span
                                    class="text-danger">*</span></label>
                            <textarea class="form-control" name="message" id="message" cols="30" rows="1"></textarea>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-warning text-light">SUBMIT <i
                                    class="fas fa-arrow-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    @include('components.homepage.paket-lainnya')
@endsection
