@extends('layouts.homepage.master')

@section('title')
    Tentang Kami
@endsection

@section('css')
    <style>
        #hero {
            background: #0B0B0B;
            color: white;
            height: 90vh;
            font-family: 'gotham-black';
        }

        #hero h1 {
            font-size: 100px !important;
            font-weight: bold;
        }

        h1 {
            font-family: 'gotham-black';
        }

        h4 {
            font-family: 'gotham-black';
        }

        h5 {
            font-family: 'gotham-black';
        }

        p {
            font-family: 'gotham';
        }

        #visi-misi {
            margin-top: 300px;
            margin-bottom: 80px;
        }

        #visi-misi h1 {
            font-size: 40px !important;
            font-weight: bold;
            color: black;
        }

        #excecutive {
            background: #0B0B0B;
            color: white;
        }

        #excecutive h1 {
            font-size: 70px !important;
            font-weight: bold;
        }

        #excecutive hr {
            height: 5px;
            color: #EEB22C;
            opacity: 10;
        }

        #struktur img {
            width: 80%;
        }

        #struktur h1 {
            font-size: 60px !important;
            font-weight: bold;
            color: black;
        }

        #struktur .hr {
            border: #EEB22C;
            border-style: none none solid none;
        }

        #sambutan {
            background: #0B0B0B;
            color: white;
        }

        #sambutan h1 {
            font-size: 60px !important;
            font-weight: bold;
        }

        #sambutan hr {
            height: 5px;
            color: #EEB22C;
            opacity: 10;
        }

        #legal-formal {
            color: black;
        }

        #legal-formal h1 {
            font-size: 70px;
            font-weight: bold;
        }

        #legal-formal hr {
            height: 5px;
            color: #EEB22C;
            opacity: 10;
        }

        #legal-formal img {
            width: 80%;
            margin-top: 100px;
        }

        @media (max-width: 430px) {
            #hero h1 {
                font-size: 23px !important;
            }

            #history h1 {
                font-size: 13pt;
            }

            #visi-misi {
                margin-top: 0;
            }

            #visi-misi h1 {
                font-size: 12px !important;
                text-align: center;
            }

            #visi-misi p {
                font-size: 10pt;
            }

            #excecutive h5,
            #excecutive h4,
            #excecutive p {
                font-size: 10pt;
            }

            #excecutive h1 {
                font-size: 36px !important;
            }

            #struktur h1 {
                font-size: 36px !important;
            }

            #sambutan h1 {
                font-size: 36px !important;
            }

            #legal-formal h1 {
                font-size: 36px;
            }


            #legal-formal img {
                width: 100%;
                margin-top: 0;
            }
        }

    </style>
@endsection

@section('content')
    <section id="hero" class="d-flex align-items-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <h1>Tentang Kami</h1>
                </div>
            </div>
        </div>
    </section>

    <section id="history">
        <div class="container my-5 px-4 px-md-0">
            <div class="row d-flex align-items-center">
                <div class="col-12 col-md-6">
                    <h1>Sejarah Jejak Imani</h1>
                    <hr>
                    {!! $sejarah->text !!}
                </div>
                <div class="col-12 col-md-6">
                    <img class="img-fluid" src="{{ asset($sejarah->image) }}" alt="" srcset="">
                </div>
            </div>
        </div>
    </section>

    <section id="visi-misi">
        <div class="container px-4 px-md-0">
            <div class="row">
                <div class="col-12 col-md-5">
                    <h1 class="text-center">
                        VISI
                    </h1>
                </div>
                <div class="col-12 col-md-7">
                    <h1>
                        {{ $visi->json }}
                    </h1>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12 col-md-5">
                    <h1 class="text-center">
                        MISI
                    </h1>
                </div>
                <div class="col-12 col-md-7">
                    {!! $misi->json !!}
                </div>
            </div>
        </div>
    </section>

    <section id="excecutive">
        <div class="container-fluid">
            <div class="row p-5 d-flex justify-content-between">
                <div class="col-12 col-md-4">
                    <h1>Eksekutif <br> Perusahaan</h1>
                </div>
                <div class="col-12 col-md-8 ps-md-5 pt-md-5">
                    <hr class="mt-md-5">
                </div>
            </div>

            <div class="row">
                <div class="col-12 p-0">
                    <img class="img-fluid" src="{{ asset('images/exc.png') }}" alt="" srcset="">
                </div>
            </div>
        </div>

        <div class="container mt-4">
            <div class="row">
                @foreach ($eksekutif as $item)
                    @php
                        $json = json_decode($item->json);
                    @endphp
                    <div class="col-11 ps-4 ps-md-0 col-md-3">
                        <h5 class="fw-bold">
                            {{ $json->jabatan }}
                        </h5>
                        <h4 class="fw-bold">
                            {{ $json->name }}
                        </h4>
                        <p class="fw-bold">
                            {{ $json->text }}
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section id="struktur">
        <div class="container-fluid py-5">
            <div class="row">
                <div class="col-6">
                    <h1>
                        Struktur Organisasi
                    </h1>
                </div>
                <div class="col-6 hr"></div>
            </div>

            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <img src="{{ asset($sambutan->image) }}" alt="" srcset="">
                </div>
            </div>
        </div>
    </section>

    <div id="sambutan">
        <div class="container py-5 px-4 px-md-0">
            <div class="row d-flex align-items-center mb-5">
                <div class="col-12 col-md-6">
                    <h1>
                        Sambutan <br> Presiden <br> Direktur
                    </h1>
                </div>
                <div class="col-12 col-md-6">
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <img class="img-fluid" src="{{ asset('images/2_IMG.jpg') }}" alt="" srcset="">
                </div>
                <div class="col-12 col-md-6 pt-3 pt-md-0">
                    {!! $sambutan->text !!}
                </div>
            </div>
        </div>
    </div>

    <div id="legal-formal">
        <div class="container-fluid py-5 px-4 px-md-0">
            <div class="row d-flex align-items-center mb-5">
                <div class="col-12 col-md-6">
                    <h1>
                        Legal Formal Perusahaan
                    </h1>
                </div>
                <div class="col-12 col-md-6">
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <img class="img-fluid" src="{{ asset('images/Tentang-Kami.jpg') }}" alt="" srcset="">
                </div>
            </div>
        </div>
    </div>
@endsection
