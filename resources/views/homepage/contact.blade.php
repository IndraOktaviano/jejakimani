@extends('layouts.homepage.master')

@section('title')
    Hubungi Kami
@endsection

@section('css')
    <style>
        .hero-img {
            height: 500px;
            background: linear-gradient(rgba(0, 0, 0, 0.788), rgba(0, 0, 0, 0.788)), url("{{ asset('images/download.jpg') }}");
            background-position: center;
        }

        .hero-img h1 {
            color: white;
        }

        .hero-img p {
            color: white;
        }

        h1 {
            font-family: 'gotham-black';
            font-weight: bold;
        }

        h6 {
            font-family: 'gotham-black';
            font-weight: bold;
        }

        p {
            font-family: 'gotham';
        }

    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row d-flex align-items-center">
            <div class="col-12 col-md-6 hero-img d-flex align-items-center">
                <div class="col-12 p-5">
                    <h1>
                        Konsultasi Umrah? <br>
                        Kontak Kami Sekarang Juga!
                    </h1>
                    <p>
                        Umrah Lebih Nyaman | Landing Madinah | Hotel Lebih Dekat | Tersedia Tabungan Umrah | Menggunakan
                        Radiophone
                    </p>
                </div>
            </div>

            <div class="col-12 col-md-6 p-5">
                <h6 class="m-0">
                    Kontak Jejak Imani
                </h6>
                <p class="m-0">
                    {{$description->json}}
                </p>
                <a href="#" class="text-decoration-none">
                    {{$sosial_media['email']->json}}
                </a>
                <br>
                <br>
                <p class="fw-bold m-0">
                    Head Office Jejak Imani
                </p>
                <p class="m-0">
                    {{$head_office->json}}
                </p>
                <br>
                @foreach ($cabang as $item)
                @php
                    $json = json_decode($item->json)
                @endphp
                <p class="fw-bold m-0">
                    Cabang {{$json->name}}
                </p>
                <p class="m-0">
                    {{$json->text}}
                </p>
                <br>
                @endforeach
            </div>
        </div>
    </div>
@endsection
