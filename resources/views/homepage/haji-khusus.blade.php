@extends('layouts.homepage.master')

@section('title')
    Daftar Haji Plus, Keberangkatan dengan Masa Tunggu yang Lebih Singkat
@endsection

@section('css')
    <style>
        #app {
            background-color: #ECEDF0;
        }

        #hero .carousel-item {
            height: 550px;
            background: linear-gradient(0deg, rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.747))
        }

        #hero .carousel-item img {
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0.2;
            /* min-height: 500px; */
        }

        #hero .carousel-caption {
            top: 20%;
            bottom: auto;
            text-align: start;
        }

        #hero .carousel-caption h1 {
            font-size: 55pt;
        }

        #description .package {
            position: relative;
            background: linear-gradient(0deg, rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.747)),
                url("{{ asset('images/haji/image-1.jpeg') }}");
            height: 350px;
            width: 90%;
            border-radius: 20px;
            color: white;
        }

        #description .package .package-bottom {
            position: absolute;
            bottom: 0;
            left: 5;
        }

        #description .btn {
            border-radius: 25px;
        }

        #advantage img {
            border-style: solid;
        }

        #advantage .card {
            border-color: transparent !important;
            background-color: inherit !important;
            max-width: 540px;
        }

        #advantage .card p {
            font-size: 11pt;
        }

        #tentatif .card {
            border-radius: 10px;
        }

        #akomodasi .card,
        #akomodasi .card img {
            border-radius: 15px;
        }

        #akomodasi .card .card-body p {
            font-size: 10pt;
        }

        #accordion {
            background-color: #ECEDF0
        }

        #accordion .accordion-item {
            background-color: #567C49;
            border-radius: 25px;
        }

        #accordion .accordion-button {
            color: white;
            background-color: #163B15;
            border-radius: 25px;
            font-family: 'gotham-black';
            font-weight: bold;
            font-size: 20px;
        }

        #accordion .accordion-button:not(.collapsed) {
            color: white;
            background-color: #163B15;
        }

        #accordion .accordion-collapse {
            color: white;
            background-color: #567C49;
            border-bottom-left-radius: 25px;
            border-bottom-right-radius: 25px;
            font-family: 'gotham';
        }

        #form {
            background-color: #ECEDF0
        }

        #form .form-label {
            font-family: 'gotham';
            font-weight: bold;
        }

        #form .form-control {
            border-radius: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-color: black;
            background-color: transparent;
            box-shadow: none;
        }

        #form .rounded {
            border-radius: 15px !important;
        }

        @media (max-width: 430px) {
            #carouselHero .carousel-caption {
                top: 5%;
            }

            #carouselHero .carousel-item h1 {
                font-size: 30pt;
            }

            #carouselHero .carousel-item p {
                font-size: 9pt;
            }

            #carouselHero .carousel-item img {
                height: 100%;
                width: auto !important;
            }

            #carouselHero .carousel-caption .btn {
                font-size: 10pt;
            }

            #description .package {
                height: 457px;
                width: 100%;
            }

            #advantage h5 {
                font-size: 12pt;
            }

            #advantage .card p {
                font-size: 10pt;
            }

            #flow-flight .w-50 {
                width: 100% !important;
            }

            #flow-flight p {
                font-size: 10pt;
            }
        }

        @media (max-width: 767px) {
            #akomodasi .carousel-inner .carousel-item>div {
                display: none;
            }

            #akomodasi .carousel-inner .carousel-item>div:first-child {
                display: block;
            }
        }

        #akomodasi .carousel-inner .carousel-item.active,
        #akomodasi .carousel-inner .carousel-item-next,
        #akomodasi .carousel-inner .carousel-item-prev {
            display: flex;
        }

        /* medium and up screens */
        @media (min-width: 768px) {

            #akomodasi .carousel-inner .carousel-item-end.active,
            #akomodasi .carousel-inner .carousel-item-next {
                transform: translateX(25%);
            }

            #akomodasi .carousel-inner .carousel-item-start.active,
            #akomodasi .carousel-inner .carousel-item-prev {
                transform: translateX(-25%);
            }
        }

        #akomodasi .carousel-inner .carousel-item-end,
        #akomodasi .carousel-inner .carousel-item-start {
            transform: translateX(0);
        }

        @media (max-width: 767px) {
            #tentatif .carousel-inner .carousel-item>div {
                display: none;
            }

            #tentatif .carousel-inner .carousel-item>div:first-child {
                display: block;
            }
        }

        #tentatif .carousel-inner .carousel-item.active,
        #tentatif .carousel-inner .carousel-item-next,
        #tentatif .carousel-inner .carousel-item-prev {
            display: flex;
        }

        /* medium and up screens */
        @media (min-width: 768px) {

            #tentatif .carousel-inner .carousel-item-end.active,
            #tentatif .carousel-inner .carousel-item-next {
                transform: translateX(25%);
            }

            #tentatif .carousel-inner .carousel-item-start.active,
            #tentatif .carousel-inner .carousel-item-prev {
                transform: translateX(-25%);
            }
        }

        #tentatif .carousel-inner .carousel-item-end,
        #tentatif .carousel-inner .carousel-item-start {
            transform: translateX(0);
        }

    </style>
@endsection

@section('js')
    <script>
        $('.close-div').on('click', function() {
            $(this).closest("#close-wrapper").remove();
        });
    </script>

    <script>
        let itemsAkomodasi = document.querySelectorAll('.carousel-akomodasi .carousel-item-akomodasi')

        itemsAkomodasi.forEach((el) => {
            const minPerSlide = 3
            let next = el.nextElementSibling
            for (var i = 1; i < minPerSlide; i++) {
                if (!next) {
                    // wrap carousel by using first child
                    next = itemsAkomodasi[0]
                }
                let cloneChild = next.cloneNode(true)
                el.appendChild(cloneChild.children[0])
                next = next.nextElementSibling
            }
        })
    </script>

    <script>
        let itemsTentatif = document.querySelectorAll('.carousel-tentatif .carousel-item-tentatif')

        itemsTentatif.forEach((el) => {
            const minPerSlide = 3
            let next = el.nextElementSibling
            for (var i = 1; i < minPerSlide; i++) {
                if (!next) {
                    // wrap carousel by using first child
                    next = itemsTentatif[0]
                }
                let cloneChild = next.cloneNode(true)
                el.appendChild(cloneChild.children[0])
                next = next.nextElementSibling
            }
        })
    </script>
@endsection

@section('content')
    <section id="hero">
        <div class="container-fluid p-0">
            <div id="carouselHero" class="carousel slide carousel-fade" data-bs-ride="carousel">
                <div class="carousel-inner">
                    @php
                        $i = 0;
                    @endphp
                    @foreach ($banner as $item)
                        <div class="carousel-item d-flex  {{ !$i++ ? 'active' : '' }}">
                            <img src="{{ asset($item->json) }}" class="d-block w-100" alt="{{ asset($item->json) }}">
                            <div class="carousel-caption d-md-block">
                                {!! $label_banner->name !!}
                                {!! $label_banner->text !!}
                                <br>
                                <br>
                                <a target="_blank"
                                    href="https://api.whatsapp.com/send?phone={{ $label_banner->phone }}&text{{ $label_banner->text_wa }}"
                                    @if ($label_banner->event) onclick="gtag('event', {{ $label_banner->event }}, { 'event_category': {{ $label_banner->event_category ?? '' }}, 'event_label': {{ $label_banner->event_label ?? '' }}});" @endif
                                    class="btn btn-warning text-light btn-shake">{{ $label_banner->label_wa }} <i
                                        class="fas fa-arrow-right"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section id="description" class="mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 d-flex align-items-center">
                    <div class="text-center">
                        {!! $section_1->text !!}
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <img src="{{ asset($section_1->image) }}" class="rounded img-fluid">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 col-md-6 d-flex justify-content-center">
                    <div class="package p-3">
                        {!! $section_2->name !!}
                        <div class="package-bottom">
                            <h5>
                                {!! $section_2->label_price !!}
                            </h5>
                            <a target="_blank"
                                href="{https://api.whatsapp.com/send?phone={{ $section_2->phone }}&text{{ $section_2->text_wa }}}"
                                @if ($section_2->event) onclick="gtag('event', {{ $section_2->event }}, { 'event_category': {{ $section_2->event_category ?? '' }}, 'event_label': {{ $section_2->event_label ?? '' }}});" @endif
                                class="btn btn-warning mb-3 mt-3 text-light btn-shake">
                                {{ $section_2->label_wa }} <i class="fas fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-3 mt-md-0 col-md-6">
                    {!! $section_2->text !!}
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 col-md-6">
                    <h1 class="fw-bold grey">{{ $section_3->name }}</h1>
                    {!! $section_3->text !!}
                </div>
                <div class="col-12 col-md-6 d-flex justify-content-center">
                    <div class="package p-3">
                        <h1>
                            <b>{{ $section_4->name }}</b>
                        </h1>
                        <div class="package-bottom">
                            {!! $section_4->text !!}
                            <a target="_blank" href="{{ route('haji-furoda.index') }}"
                                @if ($section_4->event) onclick="gtag('event', {{ $section_4->event }}, { 'event_category': {{ $section_4->event_category ?? '' }}, 'event_label': {{ $section_4->event_label ?? '' }}});" @endif
                                class="btn btn-warning mb-3 mt-3 text-light btn-shake">
                                {{ $section_4->label_wa }} <i class="fas fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="advantage">
        <div class="container mt-5">
            <div class="row">
                @foreach ($section_list_1 as $item)
                    @php
                        $json = json_decode($item->json);
                    @endphp
                    <div class="col-6">
                        <div class="card mb-3">
                            <div class="row g-0 d-flex align-items-top">
                                <div class="col-md-6">
                                    <img src="{{ asset($json->image) }}" class="img-fluid rounded"
                                        alt="{{ asset($json->image) }}">
                                </div>
                                <div class="col-md-6">
                                    <div class="card-body pt-0">
                                        <h5 class="card-title mt-2 mt-md-0">{{ $json->name }}</h5>
                                        <p class="card-text">{!! $json->text !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section id="flow-flight">
        <div class="container-fluid bg-grey text-light p-5">
            <div class="row d-flex align-items-center">
                <div class="col-12 col-md-6">
                    <h5>Grafis Perjalanan Haji</h5>
                    <div class="d-none d-md-block">
                        <p>Program Haji Plus/Haji Khusus Jejak Imani selama 26 hari yaitu mulai 04 – 29 Dzulhijjah</p>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-warning mb-3 btn-rounded-new" data-bs-toggle="modal"
                            data-bs-target="#grafisModal">
                            LIHAT PERJALANAN HAJI <i class="fas fa-arrow-right"></i>
                        </button>
                        <br>
                        <a href="{{ asset('images/haji khusus/grafis_haji-compressed.pdf') }}" target="_blank"
                            download="Grafis Haji Jejak Imani" class="btn btn-warning btn-rounded-new">UNDUH PERJALANAN HAJI
                            <i class="fas fa-arrow-right"></i>
                        </a>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="grafisModal" tabindex="-1" aria-labelledby="grafisModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="btn-close sticky-top" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                    <img class="w-100" src="{{ asset('images/haji khusus/CTA-Image.png') }}"
                                        alt="{{ asset('images/haji khusus/CTA-Image.png') }}"
                                        srcset="{{ asset('images/haji khusus/CTA-Image.png') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 d-flex justify-content-center">
                    <img class="d-block w-50" src="{{ asset('images/haji khusus/CTA-Image-500x500.png') }}"
                        alt="{{ asset('images/haji khusus/CTA-Image-500x500.png') }}"
                        srcset="{{ asset('images/haji khusus/CTA-Image-500x500.png') }}">
                </div>

                <div class="col-12 mt-3 d-md-none d-block">
                    <p>Program Haji Plus/Haji Khusus Jejak Imani selama 26 hari yaitu mulai 04 – 29 Dzulhijjah</p>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-warning mb-3 btn-rounded-new" data-bs-toggle="modal"
                        data-bs-target="#grafisModal">
                        LIHAT PERJALANAN HAJI <i class="fas fa-arrow-right"></i>
                    </button>
                    <br>
                    <a href="{{ asset('images/haji khusus/grafis_haji-compressed.pdf') }}" target="_blank"
                        download="Grafis Haji Jejak Imani" class="btn btn-warning btn-rounded-new">UNDUH PERJALANAN HAJI
                        <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="tentatif">
        <div class="container mt-5">
            <h4 class="mb-4">Program Perjalanan Haji Plus/Haji Khusus (Tentatif)</h4>
            <div class="row">
                <div id="tentatifControl" class="carousel carousel-tentatif slide" data-bs-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        @foreach ($section_list_2 as $key => $item)
                            <div class="carousel-item carousel-item-tentatif px-5 {{ !$key++ ? 'active' : '' }}">
                                <div class="card mx-md-2 bg-green text-light">
                                    @php
                                        $json = json_decode($item->json);
                                    @endphp
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $json->name }}</h5>
                                        <ul class="card-text">
                                            {!! $json->text !!}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev bg-transparent w-aut" href="#tentatifControl" role="button"
                        data-bs-slide="prev">
                        <i class="fas fa-chevron-circle-left fa-2x orange"></i>
                    </a>
                    <a class="carousel-control-next bg-transparent w-aut" href="#tentatifControl" role="button"
                        data-bs-slide="next">
                        <i class="fas fa-chevron-circle-right fa-2x orange"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="akomodasi">
        <div class="container mt-5">
            <h4 class="mb-4">Akomodasi</h4>
            <div class="row">
                <div id="akomodasiControl" class="carousel carousel-akomodasi slide" data-bs-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        @foreach ($section_list_3 as $key => $item)
                            <div class="carousel-item carousel-item-akomodasi {{ !$key++ ? 'active' : '' }}">
                                @php
                                    $json = json_decode($item->json);
                                @endphp
                                <div class="card p-2 mx-md-2 bg-green text-light">
                                    <img src="{{ $json->image }}" class="card-img-top" alt="{{ $json->image }}">
                                    <div class="card-body p-0 mt-2">
                                        <h5>{{ $json->name }}</h5>
                                        {!! $json->text !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev bg-transparent w-aut" href="#akomodasiControl" role="button"
                        data-bs-slide="prev">
                        <i class="fas fa-chevron-circle-left fa-2x orange"></i>
                    </a>
                    <a class="carousel-control-next bg-transparent w-aut" href="#akomodasiControl" role="button"
                        data-bs-slide="next">
                        <i class="fas fa-chevron-circle-right fa-2x orange"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="accordion">
        <div class="container mt-5">
            <div class="accordion" id="accordionExample">
                @foreach ($section_list_4 as $item)
                    @php
                        $json = json_decode($item->json);
                    @endphp
                    <div class="accordion-item mb-4">
                        <h2 class="accordion-header" id="{{ 'headingOne' . $item->id }}">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#{{ 'collapseOne' . $item->id }}" aria-expanded="true"
                                aria-controls="{{ 'collapseOne' . $item->id }}">
                                {{ $json->name }}
                            </button>
                        </h2>
                        <div id="{{ 'collapseOne' . $item->id }}" class="accordion-collapse collapse"
                            aria-labelledby="{{ 'headingOne' . $item->id }}" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                {!! $json->text !!}
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="accordion-item mb-4">
                    <h2 class="accordion-header" id="headingSeven">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            FAQ
                        </button>
                    </h2>
                    <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <h5>
                                Apa bedanya Haji Plus/Haji Khusus dengan Haji Reguler langsung dari Kemenag?
                            </h5>
                            <br>
                            <p>
                                Haji reguler dikelola oleh Kementerian Agama dan KBIH dengan waktu tunggu 20-35 tahun
                                tergantung
                                kota, fasilitas ⭐️ 3 & 4. Haji Plus/Haji Khusus dikelola oleh swasta dengan waktu tunggu 5-8
                                tahun, fasilitas ⭐️ 5, hotel yang digunakan didepan Masjid Nabawi & Masjidil Haram.
                                <br><br>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                            <h5>
                                Adakah flyer informasi Badal Haji?
                            </h5>
                            <br>
                            <p>
                                Silakan klik <a class="text-warning text-decoration-none"
                                    href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.">link
                                    ini</a>
                                untuk informasi detail Badal Haji.

                                <br><br>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                            <h5>
                                Apa bisa jika suami/istri atau keluarga yang pendaftaran Haji Plus/Haji Khususnya berbeda
                                tahun ingin berangkat di tahun yang sama?
                            </h5>
                            <br>
                            <p>
                                Apabila selisih pendaftarannya tidak lebih dari 2 tahun, maka masih bisa diupayakan. Bisa
                                atau tidaknya juga tergantung dengan aturan terbaru Kementerian Agama.

                                <br><br>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                            <h5>
                                Apa kelebihan dan keunggulan Haji Plus/Haji Khusus bersama Jejak Imani?
                            </h5>
                            <br>
                            <ul>
                                <li>
                                    <b>Terakreditasi A</b> Jejak Imani dengan standar pelayanan yang sangat baik, telah
                                    terakreditasi A oleh Kementerian Agama RI
                                </li>
                                <li>
                                    <b>Profesional</b> Tim Khidmat Jejak Imani berpengalaman melayani tamu-tamu Allah dengan
                                    sepenuh
                                    hati
                                </li>
                                <li>
                                    <b>Bimbingan Intensif Pembimbing</b> Jejak Imani adalah ustadz dengan background
                                    keilmuan yang bisa
                                    dipertanggungjawabkan
                                </li>
                                <li>
                                    <b>Muthawwif berpengalaman</b> Anda akan selalu dibimbing oleh muthawwif dari mahasiswa
                                    universitas
                                    terbaik di Arab Saudi/Jazirah Arab
                                </li>
                                <li>
                                    <b>Tenda Mina Dekat</b> Jejak Imani menggunakan fasilitas VIP selama perjalanan haji,
                                    salah satunya
                                    tenda di Mina yang dekat dengan Jamarat antara Maktab 111-115
                                </li>
                                <li>
                                    <b>Maskapai Plat Merah</b> Demi kenyamanan dan kepastian keberangkatan, Jejak Imani
                                    memilih untuk
                                    menggunakan maskapai Garuda Indonesia atau Saudi Airlines, direct tanpa transit
                                </li>
                                <li>
                                    <b>Bus Eksekutif</b> Selama perjalanan di Tanah Suci, Anda akan berpindah tempat
                                    menggunakan bus AC
                                    eksklusif keluaran terbaru
                                </li>
                                <li>
                                    <b>Radiophone</b> Saat bimbingan thawaf, sa’i dan prosesi ritual haji, Anda akan selalu
                                    dibimbing
                                    oleh tim khidmat kami melalui radiophone
                                </li>
                            </ul>
                            <p>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                            <h5>
                                Apakah Haji Plus/Haji Khusus bisa dilakukan tanpa menunggu antrian seperti Haji Reguler?
                            </h5>
                            <br>
                            <p>
                                Haji Plus/Haji Khusus bisa dilakukan tanpa antri yang dinamakan Haji Furoda/Haji Mandiri.
                                Kuotanya terbatas. Silakan klik <a class="text-warning text-decoration-none"
                                    href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Haji%20Khusus%20Program%20Arbain%20di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website.">link
                                    ini</a> untuk chat dengan tim sales Jejak Imani.
                                <br><br>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                            <h5>
                                Berapa biaya Badal Haji dan apa saja persyaratannya?
                            </h5>
                            <br>
                            <p>
                                Biaya Badal Haji antara $1.000-$1.500 tergantung saat tahun haji dilaksanakan. Persyaratan:
                                foto KK terakhir untuk keterangan nama dan nama ayah, surat keterangan wafat atau sakit
                                berat/tidak mampu secara fisik melaksanakan haji di masa depan.
                                <br><br>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                            <h5>
                                Berapa lama waktu yang dibutuhkan untuk mendapatkan nomor porsi Haji Plus/Haji Khusus
                                setelah mendaftar?
                            </h5>
                            <br>
                            <p>
                                Insya Allah calon jamaah haji sudah mendapatkan nomor porsi Haji Plus/Haji Khusus paling
                                lama 2 pekan setelah DP diterima oleh Jejak Imani.
                                <br><br>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                            <h5>
                                Jika jamaah sudah daftar langsung ke Kemenag dan sudah mendapatkan nomor porsi Haji Reguler
                                tetapi ingin pindah ke Haji Plus/Haji Khusus supaya bisa berangkat haji lebih cepat,
                                bagaimanakah prosedur dan biayanya?
                            </h5>
                            <br>
                            <p>
                                Calon jamaah haji bisa langsung mendaftar nomor porsi Haji Plus/Haji Khusus meski sudah
                                terdaftar nomor Haji Reguler. Nomor porsi Haji Reguler dan Haji Plus/Haji Khusus berbeda
                                sehingga tidak bisa dipindahkan, hanya bisa mendaftar nomor porsi baru Haji Plus/Haji
                                Khusus. Silakan klik link ini untuk konsultasi dengan tim sales Jejak Imani.
                                <br><br>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                            <h5>
                                Mengapa Haji Plus/Haji Khusus lewat travel jauh lebih mahal dibandingkan Haji Reguler yang
                                langsung daftar dari Kemenag?
                            </h5>
                            <br>
                            <p>
                                Haji Plus/Haji Khusus memiliki waktu tunggu yang lebih pendek dengan fasilitas ⭐️ 5 hotel
                                yang lebih dekat dengan Masjidil Haram dan Masjid Nabawi.
                                <br><br>
                            <div id="close-wrapper">
                                Apakah jawaban ini membantu Anda? <span class="orange close-div">Ya</span>
                            </div>
                            </p>
                            <hr class="mt-4" style="height: 2px">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="form">
        <div class="container my-5">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-7 bg-light rounded p-4">
                    <form method="POST">
                        @csrf
                        @method('POST')
                        <div class="mb-4">
                            <p>
                                Jika anda ingin mengetahui lebih lanjut mengenai informasi Haji Plus/Khusus Bersama Jejak
                                Imani, silakan isi form dibawah ini dan klik tombol Submit. Kami akan menghubungi Anda.
                            </p>
                        </div>
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">No. Handphone/WA <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="no_hp" name="no_hp">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Alamat Email <span
                                    class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="message" class="form-label">Pesan <span
                                    class="text-danger">*</span></label>
                            <textarea class="form-control" name="message" id="message" cols="30" rows="1"></textarea>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-warning text-light btn-shake">SUBMIT <i
                                    class="fas fa-arrow-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="description">
        <div class="container mb-5">
            <div class="row mt-5">
                <div class="col-12 col-md-6 d-flex justify-content-center">
                    <div class="package p-3">
                        <h1>
                            <b>HAJI KHUSUS</b>
                            <br>
                            ARBAIN
                        </h1>
                        <div class="package-bottom">
                            <h5>
                                <b>DP $5.000 / +- Rp 72.500.000,- <br> (Kurs Rp 14.500)</b> All in
                            </h5>
                            <a href="" class="btn btn-warning mb-3 mt-3 text-light btn-shake">
                                KLIK DISINI UNTUK KONSULTASI GRATIS <i class="fas fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 mt-3 mt-md-0">
                    <h6>HAJI bersama JEJAK IMANI</h6>
                    <br>
                    <p>
                        Mari Memenuhi Panggilan, Mabrur Bersama Kekasih Ar Rahmaan – Ustadz H. Salim A Fillah
                        <br><br>
                        عن عبدالله بن عباس: من أراد الحج فليتعجل فإنه قد يمرض المريض و تضل الضالة و تعرض الحاجة – صحيح إبن
                        ماجه
                        <br><br>
                        “Barangsiapa hendak melaksanakan ibadah Haji, hendaklah segera ia lakukan, karena terkadang
                        seseorang
                        itu
                        sakit, atau binatang (kendaraannya) hilang, dan adanya suatu hajat yang menghalanginya untuk
                        menunaikan
                        ibadah Haji.” (HR. Ibnu Majah)
                    </p>
                </div>
            </div>
        </div>
    </section>

    @include('components.homepage.paket-lainnya')
@endsection
