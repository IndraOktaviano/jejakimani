@extends('layouts.homepage.master')

@section('title')
    Tabungan Umroh Jakarta Tangerang Bogor Depok amanah Terpercaya
@endsection

@section('css')
    <style>
        #hero .img-1 {
            width: 80%;
        }

        #hero a {
            border-radius: 25px;
        }

        #keunggulan .gradient {
            background: linear-gradient(#020202, #6B7280);
            color: white;
            border-radius: 10px;
            height: 334px;
        }

        #keunggulan img {
            height: 147px;
        }

        #simulasi {
            background: #FFF2D7;
        }

        #simulasi .btn {
            border-radius: 25px;
        }

        #snk {
            background: #F3F4F6;
        }

        #snk .tab {
            overflow: hidden;
            /* border: 1px solid #ccc; */
            /* background-color: #f1f1f1; */
        }

        #snk .tab button {
            background-color: #12171F;
            float: left;
            border: none;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            outline: none;
            cursor: pointer;
            padding: 5px 16px;
            margin-right: 10px;
            transition: 0.3s;
            font-size: 17px;
            color: white;
        }

        #snk .tab button:hover {
            background-color: #E2A229;
        }

        #snk .tab button.active {
            background-color: #E2A229;
        }

        #snk .tabcontent {
            display: none;
            padding: 15px 12px;
            border: none;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
            background: white;
        }

        #form {
            background: url("{{ asset('images/tabungan-umrah/tabungan-umroh-form-bg.png') }}");
            background-position: center;
        }

        #form img {
            height: 450px;
            width: 100%;
            border-radius: 15px;
        }

        #form iframe {
            width: 100%;
            height: 500px;
        }

        #form .form-label {
            font-family: 'gotham';
            font-weight: bold;
        }

        #form .form-control {
            border-radius: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-color: black;
            background-color: transparent;
            box-shadow: none;
        }

        #form .rounded {
            border-radius: 15px !important;
        }

        #btn-wa {
            background: url("{{ asset('images/tabungan-umrah/bg-2.png') }}");
            background-size: cover;
            height: 329px;
            display: flex;
            align-items: center;
        }

        #btn-wa .col-md-3 {
            background: url("{{ asset('images/tabungan-umrah/wa-button-bg.png') }}") no-repeat, linear-gradient(to right, #246047, #398D63);
            /* background-size: cover; */
            /* background-repeat: no-repeat; */
            background-position: right;
            border-radius: 15px;
        }

        @media (max-width: 430px) {
            #hero a {
                font-size: 10pt;
            }

            #keunggulan .gradient {
                height: 100px !important;
            }

            #keunggulan img {
                height: 53px;
            }

            #keunggulan h2 {
                font-size: 10pt;
            }

            #snk .tab button {
                width: 100%;
                border-radius: 0;
            }

            #btn-wa .col-10 {
            background: url("{{ asset('images/tabungan-umrah/wa-button-bg.png') }}") no-repeat, linear-gradient(to right, #246047, #398D63);
            /* background-size: cover; */
            /* background-repeat: no-repeat; */
            background-position: right;
            border-radius: 15px;
        }
        }

    </style>
@endsection

@section('js')
    <script>
        function openTab(evt, tabName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>

    <script>
        let bulanan = document.querySelector('#bulanan');
        let mingguan = document.querySelector('#mingguan');
        let harian = document.querySelector('#harian');
        document.body.addEventListener('change', function(e) {
            let target = e.target;
            let message1;
            let message2;
            let message3;
            switch (target.id) {
                case 'paket1':
                    message1 = 'Rp 722.223';
                    message2 = 'Rp 166.667';
                    message3 = 'Rp 23.810';
                    break;
                case 'paket2':
                    message1 = 'Rp 1.083.334';
                    message2 = 'Rp 250.000';
                    message3 = 'Rp 35.715';
                    break;
                case 'paket3':
                    message1 = 'Rp 2.166.667';
                    message2 = 'Rp 500.000';
                    message3 = 'Rp 71.429';
                    break;
            }
            bulanan.textContent = message1;
            mingguan.textContent = message2;
            harian.textContent = message3;
        });
    </script>
@endsection

@section('content')
    <section id="hero">
        <div class="container-fluid bg-orange pt-5">
            <div class="row m-0 p-0">
                <div class="col-6 d-none d-md-block d-flex align-items-end justify-content-center">
                    <img class="img-fluid img-1"
                        src="{{ asset('images/umrah/tabungan-umrah/hero/img-hero-tabungan-haji.png') }}"
                        alt="{{ asset('images/umrah/tabungan-umrah/hero/img-hero-tabungan-haji.png') }}" srcset="">
                </div>
                <div class="col-12 col-md-6 text-center mb-3 mb-md-0">
                    <img class="img-fluid mb-3"
                        src="{{ asset('images/umrah/tabungan-umrah/hero/img-hero-tabungan-haji-title.png') }}"
                        alt="{{ asset('images/umrah/tabungan-umrah/hero/img-hero-tabungan-haji-title.png') }}" srcset="">
                    <a href="" class="btn btn-dark btn-shake">
                        KLIK DISINI UNTUK KONSULTASI GRATIS <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="keunggulan">
        <div class="container mt-5">
            <h1 class="fw-bold text-center mb-4">
                KEUNGGULAN TABUNGAN UMROH
            </h1>
            <div class="row">
                <div class="col-4">
                    <div class="gradient p-4">
                        <img src="{{ asset('images/umrah/tabungan-umrah/keunggulan/tabungan-haji-aman.png') }}"
                            alt="{{ asset('images/umrah/tabungan-umrah/keunggulan/tabungan-haji-aman.png') }}">
                        <h2 class="mt-3 d-none d-md-block">
                            Aman Penyimpanan Dananya
                        </h2>
                    </div>
                    <h2 class="mt-3 d-block d-md-none">
                        Aman Penyimpanan Dananya
                    </h2>
                    <p class="mt-3">
                        Dana jamaah tersimpan aman di rekening Bank Syariah Indonesia (BSI) atau Bank Muamalat (BMI) atas
                        nama jamaah, bukan di rekening travel.
                    </p>
                </div>
                <div class="col-4">
                    <div class="gradient p-4">
                        <img src="{{ asset('images/umrah/tabungan-umrah/keunggulan/tabungan-haji-transparan.png') }}"
                            alt="{{ asset('images/umrah/tabungan-umrah/keunggulan/tabungan-haji-transparan.png') }}">
                        <h2 class="mt-3 d-none d-md-block">
                            Transparan Pengelolaannya
                        </h2>
                    </div>
                    <h2 class="mt-3 d-block d-md-none">
                        Transparan Pengelolaannya
                    </h2>
                    <p class="mt-3">
                        Jamaah dapat secara langsung mengetahui besaran tabungan umrah yang sudah disetorkan setiap
                        bulannya.
                    </p>
                </div>
                <div class="col-4">
                    <div class="gradient p-4">
                        <img src="{{ asset('images/umrah/tabungan-umrah/keunggulan/tabungan-haji-pasti.png') }}"
                            alt="{{ asset('images/umrah/tabungan-umrah/keunggulan/tabungan-haji-pasti.png') }}">
                        <h2 class="mt-3 d-none d-md-block">
                            Insya Allah Pasti Berangkat Umrahnya
                        </h2>
                    </div>
                    <h2 class="mt-3 d-block d-md-none">
                        Insya Allah Pasti Berangkat Umrahnya
                    </h2>
                    <p class="mt-3">
                        Dengan dana jamaah dikelola langsung oleh Bank Syariah Indonesia (BSI) atau Bank Muamalat (BMI)
                        menjamin kepastian keberangkatan umrah para jamaah.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="simulasi">
        <div class="container py-5">
            <h1 class="mb-5 text-center">SIMULASI TABUNGAN UMROH</h1>

            <div class="row">
                <div class="col-12 col-md-6">
                    <h2>
                        Harga Paket : Rp 26.000.000
                    </h2>

                    <div class="form-check ps-5 mt-3">
                        <input class="form-check-input" type="radio" name="paket" id="paket1" checked>
                        <label class="form-check-label" for="paket1">
                            36 Bulan
                        </label>
                    </div>
                    <div class="form-check ps-5">
                        <input class="form-check-input" type="radio" name="paket" id="paket2">
                        <label class="form-check-label" for="paket2">
                            24 Bulan
                        </label>
                    </div>
                    <div class="form-check ps-5">
                        <input class="form-check-input" type="radio" name="paket" id="paket3">
                        <label class="form-check-label" for="paket3">
                            12 Bulan
                        </label>
                    </div>
                    <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20minta%20info%20prihal%20Tabungan%20umrah%20di%20Jejak%20Imani.%20Saya%20tau%20dari%20website."
                        target="_blank" class="btn btn-dark btn-shake mt-5">
                        TANYA DULU YUK <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
                <div class="col-12 mt-3 mt-md-0 col-md-6">
                    <div class="row justify-content-center">
                        <div class="col-11 col-md-7 bg-orange py-2">
                            <h5 class="text-center fw-normal m-0">
                                Besar setoran tabungan
                            </h5>
                        </div>
                    </div>
                    <div class="row mt-2 justify-content-center">
                        <div class="col-md-3 col-5 py-2 bg-orange">
                            <p class="m-0">Bulanan</p>
                        </div>
                        <div class="col-md-4 col-6 py-2 bg-grey text-light">
                            <p class="m-0 text-end" id="bulanan">Rp 722.223</p>
                        </div>
                    </div>
                    <div class="row mt-2 justify-content-center">
                        <div class="col-md-3 col-5 py-2 bg-orange">
                            <p class="m-0">Mingguan</p>
                        </div>
                        <div class="col-md-4 col-6 py-2 bg-grey text-light">
                            <p class="m-0 text-end" id="mingguan">Rp 166.667</p>
                        </div>
                    </div>
                    <div class="row mt-2 justify-content-center">
                        <div class="col-md-3 col-5 py-2 bg-orange">
                            <p class="m-0">Harian</p>
                        </div>
                        <div class="col-md-4 col-6 py-2 bg-grey text-light">
                            <p class="m-0 text-end" id="harian">Rp 23.810</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="snk">
        <div class="container py-5">
            <div class="tab">
                <button class="tablinks active" onclick="openTab(event, 'Proses')">Proses Pendaftaran <i
                        class="fas fa-arrow-right"></i></button>
                <button class="tablinks" onclick="openTab(event, 'Syarat')">Syarat Pendaftaran <i
                        class="fas fa-arrow-right"></i></button>
                <button class="tablinks" onclick="openTab(event, 'Ketentuan')">Ketentuan <i
                        class="fas fa-arrow-right"></i></button>
            </div>

            <div id="Proses" class="tabcontent shadow" style="display: block">
                <ol>
                    <li>
                        Mengisi formulir pendaftaran Tabungan Umroh Jejak Imani.
                    </li>
                    <li>
                        Setoran awal booking seat Rp 1.000.000 dibayar langsung ke Jejak Imani.
                    </li>
                    <li>
                        Jamaah melakukan pembukaan rekening tabungan umrah di Cabang Bank Syariah Indonesia (BSI) atau Bank
                        Muamalat (BMI) terdekat (Jejak Imani akan membantu jamaah berkoordinasi dengan Bank Syariah
                        Indonesia
                        (BSI) atau Bank Muamalat (BMI) yang dipilih).
                    </li>
                    <li>
                        Jamaah menyiapkan dana minimal Rp 200.000 untuk pembukaan rekening tabungan umrah di Cabang Bank
                        Syariah
                        Indonesia (BSI) atau Bank Muamalat (BMI) terdekat.
                    </li>
                    <li>
                        Jamaah melakukan setoran per bulan yang besarannya sesuai dengan kesanggupan jamaah seperti yang
                        tertulis dalam formulir pendaftaran Tabungan Umrah Jejak Imani.
                    </li>
                </ol>
            </div>

            <div id="Syarat" class="tabcontent shadow">
                <ol>
                    <li>
                        Mengisi formulir pendaftaran tabungan umrah.
                    </li>
                    <li>
                        Melampirkan fotokopi Kartu Tanda Penduduk (KTP).
                    </li>
                    <li>
                        Melampirkan fotokopi Kartu Keluarga (KK).
                    </li>
                    <li>
                        Melampirkan fotokopi Nomor Pajak Wajib Pajak (NPWP).
                    </li>
                    <li>
                        Jamaah yang belum mempunyai KTP berusia di bawah 17 tahun harus melampirkan surat pernyataan dari
                        orang
                        tua/wali.
                    </li>
                    <li>
                        Jamaah wajib memiliki email dan nomor handphone yang aktif.
                    </li>
                    <li>
                        Setoran awal booking seat minimal Rp 1.000.000/jamaah ke rekening Jejak Imani.
                    </li>
                </ol>
            </div>

            <div id="Ketentuan" class="tabcontent shadow">
                <ol>
                    <li>
                        Jamaah bersedia mengikuti semua aturan yang sudah ditetapkan Jejak Imani.
                    </li>
                    <li>
                        Persyaratan sudah harus diserahkan kepada Jejak Imani pada saat pendaftaran.
                    </li>
                    <li>
                        Setoran yang sudah dilakukan jamaah tidak bisa dicairkan dalam bentuk uang kecuali dialokasikan ke
                        jamaah lainnya dalam bentuk keberangkatan umrah.
                    </li>
                    <li>
                        Biaya keberangkatan umrah menyesuaikan dengan kurs pada saat keberangkatan.
                    </li>
                    <li>
                        Biaya paket umrah mengikuti estimasi harga pada saat keberangkatan jamaah yang ditentukan oleh Jejak
                        Imani.
                    </li>
                    <li>
                        Apabila pada saat jadwal keberangkatan yang sudah direncanakan tabungan umrah jamaah belum
                        mencukupi, maka jamaah melakukan pembayaran kekurangan biaya paket keberangkatan.
                    </li>
                    <li>
                        Apabila pada saat jadwal keberangkatan yang sudah direncanakan tabungan jamaah melebihi dari biaya
                        paket yang seharusnya dibayarkan, maka kelebihannya akan dikembalikan pada saat keberangkatan dalam
                        bentuk tunai atau dapat dipergunakan kembali untuk program tabungan umrah berikutnya.
                    </li>
                    <li>
                        Jika pada saat jadwal keberangkatan jamaah tidak bisa berangkat disebabkan oleh suatu hal, maka
                        tabungan umrah ini bisa diwariskan kepada ahli waris jamaah atau kepada orang lain dengan
                        persetujuan jamaah atau ahli waris jamaah.
                    </li>
                </ol>
            </div>
        </div>
    </section>

    <section id="form">
        <div class="container mb-md-5 py-5">
            <div class="row d-flex justify-content-evenly">
                <div class="col-11 col-md-12 mb-3 mb-md-0 col-md-5 rounded bg-light p-4">
                    <h1 class="mb-3">Video</h1>

                    <!-- Button trigger modal -->
                    <button type="button" class="border border-light" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        <img src="{{ asset('images/haji-furoda/0.jpg') }}" class="d-block w-100" alt="" srcset="">
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <iframe src="https://www.youtube.com/embed/QtT8swjxvSo" title="YouTube video player"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-11 col-md-12 col-md-5 rounded bg-light p-4">
                    <form method="POST">
                        @csrf
                        @method('POST')
                        <div class="mb-4">
                            <p>
                                Jika anda ingin mengetahui lebih lanjut mengenai informasi Haji Plus/Khusus Bersama Jejak
                                Imani, silakan isi form dibawah ini dan klik tombol Submit. Kami akan menghubungi Anda.
                            </p>
                        </div>
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">No. Handphone/WA <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="no_hp" name="no_hp">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Alamat Email <span
                                    class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="message" class="form-label">Pesan <span class="text-danger">*</span></label>
                            <textarea class="form-control" name="message" id="message" cols="30" rows="1"></textarea>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-warning btn-shake text-light">SUBMIT <i
                                    class="fas fa-arrow-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="btn-wa">
        <div class="container-fluid d-flex justify-content-center">
            <div class="col-10 col-md-3 text-center shadow btn bg-light btn-shake">
                <a href="https://api.whatsapp.com/send?phone=628119513223&text=Assalamu%E2%80%99alaikum%20Pak%20Tomi,%20saya%20mau%20tanya%20info%20prihal%20Paket%20Umroh%20yang%20Di%20Jejak%20Imani.%20Saya%20lihat%20dari%20website."
                    target="_blank" class="text-decoration-none text-dark">
                    <div class="row d-flex align-items-center">
                        <div class="col-12 py-1">
                            <P class="text-center text-light fw-normal mb-0">
                                INFO DETAIL? <br>
                                KLIK INI UNTUK TANYA KAMI ???
                            </P>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    @include('components.homepage.paket-lainnya')
@endsection
