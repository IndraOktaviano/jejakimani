@extends('layouts.homepage.master')

@section('title')
    Jejak Islami Turki
@endsection

@section('css')
    <style>
        #hero {
            background: url("{{ asset('images/jejakislami/Background_Slide_Turkey.jpeg') }}");
            background-position: center;
            background-size: cover;
            height: 600px;
        }

        #hero h1 {
            font-size: 45pt;
        }

        #hero h3 {
            font-family: 'gotham';
        }

        #hero .btn {
            color: #AF3019;
            border-radius: 15px;
        }

        #section2 .text-red {
            color: #8B2830;
            font-family: 'gotham';
        }

        #section2 .btn {
            background: #AF3019;
            color: white;
            border-radius: 15px;
        }

        #keunggulan h1 span {
            font-family: 'gotham';
        }

        #keunggulan a {
            color: #AF3019;
        }

        #keunggulan .card {
            border: none;
        }

        #keunggulan img {
            border-radius: 100%;
            /* width: 50%; */
        }

        #collapseRencana .events li {
            display: flex;
            color: #333333;
        }

        #collapseRencana .events time {
            position: relative;
            padding: 0 1.5em;
        }

        #collapseRencana .events time::after {
            content: "";
            position: absolute;
            z-index: 2;
            right: -2px;
            top: 0;
            transform: translateX(50%);
            border-radius: 50%;
            background: #fff;
            border: 1px #ccc solid;
            width: 1em;
            height: 1em;
        }


        #collapseRencana .events span {
            padding: 0 1.5em 1.5em 1.5em;
            position: relative;
        }

        #collapseRencana .events span::before {
            content: "";
            position: absolute;
            z-index: 1;
            left: 0;
            height: 100%;
            border-left: 3px #ccc dashed;
        }

        #collapseRencana .events strong {
            display: block;
            font-weight: bolder;
        }

        #collapseRencana .events {
            margin: 1em;
        }

        #collapseRencana .events,
        #collapseRencana .events *::before,
        #collapseRencana .events *::after {
            box-sizing: border-box;
            font-family: 'gotham';
        }

        #collapseFasilitas .lists h4 {
            font-size: 1.3rem;
        }

        #collapseFasilitas .lists h4 i {
            font-family: 'gotham';
        }

        #form {
            background: #8B2830;
            color: #fff;
        }

        #form form {
            color: black
        }

        #form .form-label {
            font-family: 'gotham';
            font-weight: bold;
        }

        #form .form-control {
            border-radius: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-color: black;
            background-color: transparent;
            box-shadow: none;
        }

        #form .rounded {
            border-radius: 15px !important;
        }

        #form .card {
            border-radius: 22px;
        }

        #form .card a {
            color: #8B2830;
        }

        .top-10 {
            margin-top: 10rem;
        }

        .bottom-10 {
            margin-bottom: 10rem;
        }

        @media (max-width: 430px) {
            #hero {
                height: 226px;
            }

            #hero h1 {
                font-size: 15pt;
            }

            #hero .btn {
                border-radius: 10px;
                font-size: 7pt;
            }

            #hero h3 {
                font-size: 12pt;
            }

            #section2 h5 {
                font-size: 10pt;
            }

            #section2 h3 {
                font-size: 11pt;
            }

            #keunggulan a {
                font-size: 10pt;
            }

            #collapseKeunggulan h3 {
                font-size: 8pt;
            }

            #collapseRencana .events time {
                padding: 0;
            }

            #collapseFasilitas h4, #collapseFasilitas .lists h4, #collapseFasilitas h1 {
                font-size: 12pt;
            }

            #collapseFasilitas i, #collapseFasilitas .lists i {
                font-size: 26px;
            }

            #form {
                margin-inline: 15px;
            }

            #form h4 {
                font-size: 14pt;
            }
        }

    </style>
@endsection

@section('content')
    <section id="hero">
        <div class="container py-2 py-md-5">
            <div class="row">
                <div class="col-9 col-md-6 text-light">
                    <h1>JEJAK ISLAMI <br> TURKEY</h1>
                    <h1>OTTOMAN</h1>
                    <a href="https://api.whatsapp.com/send?phone=6285720028100&text=Assalamu%E2%80%99alaikum%20Mbak%20Yuta%20saya%20melihat%20paket%20Jejak%20Islami%20Turkey%20di%20website%20dan%20ingin%20bertanya%20tentang%20paket%20tur%20Jejak%20Islami%20Turki"
                        target="_blank" class="btn btn-lg btn-light btn-shake fw-bold mt-3">
                        <i>Daftar Sekarang</i>
                    </a>
                    <h3 class="fw-normal mt-4">
                        Menyusuri Keindahan dan Sejarah Peradaban Islami di Turki
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <section id="section2">
        <div class="container">
            <hr>
            <div class="row mb-5">
                <div class="col-4 text-center">
                    <h5>PESAWAT</h5>
                    <img src="{{ asset($section_1->image) }}" alt="" class="img-fluid">
                </div>
                <div class="col-4 text-center">
                    <h5>HARGA</h5>
                    <h3 class="text-red">
                        {{ $section_1->price }}
                    </h3>
                </div>
                <div class="col-4 text-center">
                    <h5>KEBERANGKATAN</h5>
                    <h3 class="text-red">
                        {{ $section_1->name }}
                    </h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-3 col-7">
                    <a href="https://api.whatsapp.com/send?phone={{ $section_1->phone }}={{ $section_1->text_wa }}"
                        @if (isset($section_1->event)) onclick="gtag('event', {{ $section_1->event }}, { 'event_category': {{ $section_1->event_category ?? '' }}, 'event_label': {{ $section_1->event_label ?? '' }}});" @endif
                        target="_blank" class="btn btn-lg btn-light btn-shake fw-bold mt-3">
                        <i>{{ $section_1->label_wa }}</i>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="keunggulan">
        <div class="container pt-5">
            <h1 class="text-center">
                <a class="text-decoration-none" data-bs-toggle="collapse" href="#collapseKeunggulan" role="button"
                    aria-expanded="false" aria-controls="collapseKeunggulan">
                    KEUNGGULAN <span>JEJAK ISLAMI TURKEY</span> <i class="fas fa-arrow-circle-right"></i>
                </a>
            </h1>
            <div class="collapse" id="collapseKeunggulan">
                <div class="row">
                    @foreach ($provit as $item)
                        @php
                            $json = json_decode($item->json);
                        @endphp
                        <div class="col-6">
                            <div class="card mb-3" style="max-width: 540px;">
                                <div class="row g-0">
                                    <div class="col-4">
                                        <img src="{{ asset($json->image) }}" class="img-fluid" alt="image">
                                    </div>
                                    <div class="col-8">
                                        <div class="card-body">
                                            <h3>{{ $json->name }}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <h1 class="text-center mt-md-5">
                <a class="text-decoration-none" data-bs-toggle="collapse" href="#collapseRencana" role="button"
                    aria-expanded="false" aria-controls="collapseRencana">
                    RENCANA PERJALANAN <span>JEJAK ISLAMI TURKEY</span> <i class="fas fa-arrow-circle-right"></i>
                </a>
            </h1>
            <div class="collapse" id="collapseRencana">
                <div class="row">
                    <div class="col-12">
                        <ul class="events">
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($trip_plan as $item)
                                @php
                                    $json = json_decode($item->json);
                                    $i++;
                                @endphp
                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day {{ $i }} : {{ $json->name }}</strong>
                                        {!! $json->text !!}.
                                    </span>
                                </li>
                            @endforeach

                            {{-- <div class="d-none">
                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 2 : ISTANBUL – BURSA (<i>Lunch, Dinner</i>)</strong>
                                        Tiba di <b>Bandara Istanbul</b> dilanjutkan perjalanan ke <b>Bursa</b>, mengunjungi
                                        <i><b>Grand Mosque, Green Mosque, Uludag Mountain dan Cable Car</b></i>. Bermalam di
                                        <b>Bursa</b>. Hotel : Holiday Inn/setaraf *5
                                    </span>
                                </li>

                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 3 : BURSA – KUSADASI (<i>3x Meals</i>)</strong>
                                        Setelah sarapan mengunjungi <i><b>Turkish Delight Showroom</b></i> dilanjutkan
                                        perjalanan menuju <b>Kusadasi</b>. Mengunjungi <b>Masjid Isabey</b>. Setelah itu
                                        perjalanan ke hotel
                                        untuk makan malam dan menginap. Hotel : Korumar Hotel/setaraf *5
                                    </span>
                                </li>

                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 4 : KUSADASI – PAMUKKALE (<i>3x Meals</i>)</strong>
                                        Setelah sarapan mengunjungi <b>Ephesus, <i>Leather Factory Outlet dan Ephesus
                                                Ancient
                                                City</i></b>.
                                        Kemudian berkendara menuju <b>Pamukkale</b> untuk mengunjungi <b>Hierapolis</b>,
                                        kota
                                        peninggalan
                                        Romawi Kuno untuk melihat <b><i>Cotton Castle</i></b>. Hotel : Adempira
                                        Hotel/setaraf *5
                                    </span>
                                </li>

                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 5 : PAMUKKALE – KONYA – CAPPADOCIA (<i>3x Meals</i>)</strong>
                                        Setelah sarapan mengunjungi <b><i>Textile Outlet Company dan Mevlana Museum</i></b>.
                                        Selanjutnya
                                        perjalanan ke <b>Cappadocia. <i>Photo stop</i></b> di <b>Seljukian Caravanserai
                                            ‘Sultanhan’</b>. Bermalam di
                                        hotel <b>Cappadocia</b>. Hotel : Cave Hotel/setaraf *5
                                    </span>
                                </li>

                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 6 : CAPPADOCIA (<i>3x Meals</i>)</strong>
                                        <b><i>Hot Air Balloon (Optional).</i></b> Setelah sarapan mengunjungi <b><i>Ozkonak
                                                Underground City,</i></b>
                                        kemudian perjalanan ke <b><i>Erciyes Mountain (Snow Experience and Cable Car),
                                                Carpet
                                                Workshop, Turqoise Jewellery Center, Pasabag dan Derbent Valley,</i></b>
                                        melihat
                                        <b>Tarian Sufi (<i>Optional</i>)</b> dilanjutkan mengunjungi <b><i>Pottery
                                                Workshop.</i></b>
                                        Bermalam di hotel <b><i>Cappadocia.</i></b> Hotel
                                        : Cave Hotel/setaraf *5
                                    </span>
                                </li>

                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 7 : CAPPADOCIA – ANKARA (<i>3x Meals</i>)</strong>
                                        Berkendara menuju <b>Ankara,</b> mengunjungi <b><i>Uçhisar Citadel, Pigeon Valley,
                                                Mushroom Headrock,
                                                Avanos Pottery Village, Göreme View Point, Salt Lake dan Ataturk
                                                Mouseleum.</i></b> Setelah itu
                                        menuju hotel untuk <b><i>check-in</i></b> dan bermalam di <b>Istanbul</b>. Hotel :
                                        Pullman Hotel/setaraf *5
                                    </span>
                                </li>

                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 8 : ISTANBUL (<i>3x Meals</i>)</strong>
                                        Setelah sarapan bersiap mengikuti <b>Bosphorus Cruise</b>, perjalanan di atas selat
                                        yang
                                        menghubungkan benua Asia dan Eropa. Menikmati pemandangan indah sembari melewati
                                        istana-istana dan bangunan khas arsitektur Ottoman, benteng, rumah kayu tradisional
                                        dan
                                        villa modern sampai waktu yang ditentukan. Kemudian mengunjungi <b>Topkapi
                                            Palace</b>,
                                        Istana
                                        Kesultanan Ottoman jaman dahulu. Setelah itu berbelanja di <b>Grand Bazaar</b>.
                                        Hotel:
                                        Golden
                                        Way/setaraf *4.
                                    </span>
                                </li>

                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 9 : ISTANBUL (<i>3x Meals</i>)</strong>
                                        Setelah sarapan mengunjungi <i><b>Blue Mosque.</b></i> Kemudian berkunjung ke
                                        Hippodrome
                                        Square,
                                        bangunan peninggalan Kekaisaran Bizantium yang saat itu merupakan pusat olahraga dan
                                        sosial. Setelah berkunjung dan shalat fardhu di <b>Hagia Sophia.</b> Mengunjungi
                                        <b><i>Taksim Square</i></b>
                                        lalu perjalanan kembali ke <b><i>airport</i></b> untuk penerbangan pulang ke Tanah
                                        Air.
                                    </span>
                                </li>

                                <li>
                                    <time></time>
                                    <span>
                                        <strong>Day 10 : ISTANBUL – JAKARTA : ARRIVAL JAKARTA</strong>
                                        Tiba di Jakarta
                                    </span>
                                </li>
                            </div> --}}
                        </ul>
                    </div>
                    <div class="col-12 fw-bold fst-italic">
                        <p class="ms-3 mb-0">*Opsional:</p>
                        {!! $optional !!}
                        <br>
                        Itinerary dapat berubah mengikuti kondisi dan situasi setempat.
                    </div>
                    <div class="col-12 text-center">
                        <img class="img-fluid" src="{{ asset('images/jejakislami/Peta_Turkey.jpg') }}"
                            alt="{{ asset('images/jejakislami/Peta_Turkey.jpg') }}">
                    </div>
                </div>
            </div>

            <h1 class="text-center mt-md-5">
                <a class="text-decoration-none" data-bs-toggle="collapse" href="#collapseFasilitas" role="button"
                    aria-expanded="false" aria-controls="collapseFasilitas">
                    FASILITAS PERJALANAN <span>JEJAK ISLAMI TURKEY</span> <i class="fas fa-arrow-circle-right"></i>
                </a>
            </h1>
            <div class="collapse" id="collapseFasilitas">
                <div class="row mt-5">
                    @foreach ($trip_facility as $item)
                        @php
                            $json = json_decode($item->json);
                        @endphp
                        <div class="{{ isset($json->detail) ? 'col-12' : 'col-md-6' }} mb-3 mb-md-5">
                            <div class="row">
                                <div class="col-md-auto col-2 text-center">
                                    <i class="{{ $json->icon }} fa-2x"></i>
                                </div>
                                <div class="col-10">
                                    <h4>{{ $json->name }}</h4>
                                    @if (isset($json->detail))
                                        <div class="row">
                                            @foreach ($json->detail as $sub)
                                                <div class="col-12 col-md-6">
                                                    <p class="mb-0">
                                                        <b> {{ $sub->location }} :</b> {{ $sub->name }}
                                                    </p>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach

                    {{-- <div class="d-none">
                        <div class="col-6 mb-5">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-plane fa-2x"></i>
                                </div>
                                <div class="col-10 ps-3">
                                    <h4>
                                        Tiket PP Ekonomi
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mb-5">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-utensils fa-2x"></i>
                                </div>
                                <div class="col-10 ps-3">
                                    <h4>
                                        Makan & Minum Sesuai itinerary
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mb-5">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-users fa-2x"></i>
                                </div>
                                <div class="col-10 ps-3">
                                    <h4>
                                        Guide & Tour Leader
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mb-5">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-ship fa-2x"></i>
                                </div>
                                <div class="col-10 ps-3">
                                    <h4>
                                        Bosphorus Cruise
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mb-5">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-bus fa-2x"></i>
                                </div>
                                <div class="col-10 ps-3">
                                    <h4>
                                        Transportasi Bus AC
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mb-5">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-ticket-alt fa-2x"></i>
                                </div>
                                <div class="col-10 ps-3">
                                    <h4>
                                        Tiket masuk tujuan wisata sesuai itinerary
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div> --}}


                    <div class="row lists">
                        <div class="col-12 mb-3 text-center orange">
                            <h1>
                                BIAYA TIDAK TERMASUK:
                            </h1>
                        </div>
                        @foreach ($no_include as $item)
                            @php
                                $json = json_decode($item->json);
                            @endphp
                            <div class="col-12 col-md-6 mb-3 mb-md-5">
                                <div class="row">
                                    <div class="col-2 col-md-1 text-center">
                                        <i class="{{ $json->icon }} fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            {{ $json->name }}
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="d-none">
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-user-md fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            PCR Test di Turki
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-car-alt fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Tips Guide & Driver
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-briefcase-medical fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            PCR test di Indonesia
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-utensils fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Makan minum diluar program/itinerary
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-map-marker-alt fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Optional tour
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fa fa-ticket-alt fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Tiket Domestik
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="far fa-money-bill-alt fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Pengeluaran pribadi
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-sign-out-alt fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Early Check-in dan Late Check-out
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-suitcase fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Kelebihan bagasi
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-briefcase fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Handling Bandara CGK & IST
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="far fa-file-alt fa-2x"></i>
                                    </div>
                                    <div class="col-10 ps-3">
                                        <h4>
                                            Visa (<i class="">Non Refund</i>)
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section id="form">
        <div class="container py-5">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-8 mb-3">
                    <h4 class="text-center">
                        Jika anda ingin mengetahui lebih lanjut mengenai informasi Jejak Islami Turkey, Silakan isi form di
                        bawah
                        ini dan klik tombol Submit. Kami akan menghubungi Anda.
                    </h4>
                </div>
                <div class="col-10 col-md-6 bg-light rounded p-4">
                    <form method="POST">
                        @csrf
                        @method('POST')
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">No. Handphone/WA <span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="no_hp" name="no_hp">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Alamat Email <span
                                    class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="message" class="form-label">Pesan <span
                                    class="text-danger">*</span></label>
                            <textarea class="form-control" name="message" id="message" cols="30" rows="1"></textarea>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-warning text-light">SUBMIT <i
                                    class="fas fa-arrow-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-12 text-center top-10">
                    <h5>Info Detail? Konsultasikan Sekarang</h5>
                </div>
                <div class="card bottom-10 btn-shake" style="width: 20rem;">
                    <a href="" class="text-decoration-none">
                        <div class="card-body text-center">
                            <h5 class="card-title m-0"><i>WHATSAPP KAMI</i></h5>
                            <h5 class="card-title"><i>(KONSULTASI GRATIS)</i></h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
