@extends('layouts.homepage.master')

@section('title')
    Artikel Islami
@endsection

@section('css')
    <style>
        .text-cut {
            overflow: hidden;
            width: 100%;
            display: -webkit-box;
            -webkit-box-orient: vertical;
        }

        .line-4 {
            -webkit-line-clamp: 4;
        }

    </style>
@endsection

@section('content')
    <section id="article">
        <div class="container py-5">
            <div class="row">
                @foreach ($article as $item)
                    <div class="col-md-6 col-lg-4">
                        <div class="card">
                            <a href="{{ route('article.view', $item->slug) }}">
                                <img src="{{ asset($item->image) }}" class="card-img-top" alt="...">
                            </a>
                            <div class="card-body">
                                <p><small>{{ date('d F Y') }}</small></p>
                                <a href="{{ route('article.view', $item->slug) }}" class="text-decoration-none link-dark">
                                    <h2> {{ $item->name }} </h2>
                                </a>
                                <div class="card-text text-cut line-4">{!! $item->json !!}</div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
