@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('customCSS')
<style>
    .handle {
        min-width: 18px;
        background: #607D8B;
        height: 15px;
        display: inline-block;
        cursor: move;
        margin-right: 10px;
    }

    .list-group-item {
        display: flex;
        align-items: center;
    }

    .highlight {
        background: #f7e7d3;
        min-height: 30px;
        list-style-type: none;
    }
</style>
@endsection

@section('content')
<!--begin::Card-->
<div class="card">
    <!--begin::Card header-->
    <div class="card-header border-0 pt-6">
        <!--begin::Card title-->
        <div class="card-title">
            <!--begin::Search-->
            <div class="d-flex align-items-center position-relative my-1">
                <i class="bi bi-search fs-5 position-absolute ms-6"></i>
                <!--end::Svg Icon-->
                <input type="text" data-kt-table-filter="search" class="form-control form-control-solid w-250px ps-15"
                    placeholder="Cari Keunggulan" />
            </div>
            <!--end::Search-->
        </div>
        <!--begin::Card title-->
        <div class="card-toolbar">
            <!--begin::Toolbar-->
            <div class="d-flex justify-content-end" data-kt-table-toolbar="base">
                <!--begin::Add -->
                <button type="button" class="btn btn-info me-3" data-bs-toggle="modal"
                    data-bs-target="#urutanModal">Urutan</button>
                    
                <a href=" {{route('cms.content.jejak.islami.provit.create')}} " class="btn btn-primary">Tambah</a>
                <!--end::Add -->
            </div>
        </div>
        <!--end::Card toolbar--->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body pt-0">
        <!--begin::Table-->
        <table class="table align-top table-row-dashed fs-6 gy-5" id="kt_table">
            <!--begin::Table head-->
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0 cursor-pointer">
                    <th class="min-w-10px pe-2">
                        No
                    </th>
                    <th class="min-w-125px">Gambar</th>
                    <th class="min-w-125px">Judul</th>
                    <th class="text-end min-w-70px">Actions</th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <!--begin::Table body-->
            <tbody class="fw-bold text-gray-600">
                @foreach ($data as $value => $item)
                @php
                $json = json_decode($item->json)
                @endphp
                <tr>
                    <td> {{$value+1}} </td>
                    <td> <img src=" {{asset($json->image)}} " alt="image" class="img-fluid" style="max-height: 100px">
                    </td>
                    <td> {{$json->name}} </td>
                    <td>
                        <div class="btn-group" role="group">
                            <a href=" {{route('cms.content.jejak.islami.provit.edit', $item->id)}} "
                                class="btn btn-warning">Ubah</a>
                            <button type="button" class="btn btn-danger" data-kt-table-filter="delete_row"
                                data-id="{{$item->id}}" data-token="{{ csrf_token() }}">Hapus</button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <!--end::Table body-->
        </table>
        <!--end::Table-->
    </div>
    <!--end::Card body-->
</div>
<!--end::Card-->

<!-- Modal -->
<div class="modal fade" id="urutanModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
aria-labelledby="staticBackdropLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Urutan {{ $title }}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{ route('cms.content.jejak.islami.sorter.update') }}" method="POST" id="formOrder">
                <input type="hidden" name="slug" value="jejak-islami">
                <input type="hidden" name="type" value="provit">
                <input type="hidden" name="route" value="{{ $route }}">
                <input type="hidden" name="_method" value="PUT">
                @csrf
                <ul class="sorter list-group">
                    @foreach ($order as $item)
                        <li class="list-group-item">
                            <span class="handle"></span>
                            <input type="hidden" name="id[]" value="{{ $item['id'] }}">
                            {{ $item['name'] }}
                        </li>
                    @endforeach
                </ul>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
            <button type="submit" form="formOrder" class="btn btn-primary">Simpan Perubahan</button>
        </div>
    </div>
</div>
</div>
@endsection

@section('customJS')
<script src="{{asset('cms/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('cms/js/custom/dashboard/content/jejak-islami/provit/index.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
    $(document).ready(function(){

        var target = $('.sorter');
        target.sortable({
            handle: '.handle',
            placeholder: 'highlight',
            axis: "y",
        })

    })  
</script>
@endsection