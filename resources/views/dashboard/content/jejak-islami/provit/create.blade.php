@extends('layouts.app')

@section('title')
Tambah {{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">Tambah {{$title}}</div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_create_form"
                method="POST" action="{{ route('cms.content.jejak.islami.provit.store') }}">
                @csrf
                {{-- begin:Wrapper --}}
                <div class="d-flex justify-content-between aling-items-center order-last row">

                    {{-- begin:Row 1 --}}
                    <div class="col-md-8 order-md-2 order-sm-1">
                        <div class="row">
                            <div class="col-md-12 mb-10 fv-row">
                                <label class="form-label fs-6 fw-bolder text-dark">Judul</label>
                                <input type="text"
                                    class="form-control form-control w-100 @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autofocus />
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    {{-- end:Row 1 --}}

                    {{-- begin:Row 2 --}}
                    <div class="col-md-4 order-md-1 order-sm-3 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Gambar</label> <br>
                        <img id="image-preview" onclick="chooseImage()" src="" alt="image"
                            class="img-fluid cursor-pointer rounded-3">
                        <input type="file" id="image" name="image" onchange="previewImage()" accept="image/*"
                            class="d-none @error('image') is-invalid @enderror">
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    {{-- end:Row 2 --}}

                </div>
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <a href="{{route('cms.content.jejak.islami.provit.index')}}" class="btn btn-secondary">Kembali</a>
                <button type="submit" form="kt_create_form" class="btn btn-success ms-2"
                    id="kt_create_submit">
                    <span class="indicator-label">Simpan</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/content/jejak-islami/provit/create.js')}}"></script>
<script>
    let url = `{{asset('upload/empty.jpg')}}`
    
    document.getElementById("image-preview").src = url;

    function previewImage() {
      document.getElementById("image-preview").style.display = "inline-block";
      if(!document.getElementById("image").files[0])    {          
            document.getElementById("image-preview").src = url;
      }
      else {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image").files[0]);      
            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
           };   
      }    
    };

    function chooseImage() {
        document.getElementById("image").click()
    }
</script>
@endsection