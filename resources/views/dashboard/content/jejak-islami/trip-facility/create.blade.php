@extends('layouts.app')

@section('title')
    Tambah {{ $title }}
@endsection

@section('content')
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card header-->
            <div class="card-header border-bottom pt-6">
                <!--begin::Card title-->
                <div class="card-title">
                    <div class="fs-3 fw-bolder">Tambah {{ $title }}</div>
                </div>
                <!--end::Card title-->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body p-5">
                <form class="form w-100" novalidate="novalidate" id="kt_create_form" method="POST"
                    action="{{ route('cms.content.jejak.islami.trip-facility.store') }}">
                    @csrf
                    {{-- begin:Wrapper --}}
                    <div class="d-flex justify-content-between aling-items-center order-last row">

                        {{-- begin:Row 1 --}}
                        <div class="col-md-12 order-md-2 order-sm-1">
                            <div class="row">
                                <div class="col-md-12 mb-10 fv-row">
                                    <label class="form-label fs-6 fw-bolder text-dark">Name</label>
                                    <input type="text" name="name"
                                        class="form-control form-control w-100 @error('name') is-invalid @enderror"
                                        value="{{ old('name') }}" required autofocus />
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12 mb-10 fv-row">
                                    <label class="form-label fs-6 fw-bolder text-dark">Icon</label>
                                    <input type="text" name="icon"
                                        class="form-control form-control w-100 @error('icon') is-invalid @enderror"
                                        value="{{ old('icon') }}" required autofocus />
                                    <i>For more icon please visit : <a href="https://fontawesome.com/v5/search?m=free"
                                            target="_blank">https://fontawesome.com/v5/search?m=free</a></i>
                                    @error('icon')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        {{-- end:Row 1 --}}
                    </div>
                    {{-- end:Wrapper --}}
                </form>
            </div>
            <!--end::Card body-->
            <div class="card-footer">
                <div class="d-flex justify-content-end">
                    <a href="{{ route('cms.content.jejak.islami.trip-facility.index') }}"
                        class="btn btn-secondary">Kembali</a>
                    <button type="submit" form="kt_create_form" class="btn btn-success ms-2" id="kt_create_submit">
                        <span class="indicator-label">Simpan</span>
                        <span class="indicator-progress">Mohon tunggu...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
@endsection

@section('customJS')
    <script src="{{ asset('cms/js/custom/dashboard/content/jejak-islami/trip-facility/create.js') }}"></script>
@endsection
