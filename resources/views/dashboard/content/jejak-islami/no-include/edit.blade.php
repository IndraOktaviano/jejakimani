@extends('layouts.app')

@section('title')
    Perbaharui {{ $title }}
@endsection

@section('content')
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card header-->
            <div class="card-header border-bottom pt-6">
                <!--begin::Card title-->
                <div class="card-title">
                    <div class="fs-3 fw-bolder">Perbaharui {{ $title }}</div>
                </div>
                <!--end::Card title-->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body p-5">
                <form class="form w-100" novalidate="novalidate" id="kt_edit_form"
                    method="POST" action="{{ route('cms.content.jejak.islami.no-include.update', $data->id) }}">
                    <input name="_method" type="hidden" value="PUT">
                    <input name="id" type="hidden" value=" {{ $data->id }} ">
                    @php
                        $json = json_decode($data->json);
                    @endphp
                    @csrf
                    {{-- begin:Wrapper --}}

                    {{-- begin:Row 1 --}}
                    <div class="row">
                        <div class="col-md-12 mb-10 fv-row">
                            <label class="form-label fs-6 fw-bolder text-dark">Judul</label>
                            <input type="text" class="form-control form-control w-100 @error('name') is-invalid @enderror"
                                name="name" value="{{ old('name', $json->name) }}" required autofocus />
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-12 mb-10 fv-row">
                            <label class="form-label fs-6 fw-bolder text-dark">Icon</label>
                            <input type="text" name="icon"
                                class="form-control form-control w-100 @error('icon') is-invalid @enderror"
                                value="{{ old('icon', $json->icon) }}" required autofocus />
                            <i>For more icon please visit : <a href="https://fontawesome.com/v5/search?m=free"
                                    target="_blank">https://fontawesome.com/v5/search?m=free</a></i>
                            @error('icon')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>                        
                    </div>
                    {{-- end:Row 1 --}}
                </form>
            </div>
            {{-- end:Wrapper --}}
            <!--end::Card body-->
            <div class="card-footer">
                <div class="d-flex justify-content-end">
                    <a href="{{ route('cms.content.jejak.islami.no-include.index') }}" class="btn btn-secondary">Kembali</a>
                    <button type="submit" form="kt_edit_form" class="btn btn-success ms-2" id="kt_edit_submit">
                        <span class="indicator-label">Update</span>
                        <span class="indicator-progress">Mohon tunggu...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('customJS')
    <script src="{{ asset('cms/js/custom/dashboard/content/jejak-islami/trip-facility/edit.js') }}"></script>
@endsection
