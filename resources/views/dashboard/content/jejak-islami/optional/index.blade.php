@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">{{$title}}</div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" id="kt_form"
                method="POST" action="{{ route('cms.content.jejak.islami.optional.update') }}">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="id" value="{{$data->id}}">
                {{-- begin:Wrapper --}}
                <div class="row">
                    {{-- begin:Row 1 --}}
                    <div class="col-md-12 fv-row mb-10" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Deskripsi</label>
                        <textarea name="text" id="mytiny"
                            class="form-control @error('text') is-invalid @enderror">{{old('text', $data->json)}}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    {{-- end:Row 1 --}}
                </div>
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <button type="submit" form="kt_form" class="btn btn-success ms-2"
                    id="kt_submit">
                    <span class="indicator-label">Simpan</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/content/jejak-islami/optional.js')}}"></script>
<script src="{{asset('js/tinymce.min.js')}}"></script>
<script>
    tinymce.init({
      selector: '#mytiny',
      plugins: 'print preview paste searchreplace autolink directionality visualblocks visualchars code fullscreen link media pagebreak template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools colorpicker textpattern help',
      toolbar: 'formatselect | fontsizeselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat |',
    });
</script>
@endsection