@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content')
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card header-->
            <div class="card-header border-bottom pt-6">
                <!--begin::Card title-->
                <div class="card-title">
                    <div class="fs-3 fw-bolder">{{ $title }}</div>
                </div>
                <!--end::Card title-->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body p-5">
                <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_form" method="POST"
                    action="{{ route('cms.content.jejak.islami.section-1.update', $data->id) }}">
                    <input name="_method" type="hidden" value="PUT">
                    @csrf
                    @php
                        $json = json_decode($data->json);
                    @endphp
                    <input type="hidden" name="id" value="{{ $data->id }}">                    
                    {{-- begin:Wrapper --}}
                    <div class="row">
                        {{-- begin:Row 1 --}}
                        <div class="col-md-12 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Harga</label>
                            <input name="price" class="form-control @error('price') is-invalid @enderror"
                                value="{{ old('price', $json->price) }}">
                            @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-12 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Keberangkatan</label>
                            <input name="name" class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name', $json->name) }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{-- end:Row 1 --}}

                        {{-- begin:Row 2 --}}
                        <div class="col-md-6 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Pilih Gambar Baru</label> <br>
                            <img id="image-preview" onclick="chooseImage()" src="" alt="image"
                                class="img-fluid cursor-pointer rounded-3" style="max-height: 300px">
                            <input type="file" id="image" name="image" onchange="previewImage()" accept="image/*"
                                class="d-none @error('image') is-invalid @enderror">
                            @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 fv-row" style="min-height: 300px">
                            <label class="form-label fs-6 fw-bolder text-dark">Gambar Lama</label> <br>
                            <img id="old-image" src="" alt="image" class="img-fluid cursor-pointer rounded-3"
                                style="max-height: 300px">
                        </div>
                        {{-- end:Row 2 --}}

                        {{-- Row 3 --}}
                        <div class="col-md-6 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Nomor WA</label>
                            <input type="number" name="phone" value="{{ old('phone', $json->phone) }}"
                                class="form-control hide-arrow @error('phone') is-invalid @enderror">
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Label Tombol WA</label>
                            <input type="text" name="label_wa" value="{{ old('label_wa', $json->label_wa) }}"
                                class="form-control @error('label_wa') is-invalid @enderror">
                            @error('label_wa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-12 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Deskripsi WA</label>
                            <textarea name="text_wa" class="form-control @error('text_wa') is-invalid @enderror"
                                rows="4">{{ urldecode(old('text_wa', $json->text_wa)) }}</textarea>
                            @error('text_wa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        @include('components.custom.tracker-input', [
                            'tracker' => $json,
                        ])
                        {{-- end:Row 3 --}}
                    </div>
                    {{-- end:Wrapper --}}
                </form>
            </div>
            <!--end::Card body-->
            <div class="card-footer">
                <div class="d-flex justify-content-end">
                    <a href="{{ route('cms.umroh.index') }}" class="btn btn-secondary">Kembali</a>
                    <button type="submit" form="kt_form" class="btn btn-success ms-2" id="kt_submit">
                        <span class="indicator-label">Simpan</span>
                        <span class="indicator-progress">Mohon tunggu...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
@endsection

@section('customJS')
    <script src="{{ asset('cms/js/custom/dashboard/content/jejak-islami/section-1.js') }}"></script>
    @if ($json)
        <script>
            let image = '{{ asset($json->image) }}'
            document.getElementById("old-image").src = image;
        </script>
    @endif
    <script>
        let url = `{{ asset('upload/empty.jpg') }}`

        document.getElementById("image-preview").src = url;

        function previewImage() {
            document.getElementById("image-preview").style.display = "inline-block";
            if (!document.getElementById("image").files[0]) {
                document.getElementById("image-preview").src = url;
            } else {
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("image").files[0]);
                oFReader.onload = function(oFREvent) {
                    document.getElementById("image-preview").src = oFREvent.target.result;
                };
            }
        };

        function chooseImage() {
            document.getElementById("image").click()
        }
    </script>
@endsection
