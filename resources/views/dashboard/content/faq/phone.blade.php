@extends('layouts.app')

@section('title')
Perbaharui {{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">Perbaharui Artikel</div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_content_faq_form"
                method="POST" action="{{ route('cms.content.faq.phone.update', $data->id) }}">
                <input name="_method" type="hidden" value="PUT">
                <input name="id" type="hidden" value=" {{$data->id}} ">
                @csrf
                {{-- begin:Wrapper --}}
                {{-- begin:Row 1 --}}
                <div class="row">
                    <div class="col-md-12 mb-10 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Kontak</label>
                        <textarea name="phone"
                            class="form-control @error('phone') is-invalid @enderror">{{old('phone', $data->json)}}</textarea>
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>                   
                </div>
                {{-- end:Row 1 --}}
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">                
                <button type="submit" form="kt_content_faq_form" class="btn btn-success ms-2"
                    id="kt_content_faq_submit">
                    <span class="indicator-label">Update</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/content/faq/phone.js')}}"></script>
@endsection