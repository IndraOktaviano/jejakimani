@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">{{$title}}</div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_content_sosial_media_form"
                method="POST" action="{{ route('cms.content.sosial.media.store') }}">
                @csrf
                {{-- begin:Wrapper --}}
                <div class="row">
                    {{-- begin:Row 1 --}}
                    <div class="col-md-6 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Email <i
                                class="fs-4 ms-1 mb-n2 text-warning bi bi-chat-dots"></i></label>
                        <input type="email" name="email" value="{{old('email', $data['email']->json)}}"
                            class="form-control @error('email') is-invalid @enderror">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Twitter <i
                                class="fs-4 ms-1 mb-n2 text-primary bi bi-twitter"></i></label>
                        <input placeholder="Kosongkan untuk menonaktifkan" type="text" name="twitter" value="{{old('twitter', $data['twitter']->json)}}"
                            class="form-control @error('twitter') is-invalid @enderror">
                        @error('twitter')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Facebook <i
                                class="fs-4 ms-1 mb-n2 text-primary bi bi-facebook"></i></label>
                        <input placeholder="Kosongkan untuk menonaktifkan" type="text" name="facebook" value="{{old('facebook', $data['facebook']->json)}}"
                            class="form-control @error('facebook') is-invalid @enderror">
                        @error('facebook')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Youtube <i
                                class="fs-4 ms-1 mb-n2 text-danger bi bi-youtube"></i></label>
                        <input placeholder="Kosongkan untuk menonaktifkan" type="text" name="youtube" value="{{old('youtube', $data['youtube']->json)}}"
                            class="form-control @error('youtube') is-invalid @enderror">
                        @error('youtube')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Instagram <i
                                class="fs-4 ms-1 mb-n2 text-info bi bi-instagram"></i></label>
                        <input placeholder="Kosongkan untuk menonaktifkan" type="text" name="instagram"
                            value="{{old('instagram', $data['instagram']->json)}}"
                            class="form-control @error('instagram') is-invalid @enderror">
                        @error('instagram')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Whatsapp <i
                                class="fs-4 ms-1 mb-n2 text-success bi bi-whatsapp"></i></label>
                        <input placeholder="Kosongkan untuk menonaktifkan" type="number" name="whatsapp"
                            value="{{old('whatsapp', $data['whatsapp']->phone)}}"
                            class="form-control @error('whatsapp') is-invalid @enderror">
                        @error('whatsapp')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-12 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Text WA <i
                                class="fs-4 ms-1 mb-n2 text-warning bi bi-card-text"></i></label>
                        <textarea name="text"
                            class="form-control @error('text') is-invalid @enderror">{{old('text', $data['whatsapp']->text)}}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    {{-- end:Row 1 --}}
                </div>
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <a href="{{route('cms.umroh.index')}}" class="btn btn-secondary">Kembali</a>
                <button type="submit" form="kt_content_sosial_media_form" class="btn btn-success ms-2"
                    id="kt_content_sosial_media_submit">
                    <span class="indicator-label">Simpan</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/content/sosial_media.js')}}"></script>
@endsection