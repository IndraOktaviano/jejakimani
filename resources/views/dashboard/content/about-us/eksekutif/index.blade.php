@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('content')
<!--begin::Card-->
<div class="card">
    <!--begin::Card header-->
    <div class="card-header border-0 pt-6">
        <!--begin::Card title-->
        <div class="card-title">
            <!--begin::Search-->
            <div class="d-flex align-items-center position-relative my-1">
                <i class="bi bi-search fs-5 position-absolute ms-6"></i>
                <!--end::Svg Icon-->
                <input type="text" data-kt-table-filter="search" class="form-control form-control-solid w-250px ps-15"
                    placeholder="Cari Eksekutif" />
            </div>
            <!--end::Search-->
        </div>
        <!--begin::Card title-->
        <div class="card-toolbar">
            <!--begin::Toolbar-->
            <div class="d-flex justify-content-end" data-kt-table-toolbar="base">
                <!--begin::Add category-->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                    data-bs-target="#createModal">Tambah</button>
                <!--end::Add category-->
            </div>
        </div>
        <!--end::Card toolbar-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body pt-0">
        <!--begin::Table-->
        <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table">
            <!--begin::Table head-->
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0 cursor-pointer">
                    <th class="min-w-10px pe-2">
                        No
                    </th>
                    <th class="min-w-125px">Nama</th>
                    <th class="min-w-125px">Jabatan</th>
                    <th class="min-w-125px">Deskripsi</th>
                    <td class="d-none">ID</td>
                    <th class="text-end min-w-70px">Actions</th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <!--begin::Table body-->
            <tbody class="fw-bold text-gray-600">
                @foreach ($data as $value => $item)
                @php
                $json = json_decode($item->json);
                @endphp
                <tr>
                    <td> {{$value+1}} </td>
                    <td> {{$json->name}} </td>
                    <td> {{$json->jabatan}} </td>
                    <td> {{$json->text}} </td>
                    <td class="d-none">{{$item->id}}</td>
                    <td class="text-end">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-warning" data-bs-toggle="modal"
                                data-kt-table-filter="edit_row" data-bs-target="#editModal">Ubah</button>
                            <button type="button" class="btn btn-danger" data-kt-table-filter="delete_row"
                                data-id=" {{$item->id}} " data-token="{{ csrf_token() }}">Hapus</button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <!--end::Table body-->
        </table>
        <!--end::Table-->
    </div>
    <!--end::Card body-->
</div>
<!--end::Card-->

<!-- Modal Create -->
<div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Eksekutif</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="form w-100" novalidate="novalidate" id="kt_create_form" method="POST"
                    action="{{ route('cms.content.about.us.eksekutif.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-12 fv-row">
                            <label class="form-label fs-6 fw-bolder text-dark">Nama</label>
                            <input type="text"
                                class="form-control form-control w-100 @error('name') is-invalid @enderror" name="name"
                                value="" required autofocus />
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-12 fv-row">
                            <label class="form-label fs-6 fw-bolder text-dark">Jabatan</label>
                            <input type="text" name="jabatan" value="" required autofocus
                                class="form-control form-control w-100 @error('jabatan') is-invalid @enderror" />
                            @error('jabatan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-12 fv-row">
                            <label class="form-label fs-6 fw-bolder text-dark">Deskripsi</label>
                            <textarea name="text"
                                class="form-control form-control w-100 @error('text') is-invalid @enderror"></textarea>
                            @error('text')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close-create-modal"
                    data-bs-dismiss="modal">Tutup</button>
                <button type="submit" form="kt_create_form" id="submit-create-modal" class="btn btn-primary">
                    <span class="indicator-label">Simpan</span>
                    <span class="indicator-progress">Mohon Tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Perbaharui Eksekutif</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="form w-100" novalidate="novalidate" id="kt_edit_form" method="POST"
                    action="{{ route('cms.content.about.us.eksekutif.update') }}">
                    @csrf
                    <input name="_method" type="hidden" value="PUT">
                    <input name="id" type="hidden" id="id-edit" value="">
                    <div class="row">
                        <div class="col-md-12 fv-row">
                            <label class="form-label fs-6 fw-bolder text-dark">Nama</label>
                            <input type="text" id="name-edit"
                                class="form-control form-control w-100 @error('name') is-invalid @enderror" name="name"
                                value="" required autofocus />
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-12 fv-row">
                            <label class="form-label fs-6 fw-bolder text-dark">Jabatan</label>
                            <input type="text" name="jabatan" value="" required autofocus id="jabatan-edit"
                                class="form-control form-control w-100 @error('jabatan') is-invalid @enderror" />
                            @error('jabatan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-12 fv-row">
                            <label class="form-label fs-6 fw-bolder text-dark">Deskripsi</label>
                            <textarea name="text" id="text-edit"
                                class="form-control form-control w-100 @error('text') is-invalid @enderror"></textarea>
                            @error('text')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close-edit-modal"
                    data-bs-dismiss="modal">Tutup</button>
                <button type="submit" form="kt_edit_form" id="submit-edit-modal" class="btn btn-primary">
                    <span class="indicator-label">Update</span>
                    <span class="indicator-progress">Mohon Tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/content/about-us/eksekutif.js')}}"></script>
<script src="{{asset('cms/plugins/custom/datatables/datatables.bundle.js')}}"></script>
@endsection