@extends('layouts.app')

@section('title')
Perbaharui {{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">Perbaharui {{$title}} </div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_content_about_us_form"
                method="POST" action="{{ route('cms.content.about.us.sejarah.update', $data->id) }}">
                <input name="_method" type="hidden" value="PUT">
                <input name="id" type="hidden" value=" {{$data->id}} ">
                @csrf
                @php
                $json = json_decode($data->json)
                @endphp
                {{-- begin:Wrapper --}}
                {{-- begin:Row 1 --}}
                <div class="row">
                    <div class="col-md-12 mb-10 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Deskripsi</label>
                        <textarea name="text" id="mytiny"
                            class="form-control @error('text') is-invalid @enderror">{{old('text', $json->text)}}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                {{-- end:Row 1 --}}

                {{-- begin:Row 2 --}}
                <div class="row">
                    <div class="col-md-5 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Pilih Gambar Baru</label> <br>
                        <img id="image-preview" onclick="chooseImage()" src="" alt="image"
                            class="img-fluid cursor-pointer rounded-3" style="max-height: 300px">
                        <input type="file" id="image" name="image" onchange="previewImage()" accept="image/*"
                            class="d-none @error('image') is-invalid @enderror">
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-2 d-none d-md-block"></div>
                    <div class="col-md-5 fv-row" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Gambar Lama</label> <br>
                        <img id="old-image" src="" alt="image" class="img-fluid cursor-pointer rounded-3"
                            style="max-height: 300px">
                    </div>
                </div>
                {{-- end:Row 2 --}}
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <button type="submit" form="kt_content_about_us_form" class="btn btn-success ms-2"
                    id="kt_content_about_us_submit">
                    <span class="indicator-label">Update</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/content/about-us/index.js')}}"></script>
<script src="{{asset('js/tinymce.min.js')}}"></script>
<script>
    tinymce.init({
      selector: '#mytiny',
      plugins: 'print preview paste searchreplace autolink directionality visualblocks visualchars code fullscreen link media pagebreak template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools colorpicker textpattern help',
      toolbar: 'formatselect | fontsizeselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat |',
    });
</script>
@if ($json)
<script>
    let image = '{{asset($json->image)}}'
    document.getElementById("old-image").src = image;
</script>
@endif
<script>
    let url = `{{asset('upload/empty.jpg')}}`
    
    document.getElementById("image-preview").src = url;

    function previewImage() {
      document.getElementById("image-preview").style.display = "inline-block";
      if(!document.getElementById("image").files[0])    {          
            document.getElementById("image-preview").src = url;
      }
      else {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image").files[0]);      
            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
           };   
      }    
    };

    function chooseImage() {
        document.getElementById("image").click()
    }
</script>
@endsection