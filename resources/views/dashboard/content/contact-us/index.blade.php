@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">{{$title}}</div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data"
                id="kt_content_contact_us_form" method="POST" action="{{ route('cms.content.contact.us.store') }}">
                @csrf
                {{-- begin:Wrapper --}}
                <div class="row">
                    {{-- begin:Row 1 --}}
                    <input type="hidden" name="type" value="{{$type}}">
                    <div class="col-md-12 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Deskripsi</label>
                        <textarea name="data"
                            class="form-control @error('data') is-invalid @enderror">{{old('data', $data->json)}}</textarea>
                        @error('data')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    {{-- end:Row 1 --}}
                </div>
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <a href="{{route('cms.umroh.index')}}" class="btn btn-secondary">Kembali</a>
                <button type="submit" form="kt_content_contact_us_form" class="btn btn-success ms-2"
                    id="kt_content_contact_us_submit">
                    <span class="indicator-label">Simpan</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/content/contact-us/index.js')}}"></script>
@endsection