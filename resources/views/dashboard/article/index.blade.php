@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('content')
<!--begin::Card-->
<div class="card">
    <!--begin::Card header-->
    <div class="card-header border-0 pt-6">
        <!--begin::Card title-->
        <div class="card-title">
            <!--begin::Search-->
            <div class="d-flex align-items-center position-relative my-1">
                <i class="bi bi-search fs-5 position-absolute ms-6"></i>
                <!--end::Svg Icon-->
                <input type="text" data-kt-table-filter="search" class="form-control form-control-solid w-250px ps-15"
                    placeholder="Cari Article" />
            </div>
            <!--end::Search-->
        </div>
        <!--begin::Card title-->
        <div class="card-toolbar">
            <!--begin::Toolbar-->
            <div class="d-flex justify-content-end" data-kt-table-toolbar="base">
                <!--begin::Add -->
                <a href=" {{route('cms.article.create')}} " class="btn btn-primary">Tambah</a>
                <!--end::Add -->
            </div>
        </div>
        <!--end::Card toolbar--->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body pt-0">
                <!--begin::Table-->
                <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table">
                    <!--begin::Table head-->
                    <thead>
                        <!--begin::Table row-->
                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0 cursor-pointer">
                            <th class="min-w-10px pe-2">
                                No
                            </th>
                            <th class="min-w-125px">Gambar</th>
                            <th class="min-w-125px">Judul</th>
                            <th class="min-w-125px">Status</th>
                            <th class="text-end min-w-70px">Actions</th>
                        </tr>
                        <!--end::Table row-->
                    </thead>
                    <!--end::Table head-->
                    <!--begin::Table body-->
                    <tbody class="fw-bold text-gray-600">
                        @foreach ($data as $value => $item)
                        <tr>
                            <td> {{$value+1}} </td>
                            <td> <img src=" {{asset($item->image)}} " alt="image" class="img-fluid"
                                    style="max-height: 50px">
                            </td>
                            <td> {{$item->name}} </td>
                            <td> {{$item->status ? "Publish": "Draft"}} </td>
                            <td>
                            <div class="btn-group" role="group">
                                <a href=" {{route('article.show', $item->id)}} " class="btn btn-success">Lihat</a>
                                <a href=" {{route('cms.article.edit', $item->id)}} " class="btn btn-warning">Ubah</a>
                                <button type="button" class="btn btn-danger" data-kt-table-filter="delete_row"
                                    data-id="{{$item->id}}" data-token="{{ csrf_token() }}">Hapus</button>
                            </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <!--end::Table body-->
                </table>
                <!--end::Table-->
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
        @endsection

        @section('customJS')
        <script src="{{asset('cms/js/custom/dashboard/article/index.js')}}"></script>
        <script src="{{asset('cms/plugins/custom/datatables/datatables.bundle.js')}}"></script>
        @endsection