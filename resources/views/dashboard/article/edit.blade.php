@extends('layouts.app')

@section('title')
Perbaharui {{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">Perbaharui Artikel</div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_article_edit_form"
                method="POST" action="{{ route('cms.article.update', $data->id) }}">
                <input name="_method" type="hidden" value="PUT">
                <input name="id" type="hidden" value=" {{$data->id}} ">
                @csrf
                {{-- begin:Wrapper --}}
                <div class="d-flex justify-content-between aling-items-center order-last row">

                    {{-- begin:Row 1 --}}
                    <div class="col-md-8 order-md-2 order-sm-1">
                        <div class="row">
                            <div class="col-md-12 mb-10 fv-row">
                                <label class="form-label fs-6 fw-bolder text-dark">Judul</label>
                                <input type="text"
                                    class="form-control form-control w-100 @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name', $data->name) }}" required autofocus />
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-10 fv-row">
                                <label class="form-label fs-6 fw-bolder text-dark">Slug</label>
                                <input type="text"
                                    class="form-control form-control w-100 @error('slug') is-invalid @enderror"
                                    name="slug" value="{{ old('slug', $data->slug) }}" required autofocus />
                                @error('slug')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-10 fv-row">
                                <label class="form-label fs-6 fw-bolder text-dark">Status</label>
                                <select name="status" data-control="select2" data-placeholder="Pilih Status"
                                    class="form-select @error('status') is-invalid @enderror">
                                    <option value=""></option>
                                    <option {{old('status', $data->status) == "1" ? 'selected' : ''}} value="1">Publish</option>
                                    <option {{old('status', $data->status) == "2" ? 'selected' : ''}} value="2">Draft</option>
                                </select>
                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    {{-- end:Row 1 --}}

                    {{-- begin:Row 2 --}}
                    <div class="col-md-4 order-md-1 order-sm-3 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Gambar</label> <br>
                        <img id="image-preview" onclick="chooseImage()" src="" alt="image"
                            class="img-fluid cursor-pointer rounded-3">
                        <input type="file" id="image" name="image" onchange="previewImage()" accept="image/*"
                            class="d-none @error('image') is-invalid @enderror">
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    {{-- end:Row 2 --}}

                    {{-- begin:Row 3 --}}
                    <div class="col-md-12 order-md-3 order-sm-2 fv-row mb-10" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Deskripsi</label>
                        <textarea name="text" id="mytiny"
                            class="form-control @error('text') is-invalid @enderror">{{old('text', $data->json)}}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    {{-- end:Row 3 --}}
                </div>
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <a href="{{route('cms.article.index')}}" class="btn btn-secondary">Kembali</a>
                <button type="submit" form="kt_article_edit_form" class="btn btn-success ms-2"
                    id="kt_article_edit_submit">
                    <span class="indicator-label">Update</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/article/edit.js')}}"></script>
<script src="{{asset('js/tinymce.min.js')}}"></script>
<script>
    tinymce.init({
        selector: '#mytiny',
			height: 300,
			plugins: 'print preview paste searchreplace autolink directionality visualblocks visualchars code fullscreen image link media pagebreak template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools colorpicker textpattern help',
			toolbar: 'formatselect | fontsizeselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | image table removeformat pagebreak |',
			visual_table_class: 'tiny-table',
			automatic_uploads: true,
			image_advtab: true,
			images_upload_url: "{{route('cms.tinymce.upload')}}",
			file_picker_types: 'image',
			paste_data_images: true,
			relative_urls: false,
			// remove_script_host: false,
			file_picker_callback: function(cb, value, meta) {
				var input = document.createElement('input');
				input.setAttribute('type', 'file');
				input.setAttribute('accept', 'image/*');
				input.onchange = function() {
					var file = this.files[0];
					var reader = new FileReader();
					reader.readAsDataURL(file);
					reader.onload = function() {
						var id = 'post-image-' + (new Date()).getTime();
						var blobCache = tinymce.activeEditor.editorUpload.blobCache;
						var blobInfo = blobCache.create(id, file, reader.result);
						blobCache.add(blobInfo);
						cb(blobInfo.blobUri(), {
							title: file.name
						});
					};
				};
				input.click();
			},
			images_upload_handler: function(blobInfo, success, failure) {
				var xhr, formData;

				xhr = new XMLHttpRequest();
				xhr.withCredentials = false;
				xhr.open('POST', "{{route('cms.tinymce.upload')}}");
                var token = '{{ csrf_token() }}';
                xhr.setRequestHeader("X-CSRF-Token", token);

				xhr.onload = function() {
					var json;

					if (xhr.status != 200) {
						failure('HTTP Error: ' + xhr.status);
						return;
					}

					json = JSON.parse(xhr.responseText);

					if (!json || typeof json.location != 'string') {
						failure('Invalid JSON: ' + xhr.responseText);
						return;
					}

					success(json.location);
				};

				formData = new FormData();
				formData.append('file', blobInfo.blob(), blobInfo.filename());

				xhr.send(formData);
			},
			fontsize_formats: " 8px 10px 12px 14px 18px 24px 36px",
		});
</script>
<script>
    let url = `{{asset($data->image)}}`
    
    document.getElementById("image-preview").src = url;

    function previewImage() {
      document.getElementById("image-preview").style.display = "inline-block";
      if(!document.getElementById("image").files[0])    {          
            document.getElementById("image-preview").src = url;
      }
      else {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image").files[0]);      
            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
           };   
      }     
    };

    function chooseImage() {
        document.getElementById("image").click()
    }
</script>
@endsection