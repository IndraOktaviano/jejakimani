@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('content')
<!--begin::Card-->
<div class="card bg-transparent">
    <!--begin::Card header-->
    <div class="card-header bg-transparent border-0 pt-1 mb-2">
        <!--begin::Card title-->
        <div class="card-title">
        </div>
        <!--begin::Card title-->
        <div class="card-toolbar">
            <!--begin::Toolbar-->
            <div class="d-flex justify-content-end" data-kt-umroh-table-toolbar="base">
                <!--begin::Add umroh-->
                <a href=" {{route('cms.haji.'.$slug.'.banner.create')}} " class="btn btn-primary">Tambah</a>
                <!--end::Add umroh-->
            </div>
        </div>
        <!--end::Card toolbar-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body bg-transparent pt-0" id="parent-card">
        <div class="row gy-5 g-xl-3">
            @foreach ($data as $item)
            <div class="col-lg-6">
                <!--begin::Card-->
                <div class="card card-xl-stretch">
                    <div class="card-body p-10 text-center" style="min-height: 300px">
                        <img src=" {{asset($item->json)}} " alt="" class="img-fluid" style="max-height: 300px">
                    </div>
                    <div class="card-footer text-end">
                        <a href=" {{route('cms.haji.'.$slug.'.banner.edit', $item->id)}} "
                            class="btn btn-sm btn-warning">Ubah</a>
                        <button type="submit" class="btn btn-danger btn-sm" form="kt_haji_banner_destroy"
                            data-id="{{$item->id}}" data-slug="{{$slug}}" data-text="{{ucwords($slug)}}"
                            data-url="{{route('cms.haji.banner.destroy', $item->id)}}" data-token="{{ csrf_token() }}">Hapus
                        </button>
                    </div>
                </div>
                {{-- end:Card --}}
            </div>
            @endforeach
        </div>
    </div>
    <!--end::Card body-->
</div>
<!--end::Card-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/haji/banner.js')}}"></script>
@endsection