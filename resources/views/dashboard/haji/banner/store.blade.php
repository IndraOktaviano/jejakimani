@extends('layouts.app')

@section('title')
{{$condition}} {{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">{{$condition}} {{$title}} </div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_haji_banner_form"
                method="POST" action="{{ route('cms.haji.banner.store') }}">
                @csrf
                {{-- begin:Wrapper --}}
                <div class="row">
                    <input type="hidden" name="slug" value="{{$data->id ?? ''}}">
                    <input type="hidden" name="slug" value="{{$slug}}">
                    {{-- begin:Row --}}
                    <div class="col-md-6 fv-row" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Gambar</label> <br>
                        <img id="image-preview" onclick="chooseImage()" src="" alt="image"
                            class="img-fluid cursor-pointer rounded-3" style="max-height: 300px">
                        <input type="file" id="image" name="image" onchange="previewImage()" accept="image/*"
                            class="d-none @error('image') is-invalid @enderror">
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    @if ($data)
                    <div class="col-md-6 fv-row" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Gambar Lama</label> <br>
                        <img id="old-image" src="" alt="image" class="img-fluid cursor-pointer rounded-3"
                            style="max-height: 300px">
                    </div>
                    @endif
                    {{-- end:Row --}}
                </div>
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <a href="{{route('cms.haji.'.$slug.'.banner.index')}}" class="btn btn-secondary">Kembali</a>
                <button type="submit" form="kt_haji_banner_form" class="btn btn-success ms-2"
                    id="kt_haji_banner_submit">
                    <span class="indicator-label">Simpan</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/haji/banner.js')}}"></script>
@if ($data)
<script>
    let image = '{{asset($data->json)}}'
    document.getElementById("old-image").src = image;
</script>
@endif
<script>
    let url = '{{asset("upload/empty.jpg")}}'
    document.getElementById("image-preview").src = url;    

    function previewImage() {
      document.getElementById("image-preview").style.display = "inline-block";
      if(!document.getElementById("image").files[0])    {          
            document.getElementById("image-preview").src = url;
      }
      else {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image").files[0]);      
            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
           };   
      }         
    };

    function chooseImage() {
        document.getElementById("image").click()
    }
</script>
@endsection