@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content')
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card header-->
            <div class="card-header border-bottom pt-6">
                <!--begin::Card title-->
                <div class="card-title">
                    <div class="fs-3 fw-bolder">{{ $title }} </div>
                </div>
                <!--end::Card title-->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body p-5">
                <form class="form w-100" novalidate="novalidate" id="kt_haji_label_banner_form" method="POST"
                    action="{{ route('cms.haji.label.banner.store') }}">
                    @csrf
                    {{-- begin:Wrapper --}}
                    <div class="row">
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <input type="hidden" name="slug" value="{{ $slug }}">
                        {{-- begin:Row --}}
                        <div class="col-md-12 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Judul</label>
                            <textarea name="name"
                                class="mytiny form-control @error('name') is-invalid @enderror">{{ old('name', $json->name) }}</textarea>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-12 fv-row mb-10" style="min-height: 300px">
                            <label class="form-label fs-6 fw-bolder text-dark">Deskripsi</label>
                            <textarea name="text"
                                class="mytiny form-control @error('text') is-invalid @enderror">{{ old('text', $json->text) }}</textarea>
                            @error('text')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Nomor WA</label>
                            <input type="number" name="phone" value="{{ old('phone', $json->phone) }}"
                                class="form-control hide-arrow @error('phone') is-invalid @enderror">
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Label Tombol WA</label>
                            <input type="text" name="label_wa" value="{{ old('label_wa', $json->label_wa) }}"
                                class="form-control @error('label_wa') is-invalid @enderror">
                            @error('label_wa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-12 fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark">Deskripsi WA</label>
                            <textarea name="text_wa" class="form-control @error('text_wa') is-invalid @enderror"
                                rows="4">{{ urldecode(old('text_wa', $json->text_wa)) }}</textarea>
                            @error('text_wa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        @include('components.custom.tracker-input', [
                            'tracker' => $json,
                        ])
                        {{-- end:Row --}}
                    </div>
                    {{-- end:Wrapper --}}
                </form>
            </div>
            <!--end::Card body-->
            <div class="card-footer">
                <div class="d-flex justify-content-end">
                    <a href="{{ route('cms.haji.' . $slug . '.banner.index') }}" class="btn btn-secondary">Kembali</a>
                    <button type="submit" form="kt_haji_label_banner_form" class="btn btn-success ms-2"
                        id="kt_haji_label_banner_submit">
                        <span class="indicator-label">Simpan</span>
                        <span class="indicator-progress">Mohon tunggu...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
@endsection

@section('customJS')
    <script src="{{ asset('js/tinymce.min.js') }}"></script>
    <script src="{{ asset('cms/js/custom/dashboard/haji/label-banner.js') }}"></script>
    <script>
        tinymce.init({
            selector: '.mytiny',
            plugins: 'print preview paste searchreplace autolink directionality visualblocks visualchars code fullscreen link media pagebreak template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools colorpicker textpattern help',
            toolbar: 'formatselect | fontsizeselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat |',
        });
    </script>
@endsection
