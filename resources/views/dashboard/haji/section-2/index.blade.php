@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">{{$title}}</div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_haji_section_2_form"
                method="POST" action="{{ route('cms.haji.section.2.store') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="slug" value="{{$slug}}">
                {{-- begin:Wrapper --}}
                <div class="row">
                    {{-- begin:Row 1 --}}
                    <div class="col-md-12 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Judul</label>
                        <textarea name="name" id="mytiny-1"
                            class="form-control @error('name') is-invalid @enderror">{{old('name', $json->name)}}</textarea>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-4 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Harga</label>
                        <input type="text" id="numberic" name="price" value="{{old('price', $json->price)}}"
                            class="form-control @error('price') is-invalid @enderror">
                        @error('price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-8 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Label Harga</label>
                        {{-- <input type="text" name="label_price" value="{{old('label_price', $json->label_price)}}"
                            class="form-control @error('label_price') is-invalid @enderror"> --}}
                            <textarea name="label_price" id="mytiny-2"
                            class="form-control @error('label_price') is-invalid @enderror">{{old('label_price', $json->label_price)}}</textarea>
                        @error('label_price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-12 fv-row mb-10" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Deskripsi</label>
                        <textarea name="text" id="mytiny"
                            class="form-control @error('text') is-invalid @enderror">{{old('text', $json->text)}}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Nomor WA</label>
                        <input type="number" name="phone" value="{{old('phone', $json->phone)}}"
                            class="form-control hide-arrow @error('phone') is-invalid @enderror">
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Label Tombol WA</label>
                        <input type="text" name="label_wa" value="{{old('label_wa', $json->label_wa)}}"
                            class="form-control @error('label_wa') is-invalid @enderror">
                        @error('label_wa')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-12 fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">Deskripsi WA</label>
                        <textarea name="text_wa" class="form-control @error('text_wa') is-invalid @enderror"
                            rows="4">{{urldecode(old('text_wa', $json->text_wa))}}</textarea>
                        @error('text_wa')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    @include('components.custom.tracker-input', [
                            'tracker' => $json,
                        ])
                    {{-- end:Row 1 --}}

                    {{-- begin:Row 2 --}}
                    <div class="col-md-6 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Pilih Gambar Baru</label> <br>
                        <img id="image-preview" onclick="chooseImage()" src="" alt="image"
                            class="img-fluid cursor-pointer rounded-3" style="max-height: 300px">
                        <input type="file" id="image" name="image" onchange="previewImage()" accept="image/*"
                            class="d-none @error('image') is-invalid @enderror">
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Gambar Lama</label> <br>
                        <img id="old-image" src="" alt="image" class="img-fluid cursor-pointer rounded-3"
                            style="max-height: 300px">
                    </div>
                    {{-- end:Row 2 --}}
                </div>
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <a href="{{route('cms.umroh.index')}}" class="btn btn-secondary">Kembali</a>
                <button type="submit" form="kt_haji_section_2_form" class="btn btn-success ms-2"
                    id="kt_haji_section_2_submit">
                    <span class="indicator-label">Simpan</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('js/tinymce.min.js')}}"></script>
<script src="{{asset('js/autoNumeric.min.js')}}"></script>
<script src="{{asset('cms/js/custom/dashboard/haji/section-2.js')}}"></script>
<script>
    tinymce.init({
      selector: '#mytiny',
      plugins: 'print preview paste searchreplace autolink directionality visualblocks visualchars code fullscreen link media pagebreak template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools colorpicker textpattern help',
      toolbar: 'formatselect | fontsizeselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat |',
    });
    tinymce.init({
      selector: '#mytiny-1',
      plugins: 'print preview paste searchreplace autolink directionality visualblocks visualchars code fullscreen link media pagebreak template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools colorpicker textpattern help',
      toolbar: 'formatselect | fontsizeselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat |',
    });
    tinymce.init({
      selector: '#mytiny-2',
      plugins: 'print preview paste searchreplace autolink directionality visualblocks visualchars code fullscreen link media pagebreak template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools colorpicker textpattern help',
      toolbar: 'formatselect | fontsizeselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat |',
    });
</script>
@if ($json)
<script>
    let image = '{{asset($json->image)}}'
    document.getElementById("old-image").src = image;
</script>
@endif
<script>
    let url = `{{asset('upload/empty.jpg')}}`
    
    document.getElementById("image-preview").src = url;

    function previewImage() {
      document.getElementById("image-preview").style.display = "inline-block";
      if(!document.getElementById("image").files[0])    {          
            document.getElementById("image-preview").src = url;
      }
      else {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image").files[0]);      
            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
           };   
      }    
    };

    function chooseImage() {
        document.getElementById("image").click()
    }

    new AutoNumeric('#numberic', {
    decimalCharacter : ',',
    digitGroupSeparator : '.',
    maximumValue: 999999999,
    modifyValueOnWheel: false,
});
</script>
@endsection