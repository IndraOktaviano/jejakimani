@extends('layouts.app')

@section('title')
{{$title}}
@endsection

@section('customCSS')
<style>
    .text-cut {
        overflow: hidden;
        width: 100%;
        display: -webkit-box;
        -webkit-box-orient: vertical;
    }

    .line-3 {
        -webkit-line-clamp: 3;
    }

    .handle {
        min-width: 18px;
        background: #607D8B;
        height: 15px;
        display: inline-block;
        cursor: move;
        margin-right: 10px;
    }

    .list-group-item {
        display: flex;
        align-items: center;
    }

    .highlight {
        background: #f7e7d3;
        min-height: 30px;
        list-style-type: none;
    }
</style>
@endsection

@section('content')
<!--begin::Card-->
<div class="card bg-transparent">
    <!--begin::Card header-->
    <div class="card-header bg-transparent border-0 pt-1 mb-2">
        <!--begin::Card title-->
        <div class="card-title">
            <button type="button" class="btn btn-info" data-bs-toggle="modal"
                data-bs-target="#urutanModal">Urutan</button>
        </div>
        <!--begin::Card title-->
        <div class="card-toolbar">
            <!--begin::Toolbar-->
            <div class="d-flex justify-content-end ">
                <!--begin::Add-->
                <a href=" {{route('cms.haji.'.$slug.$route.'.create')}} " class="btn btn-primary">Tambah</a>
                <!--end::Add-->
            </div>
        </div>
        <!--end::Card toolbar-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body bg-transparent pt-0" id="parent-card">
        <div class="row gy-5 g-xl-3">
            @foreach ($data as $item)
            @php
            $json = json_decode($item->json);
            @endphp
            <div class="col-lg-6">
                <!--begin::Card-->
                <div class="card card-xl-stretch">
                    <div class="card-header py-5">
                        <h3 class="card-title fw-bolder">
                            {{$json->name}}
                        </h3>
                    </div>
                    <div class="card-body p-10 text-center" style="{{$image ? 'min-height: 300px': ''}}">
                        @if ($image)
                        <img src="{{asset($json->image)}}" alt="" class="img-fluid mb-5" style="max-height: 300px">
                        @endif
                        <div class="text-gray-800 text-start text-cut line-3">
                            {!!$json->text!!}
                        </div>
                    </div>
                    <div class="card-footer text-end">
                        <a href=" {{route('cms.haji.'.$slug . $route.'.edit', $item->id)}} "
                            class="btn btn-sm btn-warning">Ubah</a>
                        <button type="submit" class="btn btn-danger btn-sm" form="kt_haji_section_list_destroy"
                            data-id="{{$item->id}}" data-slug="{{$slug}}" data-type="{{$type}}" data-route="{{$route}}"
                            data-url="{{route('cms.haji.section.list.destroy', $item->id)}}" data-no="{{$no}}"
                            data-token="{{ csrf_token() }}">Hapus
                        </button>
                    </div>
                </div>
                {{-- end:Card --}}
            </div>
            @endforeach
        </div>
    </div>
    <!--end::Card body-->
</div>
<!--end::Card-->

<!-- Modal -->
<div class="modal fade" id="urutanModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Urutan Haji Section List {{$no}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{route('cms.haji.section.list.order')}}" method="POST" id="formOrder">
                    <input type="hidden" name="slug" value="{{$slug}}">
                    <input type="hidden" name="type" value="{{$type}}">
                    <input type="hidden" name="no" value="{{$no}}">
                    <input type="hidden" name="route" value="{{$route}}">
                    @csrf
                    <ul class="sorter list-group">
                        @foreach ($order as $item)
                        <li class="list-group-item">
                            <span class="handle"></span>
                            <input type="hidden" name="id[]" value="{{$item['id']}}">
                            {{$item['name']}}
                        </li>
                        @endforeach
                    </ul>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="submit" form="formOrder" class="btn btn-primary">Simpan Perubahan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customJS')
<script src="{{asset('cms/js/custom/dashboard/haji/section-list.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
    $(document).ready(function(){

        var target = $('.sorter');
        target.sortable({
            handle: '.handle',
            placeholder: 'highlight',
            axis: "y",
        })

    })  
</script>
@endsection