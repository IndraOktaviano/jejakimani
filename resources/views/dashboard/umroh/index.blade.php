@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content')
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-0 pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="bi bi-search fs-5 position-absolute ms-6"></i>
                    <!--end::Svg Icon-->
                    <input type="text" data-kt-umroh-table-filter="search"
                        class="form-control form-control-solid w-250px ps-15" placeholder="Cari Paket" />
                </div>
                <!--end::Search-->
            </div>
            <!--begin::Card title-->
            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-umroh-table-toolbar="base">
                    <!--begin::Add umroh-->
                    <button type="button" class="btn btn-info me-3" data-bs-toggle="modal"
                        data-bs-target="#modal-tracker">
                        Tracker
                    </button>
                    <a href=" {{ route('cms.umroh.create') }} " class="btn btn-primary">Tambah</a>
                    <!--end::Add umroh-->
                </div>
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_umrohs_table">
                <!--begin::Table head-->
                <thead>
                    <!--begin::Table row-->
                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0 cursor-pointer">
                        <th class="min-w-10px pe-2">
                            No
                        </th>
                        <th>Gambar</th>
                        <th class="min-w-125px">Nama</th>
                        <th class="min-w-125px">Kategori</th>
                        <th class="min-w-125px">Harga</th>
                        <th class="min-w-125px">Jadwal</th>
                        <th class="d-none">Real Tanggal</th>
                        <th class="min-w-125px">Maskapai</th>
                        <th class="text-end min-w-70px">Actions</th>
                    </tr>
                    <!--end::Table row-->
                </thead>
                <!--end::Table head-->
                <!--begin::Table body-->
                <tbody class="fw-bold text-gray-600">
                    @foreach ($data as $value => $item)
                        <tr>
                            <td> {{ $value + 1 }} </td>
                            <td> <img src=" {{ asset($item->image) }} " alt="maskapai" class="img-fluid"
                                    style="max-height: 50px"> </td>
                            <td> {{ $item->name }} </td>
                            <td> {{ ucwords($item->category->name) }} </td>
                            <td class="text-end"> {{ number_format($item->price, 2, '.', ',') }} </td>
                            <td> {{ date('d-m-Y', strtotime($item->schedule)) }} </td>
                            <td class="d-none"> {{ date('Y-m-d', strtotime($item->schedule)) }} </td>
                            <td> <img src=" {{ asset($item->maskapai) }} " alt="maskapai" class="img-fluid"
                                    style="max-height: 50px"> </td>
                            <td>
                                <div class="btn-group" role="group">
                                    <a href=" {{ route('cms.umroh.edit', $item->id) }} " class="btn btn-warning">Ubah</a>
                                    <button type="button" class="btn btn-danger" data-kt-umroh-table-filter="delete_row"
                                        data-id="{{ $item->id }}" data-token="{{ csrf_token() }}">Hapus</button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <!--end::Table body-->
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

    <!-- Modal -->
    <div class="modal fade" id="modal-tracker" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('cms.umroh.tracker') }}" method="POST" id="submit-tracker">
                        @csrf
                        <div class="row">
                            <label class="form-label fs-6 fw-bolder text-dark">Tracker on Home Page</label>
                            <div class="row">
                                @include(
                                    'components.custom.tracker-input-multi',
                                    [
                                        'tracker' => $tracker_home,
                                    ]
                                )
                                <input type="hidden" name="page[]" value="home">
                            </div>
                            <label class="form-label fs-6 fw-bolder text-dark">Tracker on Detail Page</label>
                            <div class="row">
                                @include(
                                    'components.custom.tracker-input-multi',
                                    [
                                        'tracker' => $tracker_detail,
                                    ]
                                )
                                <input type="hidden" name="page[]" value="detail">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" form="submit-tracker" class="btn btn-primary">Perbarui</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customJS')
    <script src="{{ asset('cms/js/custom/dashboard/umroh/index.js') }}"></script>
    <script src="{{ asset('cms/plugins/custom/datatables/datatables.bundle.js') }}"></script>
@endsection
