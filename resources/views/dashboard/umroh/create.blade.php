@extends('layouts.app')

@section('title')
Tambah {{$title}}
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="container-xxl">
    <!--begin::Card-->
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-bottom pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <div class="fs-3 fw-bolder">Tambah Umroh</div>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body p-5">
            <form class="form w-100" novalidate="novalidate" enctype="multipart/form-data" id="kt_umroh_create_form"
                method="POST" action="{{ route('cms.umroh.store') }}">
                @csrf
                {{-- begin:Wrapper --}}

                {{-- begin:Row 1 --}}
                <div class="row">
                    <div class="col-md-6 mb-10 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Judul</label>
                        <input type="text" class="form-control form-control w-100 @error('name') is-invalid @enderror"
                            name="name" value="{{ old('name') }}" required autofocus />
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 mb-10 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Kategori</label>
                        <select name="category" data-control="select2" data-placeholder="Pilih Kategori"
                            class="form-select @error('category') is-invalid @enderror">
                            <option value=""></option>
                            @foreach ($category as $item)
                            <option {{old('category')==$item->id ? 'selected': ''}} value=" {{$item->id}} ">
                                {{ucwords($item->name)}} </option>
                            @endforeach
                        </select>
                        @error('category')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-10">
                    <div class="col-md-6 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Harga</label>
                        <input id="numberic" type="text" class="form-control @error('price') is-invalid @enderror"
                            name="price" value="{{ old('price') }}" required autofocus />
                        @error('price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Jadwal</label>
                        <input type="date" name="schedule" value="{{old('schedule')}}"
                            class="form-control @error('schedule') is-invalid @enderror">
                        @error('schedule')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-10">
                    <div class="col-12 fv-row">
                        <label class="form-label fs-6 fw-bolder text-dark">Slug</label>
                        <input type="text" class="form-control @error('slug') is-invalid @enderror" name="slug"
                            value="{{ old('slug') }}" required autofocus />
                    </div>
                </div>
                {{-- end:Row 1 --}}

                {{-- begin:Row 2 --}}
                <div class="row">
                    <div class="col-md-5 fv-row" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Gambar</label> <br>
                        <img id="image-preview-1" onclick="chooseImage1()" src="" alt="image"
                            class="img-fluid cursor-pointer rounded-3">
                        <input type="file" id="image" name="image" onchange="previewImage1()" accept="image/*"
                            class="d-none @error('image') is-invalid @enderror">
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-2 d-none d-md-block"></div>
                    <div class="col-md-5 fv-row" style="min-height: 300px">
                        <label class="form-label fs-6 fw-bolder text-dark">Maskapai</label> <br>
                        <img id="image-preview-2" onclick="chooseImage2()" src="" alt="maskapai"
                            class="img-fluid cursor-pointer rounded-3">
                        <input type="file" id="maskapai" name="maskapai" onchange="previewImage2()" accept="image/*"
                            class="d-none @error('maskapai') is-invalid @enderror">
                        @error('maskapai')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    {{-- end:Row 2 --}}
                </div>
                {{-- end:Wrapper --}}
            </form>
        </div>
        <!--end::Card body-->
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <a href="{{route('cms.umroh.index')}}" class="btn btn-secondary">Kembali</a>
                <button type="submit" form="kt_umroh_create_form" class="btn btn-success ms-2"
                    id="kt_umroh_create_submit">
                    <span class="indicator-label">Simpan</span>
                    <span class="indicator-progress">Mohon tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('customJS')
<script src="{{asset('js/autoNumeric.min.js')}}"></script>
<script src="{{asset('cms/js/custom/dashboard/umroh/create.js')}}"></script>
<script>
    let url = `{{asset('upload/empty.jpg')}}`
    
    document.getElementById("image-preview-1").src = url;
    document.getElementById("image-preview-2").src = url;

    function previewImage1() {
      document.getElementById("image-preview-1").style.display = "inline-block";
      if(!document.getElementById("image").files[0])    {          
            document.getElementById("image-preview-1").src = url;
      }
      else {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image").files[0]);      
            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview-1").src = oFREvent.target.result;
           };   
      }    
    };    

    function previewImage2() {
      document.getElementById("image-preview-2").style.display = "inline-block";
      if(!document.getElementById("maskapai").files[0])    {          
            document.getElementById("image-preview-2").src = url;
      }
      else {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("maskapai").files[0]);      
            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview-2").src = oFREvent.target.result;
           };   
      }    
    };    

    function chooseImage1() {
        document.getElementById("image").click()
    }

    function chooseImage2() {
        document.getElementById("maskapai").click()
    }

    new AutoNumeric('#numberic', {
        decimalCharacter : ',',
        digitGroupSeparator : '.',
        maximumValue: 999999999,
        modifyValueOnWheel: false,
    });
</script>
@endsection